// tmilib_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

class TMIProcessor : public TMIProcessCallback {
public:
	TMIProcessor() {}
	~TMIProcessor() {}

	void OnFlowStore(shared_ptr<TMIFlow> f) {
		//printf("FLOW CLOSED %d ms ->>>[%s] %s:%d <==%s==> %s:%d\n", f.GetDuration(), f.process_name.c_str(), f.GetLocalAddr().c_str(), f.GetLocalPort(), f.transport_protocol.c_str(), f.GetRemoteAddr().c_str(), f.GetRemotePort());
	}

	void OnFlowUpdate(shared_ptr<TMIFlow> f) {

		//printf("[%s] %s:%d <==%s==> %s:%d\n",f.process_name.c_str(), f.GetLocalAddr().c_str(), f.GetLocalPort(), f.transport_protocol.c_str(), f.GetRemoteAddr().c_str(), f.GetRemotePort());
	}
	void OnFlowAged(shared_ptr<TMIFlow> f) {
		//printf("FLOW AGED %llu ms ->>>[%s] %s:%d <==%s==> %s:%d\n", f.GetDuration(), f.process_name.c_str(), f.GetLocalAddr().c_str(), f.GetLocalPort(), f.transport_protocol.c_str(), f.GetRemoteAddr().c_str(), f.GetRemotePort());
	}

	void OnFlowSpeed(shared_ptr<TMIFlow> f) {

	}

};


int main(int argc, char *argv[]) {
	TMIControl& control = TMIControl::Instance();

	TMIProcessor processor;

	DWORD pid = 0;
	string appname;
	string country;

	if (argc > 1) {
		pid = atoi(argv[1]);
		string fullPath;
		char *path = TMIControl::GetProcessPath(pid, fullPath);
		if (!path) {
			printf("ERROR: No such process.\n");
			country = argv[1];
		} else  {
			appname = path;
		}
			
	}

	if (!control.OpenDriver()) {
		printf("ERROR: Driver not loaded.\n");
		return -1;
	}

	control.RegisterClient(&processor);
	while (1) {

		if (!control.Process()) {
			Sleep(1000);
			if (country.length() > 0) {
				vector <shared_ptr<TMIFlow>> flows = control.GetLiveSnapshot(country);
				printf("%s connection list (%zu connections):\n", country.c_str(), flows.size());
				for (int i = 0; i < flows.size(); i++) {
					shared_ptr<TMIFlow> f = flows[i];
					printf("[%s |%c%c| %s] %s:%d <==%s==> %s:%d RX: %lluKB, TX: %lluKB\n",
						f->GetCountry().c_str(),
						f->IsClient() ? 'C' : ' ',
						f->IsServer() ? 'S' : ' ',
						f->GetProcessName().c_str(),
						f->GetLocalAddr().c_str(),
						f->GetLocalPort(),
						f->GetTransportProtocol().c_str(),
						f->GetRemoteAddr().c_str(),
						f->GetRemotePort(),
						f->GetFlowRx(),
						f->GetFlowTx()
						);
				}

			} else if (!pid) {
				vector <shared_ptr<TMIFlow>> flows = control.GetLiveSnapshot();
				printf("All processes connection list:\n");
				for (int i = 0; i < flows.size(); i++) {
					shared_ptr<TMIFlow> f = flows[i];
					printf("[%s |%c%c| %s] %s:%d <==%s==> %s:%d RX: %lluKB, TX: %lluKB\n",
						f->GetCountry().c_str(),
						f->IsClient() ? 'C' : ' ',
						f->IsServer() ? 'S' : ' ',
						f->GetProcessName().c_str(),
						f->GetLocalAddr().c_str(),
						f->GetLocalPort(),
						f->GetTransportProtocol().c_str(),
						f->GetRemoteAddr().c_str(),
						f->GetRemotePort(),
						f->GetFlowRx(),
						f->GetFlowTx()
						);
				}
			}
			else {
				vector <shared_ptr<TMIFlow>> flows = control.GetLiveSnapshot(pid);
				printf("Process %s(%u) connection list (%zu connections):\n", appname.c_str(), (DWORD)pid, flows.size());
				for (int i = 0; i < flows.size(); i++) {
					shared_ptr<TMIFlow> f = flows[i];

					printf("[%s | %c%c] %s:%d <==%s==> %s:%d RX: %lluKB, TX: %lluKB\n",
						f->GetCountry().c_str(),
						f->IsClient() ? 'C' : ' ',
						f->IsServer() ? 'S' : ' ',
						f->GetLocalAddr().c_str(),
						f->GetLocalPort(),
						f->GetTransportProtocol().c_str(),
						f->GetRemoteAddr().c_str(),
						f->GetRemotePort(),
						f->GetFlowRx(),
						f->GetFlowTx()
						);
				}

			}
		}
	}

	return 0;
}

