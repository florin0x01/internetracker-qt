#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void handlebut();

private:
    Ui::MainWindow *ui;
   
    QTimer* timer;

    QMap<QString, bool> countriesActive;
};

#endif // MAINWINDOW_H
