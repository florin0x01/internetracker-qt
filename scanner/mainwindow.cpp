#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QTimer>
#include <Qdebug>
#include <QDir>
#include <QDirIterator>
#include <QGraphicsPixmapItem>
#include <iostream>
#include <QFile>
#include <QTextStream>



void processFile(const QString& fil)
{

	qDebug() << fil;
    QPixmap p(fil);
	
QGraphicsPixmapItem *item = new QGraphicsPixmapItem(p);

QFile file(fil + ".bin");
if (!file.open(QIODevice::WriteOnly)) return;


qreal xleft = item->opaqueArea().boundingRect().topLeft().x();
qreal yleft = item->opaqueArea().boundingRect().topLeft().y();

qreal xright = item->opaqueArea().boundingRect().bottomRight().x();
qreal yright = item->opaqueArea().boundingRect().bottomRight().y();

//file << (double)xleft << )doyleft << xright << yleft << "\n";
QDataStream out(&file);
out << QPoint(xleft, yleft) << QPoint(xright, yright);

file.close();

QPixmap result = p.copy(QRect(QPoint(xleft, yleft), QPoint(xright, yright)));

QString basename = QFileInfo(file).baseName();
	
result.save(basename + "2.png");

/*
    QImage p(fil);
    int l =p.width(), r = 0, t = p.height(), b = 0;

    for (int y = 0; y < p.height(); ++y) {
        QRgb *row = (QRgb*)p.scanLine(y);
        bool rowFilled = false;
        for (int x = 0; x < p.width(); ++x) {
            if (qAlpha(row[x])) {
                rowFilled = true;
                r = std::max(r, x);
                if (l > x) {
                    l = x;
                    x = r; // shortcut to only search for new right bound from here
                }
            }
        }
        if (rowFilled) {
            t = std::min(t, y);
            b = y;
        }
    }

    std::cerr << "Image: " << fil << ", top left: " << r << ": bottom : " << b << std::endl;
    */
}

void MainWindow::handlebut()
{
    QString txt = ui->lineEdit->text();
    QDirIterator it(txt, QStringList() << "*.png", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        processFile(it.next());
       
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
     connect(ui->scanfilesbut, SIGNAL(released()), this, SLOT(handlebut()));
}

MainWindow::~MainWindow()
{
    delete ui;
}
