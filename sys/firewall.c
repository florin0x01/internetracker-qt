#include "firewall.h"
#include "cmd.h"

volatile SHORT firewall_country[FIREWALL_MAX_COUNTRY];
volatile SHORT firewall_initialized = 0;

static LIST_ENTRY gAppTable[APP_HASHTABLE_SIZE];
static KSPIN_LOCK gAppBucketLock[APP_HASHTABLE_SIZE];
static UINT32 gAppBucketSize[APP_HASHTABLE_SIZE];

#define JHASH_SEED 0xd00db00b

UINT32 hash_app(char *app, UINT32 len) {
	UINT32 hash = 0;
	hash = jhash(app, len, JHASH_SEED);
	return hash;
}


static inline SHORT firewall_ready() {
	return InterlockedCompareExchange16(&firewall_initialized, 0, 0);
}

void firewall_init() {

	for (int i = 0; i < FIREWALL_MAX_COUNTRY; i++) {		
		InterlockedExchange16(&firewall_country[i], 0);
	}

	DbgPrint("TrackMyInternet: firewall initialized \n");	
	InterlockedExchange16(&firewall_initialized, 1);

	for (int i = 0; i < APP_HASHTABLE_SIZE; i++) {
		InitializeListHead(&gAppTable[i]);
		KeInitializeSpinLock(&gAppBucketLock[i]);
		gAppBucketSize[i] = 0;
	}
}

void firewall_destroy() {
	KIRQL irql1;
	PLIST_ENTRY listEntry;
	list_block_app_entry *entry;

	for (int i = 0; i < APP_HASHTABLE_SIZE; i++) {

		KeAcquireSpinLock(&gAppBucketLock[i], &irql1);
		PLIST_ENTRY bucket = &gAppTable[i];

		while (!IsListEmpty(bucket)) {

			listEntry = RemoveHeadList(bucket);
			gAppBucketSize[i]--;

			entry = CONTAINING_RECORD(
				listEntry,
				list_block_app_entry,
				listEntry
				);

			ExFreePoolWithTag(entry->app->app, 'OPPA');
			ExFreePoolWithTag(entry->app, 'OPPA');
			ExFreePoolWithTag(entry, 'OPPA');
		}
		KeReleaseSpinLock(&gAppBucketLock[i], irql1);
	}

	DbgPrint("TrackMyInternet: firewall stopped \n");
}

void firewall_geoip_block(PCHAR country_iso) {
	if (firewall_ready()) {
		InterlockedExchange16(&firewall_country[*((PSHORT)country_iso)], 1);
		//DbgPrint("TrackMyInternet: blocked %c%c\n", country_iso[0], country_iso[1]);
	}	
}

void firewall_geoip_unblock(PCHAR country_iso) {
	if (firewall_ready()) {
		InterlockedExchange16(&firewall_country[*((PSHORT)country_iso)], 0);
		//DbgPrint("TrackMyInternet: unblocked %c%c\n", country_iso[0], country_iso[1]);
	}
}

int firewall_geoip_isblocked(PCHAR country_iso) {
	if (!firewall_ready()) {
		return 0;
	}

	return InterlockedCompareExchange16(&firewall_country[*((PSHORT)country_iso)], 0, 0);
}

// 256 blocked apps
static UCHAR reply_buffer[MAX_PATH * 256];

static blocked_countries *hdr = (blocked_countries *)reply_buffer;
static block_country *country_list = (block_country *)(reply_buffer + sizeof(blocked_countries));

static blocked_apps *apps = (blocked_apps *)reply_buffer;
static char *app_list = (char *)(reply_buffer + sizeof(blocked_apps));

PUCHAR firewall_fill_blocked_countries(UINT32 *len) {
	hdr->count = 0;
	for (SHORT i = 0; i < FIREWALL_MAX_COUNTRY; i++) {
		if (InterlockedCompareExchange16(&firewall_country[i], 0, 0)) {
			hdr->count++;
			*((PSHORT)country_list->country_iso) = i;
			country_list++;
		}
	}

	*len = sizeof(blocked_countries) + sizeof(block_country) * hdr->count;
	return reply_buffer;
}


PUCHAR firewall_fill_blocked_apps(UINT32 *len) {
	LIST_ENTRY* listEntry;
	list_block_app_entry *entry;
	KIRQL irql1;

	if (!len) {
		return NULL;
	}

	*len = sizeof(blocked_apps);
	apps->count = 0;

	for (int i = 0; i < APP_HASHTABLE_SIZE; i++) {
		KeAcquireSpinLock(&gAppBucketLock[i], &irql1);
		PLIST_ENTRY bucket = &gAppTable[i];

		for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink) {
			entry = CONTAINING_RECORD(
				listEntry,
				list_block_app_entry,
				listEntry
				);
			
			if (*len + sizeof(blocked_app_reply) + entry->app->len >= sizeof(reply_buffer)) {
				// no more space
				return reply_buffer;
			}

			// enough space in buffer copy data
			blocked_app_reply *app = (blocked_app_reply*)(reply_buffer + *len);
			app->size = entry->app->len;
			*len += sizeof(blocked_app_reply);
			char *app_name = (char*)(reply_buffer + *len);
			memcpy(app_name, entry->app->app, entry->app->len);
			*len += app->size;
			apps->count++;
		}

		KeReleaseSpinLock(&gAppBucketLock[i], irql1);
	}

	return reply_buffer;
}


static int firewall_add_app(PLIST_ENTRY bucket, PCHAR appname, USHORT len) {
	list_block_app_entry *entry = ExAllocatePoolWithTag(NonPagedPool, sizeof(list_block_app_entry), 'OPPA');

	if (!entry) {
		return -1;
	}

	blocked_app *app = ExAllocatePoolWithTag(NonPagedPool, sizeof(blocked_app), 'OPPA');

	if (!app) {
		ExFreePoolWithTag(entry, 'OPPA');
		return -1;
	}

	app->app = ExAllocatePoolWithTag(NonPagedPool, len, 'OPPA');
	app->len = len;
	memcpy(app->app, appname, len);	
	entry->app = app;
		
	char path[4096];
	memcpy(path, appname, len);
	path[len] = 0;
	DbgPrint("TrackMyInternet: Added blocked app[%d] = %ws",len, path);

	InsertTailList(bucket, &entry->listEntry);
	return 0;
}

int firewall_app_isblocked(PCHAR appname, USHORT len) {
	if (!appname || !len) {
		return 0;
	}

	// len is in bytes
	//firewall_util_lowercase(app_path, len / sizeof(WCHAR));

	UINT32 hash = hash_app((PCHAR)appname, len);
	UINT32 hash_index = hash % APP_HASHTABLE_SIZE;
	KIRQL irql1;
	LIST_ENTRY *listEntry;
	list_block_app_entry *appEntry;
	blocked_app *app;

		
	KeAcquireSpinLock(&gAppBucketLock[hash_index], &irql1);
	PLIST_ENTRY bucket = &gAppTable[hash_index];

	for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink) {
		appEntry = CONTAINING_RECORD(
			listEntry,
			list_block_app_entry,
			listEntry
			);

		app = appEntry->app;
		//DbgPrint("TrackMyInternet: %ws vs %ws", appname, app->app);
		if (len == app->len && !memcmp(app->app, appname, len)) {
			//DbgPrint("TrackMyInternet: Matched blocked app[%d] = %ws", len, app->app);
			KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);
			return 1;
		}
	}


	KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);

	return 0;

}
void firewall_app_block(PCHAR appname, USHORT len) {
	if (!appname || !len) {
		return;
	}

	firewall_util_lowercase((PWCHAR)appname, len / sizeof(WCHAR));
	//DbgPrint("TrackMyInternet: Matched blocked app[%d] = %ws", len, app->app);
	UINT32 hash = hash_app(appname, len);
	UINT32 hash_index = hash % APP_HASHTABLE_SIZE;
	KIRQL irql1;
	LIST_ENTRY *listEntry;
	list_block_app_entry *appEntry;
	blocked_app *app;
	

	KeAcquireSpinLock(&gAppBucketLock[hash_index], &irql1);
	PLIST_ENTRY bucket = &gAppTable[hash_index];

	for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink) {
		appEntry = CONTAINING_RECORD(
			listEntry,
			list_block_app_entry,
			listEntry
			);

		app = appEntry->app;
		if (len == app->len && !memcmp(app->app, appname, len)) {
			// already present
			KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);			
			return;
		}
	}


	if (gAppBucketSize[hash_index] < APP_BUCKET_SIZE_MAX && firewall_add_app(bucket, appname, len) == 0) {
		gAppBucketSize[hash_index]++;
	}

	KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);

	return;
}

void firewall_app_unblock(PCHAR appname, USHORT len) {
	if (!appname || !len) {
		return;
	}

	firewall_util_lowercase((PWCHAR)appname, len / sizeof(WCHAR));

	UINT32 hash = hash_app(appname, len);
	UINT32 hash_index = hash % APP_HASHTABLE_SIZE;
	KIRQL irql1;
	LIST_ENTRY *listEntry;
	list_block_app_entry *appEntry;
	blocked_app *app;

	char path[4096];
	memcpy(path, appname, len);
	path[len] = 0;
	DbgPrint("TrackMyInternet: Unblocking app[%d] = %ws", len, path);

	KeAcquireSpinLock(&gAppBucketLock[hash_index], &irql1);
	PLIST_ENTRY bucket = &gAppTable[hash_index];


	for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink)
	{
		appEntry = CONTAINING_RECORD(
			listEntry,
			list_block_app_entry,
			listEntry
			);

		app = appEntry->app;
		if (len == app->len && !memcmp(app->app, appname, len)) {
			// found
			RemoveEntryList(listEntry);
			gAppBucketSize[hash_index]--;
			KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);

			ExFreePoolWithTag(appEntry->app->app, 'OPPA');
			ExFreePoolWithTag(appEntry->app, 'OPPA');
			ExFreePoolWithTag(appEntry, 'OPPA');
			return;
		}

	}


	KeReleaseSpinLock(&gAppBucketLock[hash_index], irql1);

	return;
}

