#pragma once

#define CMD_VERSION			0
#define CMD_CONFIG_GEOIP	1
#define CMD_START_STREAM	2
#define CMD_STOP_STREAM		3
#define CMD_BLOCK_COUNTRY	4
#define CMD_BLOCK_IP		5
#define CMD_UNBLOCK_COUNTRY	6
#define CMD_UNBLOCK_IP		7
#define CMD_LIST_BLOCKED_COUNTRIES	8
#define CMD_BLOCK_APP	9
#define CMD_UNBLOCK_APP	10
#define CMD_LIST_BLOCKED_APPS	11

void parse_command(PUCHAR buffer, UINT32 len);

void cmd_reply(PUCHAR buffer, UINT32 len);
PUCHAR cmd_get_reply(UINT32 *len);