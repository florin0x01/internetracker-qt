#include <ntddk.h>
#include <wdf.h>

#pragma warning(push)
#pragma warning(disable:4201)       // unnamed struct/union

#include <fwpsk.h>

#pragma warning(pop)

#include <fwpmk.h>

#include <ws2ipdef.h>
#include <in6addr.h>
#include <ip2string.h>

#include "cmd.h"
#include "inspect.h"
#include "public.h"
#include "geoip.h"
#include "firewall.h"

static PUCHAR reply = NULL;
static UINT32 reply_len = 0;

void cmd_reply(PUCHAR buffer, UINT32 len) {
	reply = buffer;
	reply_len = len;
}

PUCHAR cmd_get_reply(UINT32 *len) {
	PUCHAR reply_buffer = reply;
	reply = NULL;
	*len = reply_len;
	reply_len = 0;

	return reply_buffer;
}

static void parse_version(PUCHAR data) {
	UNREFERENCED_PARAMETER(data);
	return;	
}

static void parse_cmd_list_blocked_countries(PUCHAR data) {
	UNREFERENCED_PARAMETER(data);
	UINT32 len = 0;
	PUCHAR buffer = firewall_fill_blocked_countries(&len);

	cmd_reply(buffer, len);
}

static void parse_cmd_block_country(PUCHAR data) {
	block_country *country = (block_country *)data;
	firewall_geoip_block(country->country_iso);
}

static void parse_cmd_unblock_country(PUCHAR data) {
	block_country *country = (block_country *)data;
	firewall_geoip_unblock(country->country_iso);
}


static void parse_cmd_block_app(PUCHAR data, UINT32 size) {
	firewall_app_block((PCHAR)data, (USHORT)size);
}

static void parse_cmd_unblock_app(PUCHAR data, UINT32 size) {
	firewall_app_unblock((PCHAR)data, (USHORT)size);
}

static void parse_cmd_list_blocked_apps(PUCHAR data) {
	UNREFERENCED_PARAMETER(data);
	UINT32 len = 0;
	PUCHAR buffer = firewall_fill_blocked_apps(&len);

	cmd_reply(buffer, len);
}


static void parse_config_geoip(PUCHAR data) {
	UINT32 index;
	geoip_config *config = (geoip_config *)data;
	geoip_entry_v4 *entry = (geoip_entry_v4 *)(data + sizeof(geoip_config));

	if (geoip_configured()) {
		geoip_reset_config();
	}

	for (index = 0; index < config->entry_count; index++, entry++) {
		geoip_add_v4(entry);
	}

	geoip_config_done();
	DbgPrint("stats_geoip_max_bucket_size = %u\n", stats_geoip_max_bucket_size);
}

void parse_command(PUCHAR buffer, UINT32 len) {
	if (!buffer) {
		return;
	}

	driver_command_v1 *cmd = (driver_command_v1 *)buffer;
	PUCHAR data = buffer + sizeof(driver_command_v1);

	// validate data field
	if (cmd->data_size != len - sizeof(driver_command_v1)) {
		return;
	}

	switch (cmd->cmd) {
		case CMD_VERSION:
			parse_version(data);
			break;
		case CMD_CONFIG_GEOIP:
			parse_config_geoip(data);
			break;
		case CMD_START_STREAM:
			break;
		case CMD_STOP_STREAM:
			break;
		case CMD_BLOCK_COUNTRY:
			parse_cmd_block_country(data);
			break;
		case CMD_BLOCK_IP:
			break;
		case CMD_UNBLOCK_COUNTRY:
			parse_cmd_unblock_country(data);
			break;
		case CMD_UNBLOCK_IP:
			break;
		case CMD_LIST_BLOCKED_COUNTRIES:
			parse_cmd_list_blocked_countries(data);
			break;
		case CMD_BLOCK_APP:
			parse_cmd_block_app(data, cmd->data_size);
			break;
		case CMD_UNBLOCK_APP:
			parse_cmd_unblock_app(data, cmd->data_size);
			break;
		case CMD_LIST_BLOCKED_APPS:
			parse_cmd_list_blocked_apps(data);
			break;
	}
}