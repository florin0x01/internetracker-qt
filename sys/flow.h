#pragma once
#pragma warning(disable:4201)       // unnamed struct/union
#pragma warning(disable:4214) 
#include <ntddk.h>
#include <wdf.h>
#include "inspect.h"

#include "public.h"

typedef struct _flow_entry {
	LIST_ENTRY listEntry;
	flow *f;
}flow_entry;

struct tcphdr {
	USHORT th_sport;  /* source port */
	USHORT th_dport;  /* destination port */
	UINT32 th_seq;   /* sequence number */
	UINT32 th_ack;   /* acknowledgement number */
	UCHAR th_x2;	
	UCHAR th_flags;
#define TH_FIN 0x01
#define TH_SYN 0x02
#define TH_RST 0x04
#define TH_PUSH 0x08
#define TH_ACK 0x10
#define TH_URG 0x20
#define TH_ECE 0x40
#define TH_CWR 0x80
#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)

	USHORT th_win;   /* window */
	USHORT th_sum;   /* checksum */
	USHORT th_urp;   /* urgent pointer */
};

typedef struct _NDF_TCP_HEADER
{
	USHORT SrcPort : 16;
	USHORT DstPort : 16;
	ULONG SequenceNumber : 32;
	ULONG ApprovalNumber : 32;
	ULONG HeaderInfo[2];
} NDF_TCP_HEADER, *PNDF_TCP_HEADER;


void init_flows();
void destroy_flows();

int flow_add(TL_INSPECT_PENDED_PACKET *packet);
size_t flow_get(PUCHAR buffer, size_t length);