#pragma once
#pragma warning(disable:4201)       // unnamed struct/union
//
// Device type           -- in the "User Defined" range."
//
#define FILEIO_TYPE 40001
//
// The IOCTL function codes from 0x800 to 0xFFF are for customer use.
//
#define IOCTL_NONPNP_METHOD_IN_DIRECT \
    CTL_CODE( FILEIO_TYPE, 0x900, METHOD_IN_DIRECT, FILE_ANY_ACCESS  )

#define IOCTL_NONPNP_METHOD_OUT_DIRECT \
    CTL_CODE( FILEIO_TYPE, 0x901, METHOD_OUT_DIRECT , FILE_ANY_ACCESS  )

#define IOCTL_NONPNP_METHOD_BUFFERED \
    CTL_CODE( FILEIO_TYPE, 0x902, METHOD_BUFFERED, FILE_ANY_ACCESS  )

#define IOCTL_NONPNP_METHOD_NEITHER \
    CTL_CODE( FILEIO_TYPE, 0x903, METHOD_NEITHER , FILE_ANY_ACCESS  )


#define DRIVER_FUNC_INSTALL     0x01
#define DRIVER_FUNC_REMOVE      0x02
#define DRIVER_FUNC_READ      0x02

#define DRIVER_NAME       "TrackMyInternet"
#define DEVICE_NAME       "\\\\.\\TrackMyInternet\\device"

// flow extension, required by serialization/deserialziation
// flow extensions are not used by the driver itslef
#define FLAG_BLOCKED_APP (1 << 3)
#define FLAG_BLOCKED_COUNTRY (1 << 4)
#define FLAG_NEED_GEOIP (1 << 5)
#define FLAG_GEOIP_OK (1 << 6)
#define FLAG_GEOIP_FAIL (1 << 7)
#define FLAG_FLOW_EXT (1 << 8)
#define FLAG_SERVER (1 << 16)
#define FLAG_CLIENT (1 << 17)

#pragma pack(push,1)
typedef struct _flow {
	UINT32 hash;
	UINT32 sid[3];

	ULONG interfaceIndex;
	UINT64 processId;
	UINT8  flow_ended;
	UINT32 flags;

	UINT64 totalTx; // bytes
	UINT64 totalRx; // bytes
	UINT32 totalPacketTx;
	UINT32 totalPacketRx;

	USHORT addressFamily;
	UINT8 protocol;

	// time of first packet
	LARGE_INTEGER start_time;
	// time of last packet
	LARGE_INTEGER time;

	union {
		FWP_BYTE_ARRAY16 localAddr;
		UINT32 ipv4LocalAddr;
	};

	union
	{
		FWP_BYTE_ARRAY16 remoteAddr;
		UINT32 ipv4RemoteAddr;
	};

	union
	{
		UINT16 localPort;
		UINT16 icmpType;
	};

	union
	{
		UINT16 remotePort;
		UINT16 icmpCode;
	};
	
	CHAR country_iso[3];
	CHAR country_name[32];
}flow;

// user by userspace only
typedef struct _flow_ext_v1 {
	char process_path[1024];
	LARGE_INTEGER start;
	LARGE_INTEGER end;
	INT64 db_id;

	/// used in group by queries
	UINT32 count; 
}flow_ext_v1;

//////////////////////////////
// GEOIP
//////////////////////////////
typedef struct _geoip_config {
	UINT64 version;
	UINT32 entry_count;
	// append geoip_entries next
}geoip_config;

typedef struct _geoip_entry_v4 {
	UINT32 network;
	UINT32 netmask;
	char country_iso[3];
	char country_name[32];
}geoip_entry_v4;

//////////////////////////////
// FIREWALL
//////////////////////////////
typedef struct _block_country {
	char country_iso[3];	
}block_country;

typedef struct _blocked_countries {
	USHORT count;
	//block_country elements come next
}blocked_countries;

typedef struct _blocked_apps {
	USHORT count;
}blocked_apps;

typedef struct _blocked_app_reply {
	USHORT size;
	// data buffer follows nexts
}blocked_app_reply;

typedef struct _driver_command_v1 {
	UCHAR cmd;
	DWORD data_size;
}driver_command_v1;

#pragma pack(pop)