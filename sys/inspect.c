/*++

Copyright (c) Microsoft Corporation. All rights reserved

Abstract:

   This file implements the classifyFn callout functions for the ALE connect,
   recv-accept, and transport callouts. In addition the system worker thread 
   that performs the actual packet inspection is also implemented here along 
   with the eventing mechanisms shared between the classify function and the
   worker thread.

   connect/Packet inspection is done out-of-band by a system worker thread 
   using the reference-drop-clone-reinject as well as ALE pend/complete 
   mechanism. Therefore the sample can serve as a base in scenarios where 
   filtering decision cannot be made within the classifyFn() callout and 
   instead must be made, for example, by an user-mode application.

Environment:

    Kernel mode

--*/


#include <ntddk.h>

#pragma warning(push)
#pragma warning(disable:4201)       // unnamed struct/union

#include <fwpsk.h>
#include <Mstcpip.h>
#pragma warning(pop)

#include <fwpmk.h>
#include <wdf.h>

#define NTSTRSAFE_LIB
#include <ntstrsafe.h>
#include <wdmsec.h> // for SDDLs

#include "inspect.h"
#include "flow.h"
#include "firewall.h"
#include "utils.h"

#if(NTDDI_VERSION >= NTDDI_WIN7)
void dumpPacket(TL_INSPECT_PENDED_PACKET *packet) {
	if (!packet) {
		return ;
	}

	flow_add(packet);
	FreePendedPacket(packet);
}

void
TLInspectALEConnectClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_opt_ const void* classifyContext,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#else

void
TLInspectALEConnectClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#endif /// (NTDDI_VERSION >= NTDDI_WIN7)

/* ++

   This is the classifyFn function for the ALE connect (v4 and v6) callout.
   For an initial classify (where the FWP_CONDITION_FLAG_IS_REAUTHORIZE flag
   is not set), it is queued to the connection list for inspection by the
   worker thread. For re-auth, we first check if it is triggered by an ealier
   FwpsCompleteOperation call by looking for an pended connect that has been
   inspected. If found, we remove it from the connect list and return the 
   inspection result; otherwise we can conclude that the re-auth is triggered 
   by policy change so we queue it to the packet queue to be process by the 
   worker thread like any other regular packets.

-- */
{

   TL_INSPECT_PENDED_PACKET* pendedConnect = NULL;
   ADDRESS_FAMILY addressFamily;

#if(NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(classifyContext);
#endif /// (NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(filter);
   UNREFERENCED_PARAMETER(flowContext);
   UNREFERENCED_PARAMETER(classifyOut);

   addressFamily = GetAddressFamilyForLayer(inFixedValues->layerId);
    pendedConnect = AllocateAndInitializePendedPacket(
                        inFixedValues,
                        inMetaValues,
                        addressFamily,
                        layerData,
                        TL_INSPECT_CONNECT_PACKET,
                        FWP_DIRECTION_OUTBOUND
                        );	
    if (pendedConnect == NULL)
    {
        return;
    }

	pendedConnect->flags |= FLAG_CLIENT;
	dumpPacket(pendedConnect);

	if (pendedConnect->flags & (FLAG_BLOCKED_COUNTRY | FLAG_BLOCKED_APP)) {
		classifyOut->actionType = FWP_ACTION_BLOCK;
		//classifyOut->flags |= FWPS_CLASSIFY_OUT_FLAG_ABSORB;
		classifyOut->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	else {
		classifyOut->actionType = FWP_ACTION_PERMIT;
	}	

   return;
}

#if(NTDDI_VERSION >= NTDDI_WIN7)

void
TLInspectALERecvAcceptClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_opt_ const void* classifyContext,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#else

void
TLInspectALERecvAcceptClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#endif /// (NTDDI_VERSION >= NTDDI_WIN7)
/* ++

   This is the classifyFn function for the ALE Recv-Accept (v4 and v6) callout.
   For an initial classify (where the FWP_CONDITION_FLAG_IS_REAUTHORIZE flag
   is not set), it is queued to the connection list for inspection by the
   worker thread. For re-auth, it is queued to the packet queue to be process 
   by the worker thread like any other regular packets.

-- */
{
   TL_INSPECT_PENDED_PACKET* pendedRecvAccept = NULL;

   ADDRESS_FAMILY addressFamily;

#if(NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(classifyContext);
#endif /// (NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(filter);
   UNREFERENCED_PARAMETER(flowContext);
   UNREFERENCED_PARAMETER(classifyOut);


   addressFamily = GetAddressFamilyForLayer(inFixedValues->layerId);
    pendedRecvAccept = AllocateAndInitializePendedPacket(
                            inFixedValues,
                            inMetaValues,
                            addressFamily,
                            layerData,
                            TL_INSPECT_CONNECT_PACKET,
                            FWP_DIRECTION_INBOUND
                            );

    if (pendedRecvAccept == NULL)
    {
		return;
    }

	pendedRecvAccept->flags |= FLAG_SERVER;
	dumpPacket(pendedRecvAccept);
	if (pendedRecvAccept->flags & (FLAG_BLOCKED_COUNTRY | FLAG_BLOCKED_APP)) {
		classifyOut->actionType = FWP_ACTION_BLOCK;
		//classifyOut->flags |= FWPS_CLASSIFY_OUT_FLAG_ABSORB;
		classifyOut->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	else {
		classifyOut->actionType = FWP_ACTION_PERMIT;
	}


   return;
}

#if(NTDDI_VERSION >= NTDDI_WIN7)

void
TLInspectTransportClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_opt_ const void* classifyContext,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#else

void
TLInspectTransportClassify(
   _In_ const FWPS_INCOMING_VALUES* inFixedValues,
   _In_ const FWPS_INCOMING_METADATA_VALUES* inMetaValues,
   _Inout_opt_ void* layerData,
   _In_ const FWPS_FILTER* filter,
   _In_ UINT64 flowContext,
   _Inout_ FWPS_CLASSIFY_OUT* classifyOut
   )

#endif
/* ++

   This is the classifyFn function for the Transport (v4 and v6) callout.
   packets (inbound or outbound) are ueued to the packet queue to be processed 
   by the worker thread.

-- */
{


   TL_INSPECT_PENDED_PACKET* pendedPacket = NULL;
   FWP_DIRECTION packetDirection;

   ADDRESS_FAMILY addressFamily;

#if(NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(classifyContext);
#endif /// (NTDDI_VERSION >= NTDDI_WIN7)
   UNREFERENCED_PARAMETER(filter);
   UNREFERENCED_PARAMETER(flowContext);
   UNREFERENCED_PARAMETER(classifyOut);

   addressFamily = GetAddressFamilyForLayer(inFixedValues->layerId);
   packetDirection = GetPacketDirectionForLayer(inFixedValues->layerId);

   pendedPacket = AllocateAndInitializePendedPacket(
                     inFixedValues,
                     inMetaValues,
                     addressFamily,
                     layerData,
                     TL_INSPECT_DATA_PACKET,
                     packetDirection
                     );

   if (pendedPacket == NULL)
   {
	   return;
   }

   dumpPacket(pendedPacket);
   if (pendedPacket->flags & (FLAG_BLOCKED_COUNTRY | FLAG_BLOCKED_APP)) {
	   classifyOut->actionType = FWP_ACTION_BLOCK;
	   //classifyOut->flags |= FWPS_CLASSIFY_OUT_FLAG_ABSORB;
	   classifyOut->rights &= ~FWPS_RIGHT_ACTION_WRITE;
   }
   else {
	   classifyOut->actionType = FWP_ACTION_PERMIT;
   }


   return;
}

NTSTATUS
TLInspectALEConnectNotify(
   _In_  FWPS_CALLOUT_NOTIFY_TYPE notifyType,
   _In_ const GUID* filterKey,
   _Inout_ const FWPS_FILTER* filter
   )
{
   UNREFERENCED_PARAMETER(notifyType);
   UNREFERENCED_PARAMETER(filterKey);
   UNREFERENCED_PARAMETER(filter);

   //DbgPrint("TrackMyInternet in %s\n", __FUNCTION__);
   return STATUS_SUCCESS;
}

NTSTATUS
TLInspectALERecvAcceptNotify(
   _In_ FWPS_CALLOUT_NOTIFY_TYPE notifyType,
   _In_ const GUID* filterKey,
   _Inout_ const FWPS_FILTER* filter
   )
{
   UNREFERENCED_PARAMETER(notifyType);
   UNREFERENCED_PARAMETER(filterKey);
   UNREFERENCED_PARAMETER(filter);

   //DbgPrint("TrackMyInternet in %s\n", __FUNCTION__);
   return STATUS_SUCCESS;
}

NTSTATUS
TLInspectTransportNotify(
   _In_ FWPS_CALLOUT_NOTIFY_TYPE notifyType,
   _In_ const GUID* filterKey,
   _Inout_ const FWPS_FILTER* filter
   )
{
   UNREFERENCED_PARAMETER(notifyType);
   UNREFERENCED_PARAMETER(filterKey);
   UNREFERENCED_PARAMETER(filter);

//   DbgPrint("TrackMyInternet in %s\n", __FUNCTION__);
   return STATUS_SUCCESS;
}
