#pragma once
#include <ntddk.h>
#include <wdf.h>

#pragma warning(push)
#pragma warning(disable:4201)       // unnamed struct/union

#include <fwpsk.h>

#pragma warning(pop)

#include <fwpmk.h>

#include <ws2ipdef.h>
#include <in6addr.h>
#include <ip2string.h>

#include "inspect.h"
#include "public.h"
#include "jhash.h"

#define FIREWALL_MAX_COUNTRY (256*256)
#define APP_HASHTABLE_SIZE 128
#define APP_BUCKET_SIZE_MAX 128

void firewall_init();
void firewall_destroy();
void firewall_geoip_block(PCHAR country_iso);
void firewall_geoip_unblock(PCHAR country_iso);
int firewall_geoip_isblocked(PCHAR country_iso);
PUCHAR firewall_fill_blocked_countries(UINT32 *len);

typedef struct _blocked_app {
	USHORT len;
	char *app;
}blocked_app;

typedef struct _list_block_app_entry {
	LIST_ENTRY listEntry;
	blocked_app *app;
}list_block_app_entry;

void firewall_app_block(PCHAR appname, USHORT len);
void firewall_app_unblock(PCHAR appname, USHORT len);
int firewall_app_isblocked(PCHAR appname, USHORT len);
PUCHAR firewall_fill_blocked_apps(UINT32 *len);

static inline void firewall_util_lowercase(PWCHAR string, USHORT len) {
	if (!string) {
		return;
	}

	for (int i = 0; i < len; i++) {
		string[i] = RtlDowncaseUnicodeChar(string[i]);
	}
}
