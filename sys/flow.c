#pragma warning(disable: 4201) 
#include <fwpsk.h>

#include "flow.h"
#include "inspect.h"
#include "jhash.h"
#include "geoip.h"
#include "firewall.h"

#define HASHTABLE_SIZE 8192
#define BUCKET_SIZE_MAX 128

static LIST_ENTRY gFlowTable[HASHTABLE_SIZE];
static KSPIN_LOCK gFlowBucketLock[HASHTABLE_SIZE];
static UINT32 gFlowBucketSize[HASHTABLE_SIZE];

int flow_add_new(LIST_ENTRY *bucket, TL_INSPECT_PENDED_PACKET *packet, UINT32 hash, UINT32 *sid);
int flow_update(flow *f, TL_INSPECT_PENDED_PACKET *packet);

static inline void flow_process_app(flow *f, TL_INSPECT_PENDED_PACKET *packet) {
	if (packet->processPath != NULL) {
		if (firewall_app_isblocked((PCHAR)packet->processPath->data,(USHORT) packet->processPath->size)) {
			packet->flags |= FLAG_BLOCKED_APP;
			f->flags |= FLAG_BLOCKED_APP;
		}
	}
}

static inline void flow_process_geoip(flow *f, TL_INSPECT_PENDED_PACKET *packet) {
	if (geoip_configured() && f->flags & FLAG_NEED_GEOIP) {
		// perform geoipLookup
		geoip_entry_v4 *geoip_entry = geoip_find_v4(packet->ipv4RemoteAddr);
		f->flags &= ~FLAG_NEED_GEOIP;
		if (geoip_entry) {
			f->flags |= FLAG_GEOIP_OK;
			memcpy(f->country_iso, geoip_entry->country_iso, sizeof(f->country_iso));
			memcpy(f->country_name, geoip_entry->country_name, sizeof(f->country_name));
			if (firewall_geoip_isblocked(f->country_iso)) {
				f->flags |= FLAG_BLOCKED_COUNTRY;
				packet->flags |= FLAG_BLOCKED_COUNTRY;
				DbgPrint("TrackMyInternet: %s is blocked\n", f->country_iso);
			}
		}
		else {
			f->flags |= FLAG_GEOIP_FAIL;
		}
	}
}
void init_flows() {
	int i;	
	for (i = 0; i < HASHTABLE_SIZE; i++) {
		InitializeListHead(&gFlowTable[i]);
		KeInitializeSpinLock(&gFlowBucketLock[i]);
		gFlowBucketSize[i] = 0;
	}
}


void destroy_flows() {
	KIRQL irql1;
	PLIST_ENTRY listEntry;
	flow_entry *entry;

	for (int i = 0; i < HASHTABLE_SIZE; i++) {

		KeAcquireSpinLock(&gFlowBucketLock[i], &irql1);
		PLIST_ENTRY bucket = &gFlowTable[i];

		while (!IsListEmpty(bucket)) {

			listEntry = RemoveHeadList(bucket);
			gFlowBucketSize[i]--;

			entry = CONTAINING_RECORD(
				listEntry,
				flow_entry,
				listEntry
				);

			ExFreePoolWithTag(entry->f, 'WOLF');
			ExFreePoolWithTag(entry, 'WOLF');
		}
		KeReleaseSpinLock(&gFlowBucketLock[i], irql1);
	}
}

#define JHASH_SEED 0xd00db00b

UINT32 hash_packet(TL_INSPECT_PENDED_PACKET *packet, UINT32 *sid) {
	UINT32 hash = 0;
	if (packet->addressFamily == AF_INET) {
		sid[0] = packet->ipv4LocalAddr;
		sid[1] = packet->ipv4RemoteAddr;
		sid[2] = (packet->localPort << 16) + packet->remotePort;
		hash = jhash2(sid, 3, JHASH_SEED);
	}

	return hash;
}

void flow_process_netbuffers(flow *flow, TL_INSPECT_PENDED_PACKET *packet) {
	if (!packet) {
		return;
	}

	PNET_BUFFER_LIST net_buffer_list = packet->netBufferList;
	PNET_BUFFER net_buffer;
		
	while (net_buffer_list) {
		net_buffer = net_buffer_list->FirstNetBuffer;
		struct tcphdr *pTCPHeader;
		ULONG nblOffset;
		NDIS_STATUS ndisStatus;


		// do extra processing for TCP
		if (packet->protocol == IPPROTO_TCP && packet->addressFamily == AF_INET) {
			//
			// For inbound net buffer list, we can assume it contains only one 
			// net buffer.
			//
			net_buffer = NET_BUFFER_LIST_FIRST_NB(packet->netBufferList);
			nblOffset = NET_BUFFER_DATA_OFFSET(net_buffer);

			//
			// The TCP/IP stack could have retreated the net buffer list by the 
			// transportHeaderSize amount; detect the condition here to avoid
			// retreating twice.
			//
			UINT32 transportHeaderSize = packet->transportHeaderSize;

			if (nblOffset != packet->nblOffset)
			{				
			//	transportHeaderSize = 0;
			}
			
			//
			// Adjust the net buffer list offset to the start of the TCP header.
			//
			if (transportHeaderSize > 0) {
				ndisStatus = NdisRetreatNetBufferDataStart(
					net_buffer,
					transportHeaderSize,
					0,
					NULL);
			}
			
			// Fetch Ethernet Header
			pTCPHeader = (struct tcphdr*)NdisGetDataBuffer(
				net_buffer,
				sizeof(struct tcphdr),
				NULL, // No storage provided or needed
				1, // No alignment requirement
				0);

			// flow ended
			if (pTCPHeader && (pTCPHeader->th_flags & (TH_FIN | TH_RST)) != 0) {
				flow->flow_ended = 1;
			}

			if (transportHeaderSize > 0) {
				//
				// Undo the adjustment on the original net buffer list.
				//
				NdisAdvanceNetBufferDataStart(
					net_buffer,
					transportHeaderSize,
					FALSE,
					NULL
					);
			}
		}

		if (net_buffer) {
			if (packet->direction == FWP_DIRECTION_INBOUND) {
				flow->totalRx += net_buffer->DataLength;
			}
			else {
				flow->totalTx += net_buffer->DataLength;
			}
		}

		net_buffer_list = net_buffer_list->Next;
	}
	
}

int flow_add(TL_INSPECT_PENDED_PACKET *packet) {
	UINT32 sid[3];
	if (!packet) {
		return -1;;
	}
	UINT32 hash = hash_packet(packet, sid);
	UINT32 hash_index = hash % HASHTABLE_SIZE;
	KIRQL irql1;
	LIST_ENTRY *listEntry;
	flow_entry *flowEntry;
	flow *f;
	
	KeAcquireSpinLock(&gFlowBucketLock[hash_index], &irql1);
	PLIST_ENTRY bucket = &gFlowTable[hash_index];
	
	for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink)
	{
		flowEntry = CONTAINING_RECORD(
			listEntry,
			flow_entry,
			listEntry
			);

		f = flowEntry->f;
		if (!memcmp(sid, f->sid, sizeof(sid))) {
			// match
			// update stats
			flow_process_geoip(f, packet);
			flow_process_app(f, packet);
			flow_update(f, packet);
			
			KeReleaseSpinLock(&gFlowBucketLock[hash_index], irql1);
			//DbgPrint("Updated flow\n");
			return 0;
		}

	}
	

	if (gFlowBucketSize[hash_index] < BUCKET_SIZE_MAX && flow_add_new(bucket, packet, hash, sid) == 0) {
		gFlowBucketSize[hash_index]++;
	}

	KeReleaseSpinLock(&gFlowBucketLock[hash_index], irql1);

	return 0;
}

int flow_update(flow *f, TL_INSPECT_PENDED_PACKET *packet) {
	if (!f) {
		return -1;
	}

	//always update time
	f->time.QuadPart = packet->time.QuadPart;


	flow_process_netbuffers(f, packet);
	if (packet->direction == FWP_DIRECTION_INBOUND) {
		f->totalPacketRx++;
	}
	else {
		f->totalPacketTx++;
	}

	f->flags |= packet->flags;


	return 0;
}

int flow_add_new(LIST_ENTRY *bucket, TL_INSPECT_PENDED_PACKET *packet, UINT32 hash, UINT32 *sid) {


	flow *f = ExAllocatePoolWithTag(NonPagedPool, sizeof(flow), 'WOLF');

	if (!f) {
		return -1;
	}

	flow_entry *entry = ExAllocatePoolWithTag(NonPagedPool, sizeof(flow_entry), 'WOLF');

	if (!entry) {
		ExFreePoolWithTag(f, 'WOLF');
		return -1;
	}

	entry->f = f;
	
	memset(f, 0, sizeof(flow));
	memcpy(f->sid, sid, sizeof(f->sid));
	f->hash = hash;
	f->flow_ended = 0;
	// set last packet time
	f->time.QuadPart = packet->time.QuadPart;
	// set first packet time
	f->start_time.QuadPart = packet->time.QuadPart;

	f->interfaceIndex = packet->interfaceIndex;
	f->totalTx = 0;
	f->totalRx = 0;	
	f->totalPacketRx = f->totalPacketTx = 0;

	if (packet->direction == FWP_DIRECTION_INBOUND) {
		f->totalPacketRx++;
	}
	else {
		f->totalPacketTx++;
	}
	
	if (packet->processId != 0) {
		f->processId = packet->processId;
	}

	flow_process_netbuffers(f, packet);

	f->addressFamily= packet->addressFamily;
	f->protocol = packet->protocol;

	memcpy(&f->localAddr, &packet->localAddr, sizeof(FWP_BYTE_ARRAY16));

	memcpy(&f->remoteAddr, &packet->remoteAddr, sizeof(FWP_BYTE_ARRAY16));

	f->localPort = packet->localPort;
	f->remotePort = packet->remotePort;
	f->flags = packet->flags | FLAG_NEED_GEOIP;

	flow_process_geoip(f, packet);	
	flow_process_app(f, packet);

	InsertTailList(bucket, &entry->listEntry);
	return 0;
}

size_t flow_get(PUCHAR buffer, size_t length) {
	LIST_ENTRY* listEntry;
	flow_entry *entry;
	KIRQL irql1;
	size_t written = 0;

	for (int i = 0; i < HASHTABLE_SIZE && (length - written > sizeof(flow)); i++) {	
				
		KeAcquireSpinLock(&gFlowBucketLock[i], &irql1);
		PLIST_ENTRY bucket = &gFlowTable[i];

		while (length - written > sizeof(flow) && !IsListEmpty(bucket)) {

			listEntry = RemoveHeadList(bucket);
			gFlowBucketSize[i]--;

			entry = CONTAINING_RECORD(
				listEntry,
				flow_entry,
				listEntry
				);

			memcpy(buffer + written, entry->f, sizeof(flow));

			written += sizeof(flow);
			
			ExFreePoolWithTag(entry->f, 'WOLF');
			ExFreePoolWithTag(entry, 'WOLF');
		}
		
		KeReleaseSpinLock(&gFlowBucketLock[i], irql1);
	}


	return written;
}