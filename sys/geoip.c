#include "geoip.h"

volatile SHORT configured = 0;

#define GEOIP_HASHTABLE_SIZE (16384*2)
#define GEOIP_BUCKET_SIZE_MAX (4096)

static LIST_ENTRY gGeoipTable[GEOIP_HASHTABLE_SIZE];
static UINT32 gGeoipBucketSize[GEOIP_HASHTABLE_SIZE];

#define JHASH_SEED 0xbeef1337

static geoip_entry_v4 LAN = { 0, 0, "LL", "Local Area Network" };

UINT32 stats_geoip_max_bucket_size = 0;

int geoip_configured() {
	return InterlockedCompareExchange16(&configured, 0, 0);
}

void geoip_config_done() {
	InterlockedExchange16(&configured, 1);
}

void geoip_reset_config() {
	InterlockedExchange16(&configured, 0);
	geoip_destroy();
	geoip_init();
}

static inline UINT32 hash_geoip(UINT32 network) {
	return jhash_1word(network, JHASH_SEED);
}

void geoip_init() {
	int i;
	for (i = 0; i < GEOIP_HASHTABLE_SIZE; i++) {
		InitializeListHead(&gGeoipTable[i]);
		gGeoipBucketSize[i] = 0;
	}
	stats_geoip_max_bucket_size = 0;
	InterlockedExchange16(&configured, 0);
}


void geoip_destroy() {
	PLIST_ENTRY listEntry;
	list_geoip_entry_v4 *entry;

	for (int i = 0; i < GEOIP_HASHTABLE_SIZE; i++) {

		PLIST_ENTRY bucket = &gGeoipTable[i];

		while (!IsListEmpty(bucket)) {

			listEntry = RemoveHeadList(bucket);
			gGeoipBucketSize[i]--;

			entry = CONTAINING_RECORD(
				listEntry,
				list_geoip_entry_v4,
				listEntry
				);			
			ExFreePoolWithTag(entry->v4, 'IOEG');
			ExFreePoolWithTag(entry, 'IOEG');
		}

	}

}


int geoip_add_new(PLIST_ENTRY bucket, geoip_entry_v4 *_entry) {
	if (!_entry) {
		return -1;
	}

	list_geoip_entry_v4 *list_entry = ExAllocatePoolWithTag(NonPagedPool, sizeof(list_geoip_entry_v4), 'IOEG');

	if (!list_entry) {
		return -1;
	}

	geoip_entry_v4 *entry = ExAllocatePoolWithTag(NonPagedPool, sizeof(geoip_entry_v4), 'IOEG');
	if (!list_entry) {
		ExFreePoolWithTag(list_entry, 'IOEG');
		return -1;
	}

	memcpy(entry, _entry, sizeof(geoip_entry_v4));

	list_entry->v4 = entry;
	//DbgPrint("Added net:  network: %08x, netmask: %08x \n", list_entry->v4->network, list_entry->v4->netmask);

	InsertTailList(bucket, &list_entry->listEntry);

	return 0;
}



void geoip_add_v4(geoip_entry_v4 *entry) {
	UINT32 hash = hash_geoip(entry->network);
	UINT32 hash_index = hash % GEOIP_HASHTABLE_SIZE;
	LIST_ENTRY *listEntry;
	list_geoip_entry_v4 *geoipEntry;
	geoip_entry_v4 *entry_v4;

	PLIST_ENTRY bucket = &gGeoipTable[hash_index];

	for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink)
	{
		geoipEntry = CONTAINING_RECORD(
			listEntry,
			list_geoip_entry_v4,
			listEntry
			);

		entry_v4 = geoipEntry->v4;
		if (entry_v4->network == entry->network && entry_v4->netmask == entry_v4->netmask) {
			DbgPrint("Entry already in list:network: %08x, netmask: %08x, country: %s\n", entry_v4->network, entry_v4->netmask, entry_v4->country_name);
			// already in list
			return;
		}

	}

	if (gGeoipBucketSize[hash_index] < GEOIP_BUCKET_SIZE_MAX && geoip_add_new(bucket, entry) == 0) {
		//DbgPrint("Added flow\n");
		gGeoipBucketSize[hash_index]++;
		if (stats_geoip_max_bucket_size < gGeoipBucketSize[hash_index]) {
			stats_geoip_max_bucket_size = gGeoipBucketSize[hash_index];
		}
	}
	
}



int cidr_match_v4(UINT32 addr, UINT32 net, UINT32 netmask) {
	if (netmask == 0) {
		return (addr == net);
	}
	return (addr & netmask) == (net & netmask);
}

geoip_entry_v4 *geoip_find_v4(UINT32 ipv4) {
	geoip_entry_v4 *entry_v4 = NULL;
	UINT32 mask = 0;
	UINT32 network = 0;
	
	ipv4 = RtlUlongByteSwap(ipv4);

	// avoid lookup for local traffic
	if (cidr_match_v4(ipv4, 0x7f000000, 0xff000000)) {
		return &LAN;
	}

	if (cidr_match_v4(ipv4, 0xc0a80000, 0xffff0000)) {
		return &LAN;
	}

	if (cidr_match_v4(ipv4, 0x0a000000, 0xff000000)) {
		return &LAN;
	}


	// try different masks
	for (UINT32 i = 0; i < 32; i++) {		

		mask = ~(~((UINT32)(0)) >> (32 - i));
		network = ipv4 & mask;
		
		UINT32 hash = hash_geoip(network);
		UINT32 hash_index = hash % GEOIP_HASHTABLE_SIZE;
		LIST_ENTRY *listEntry;
		list_geoip_entry_v4 *geoipEntry;

		PLIST_ENTRY bucket = &gGeoipTable[hash_index];

		//DbgPrint("Trying ip: %08x, network: %08x\n", ipv4, network);
		for (listEntry = bucket->Flink; listEntry != bucket; listEntry = listEntry->Flink)
		{
			geoipEntry = CONTAINING_RECORD(
				listEntry,
				list_geoip_entry_v4,
				listEntry
				);

			entry_v4 = geoipEntry->v4;
			if (cidr_match_v4(ipv4, entry_v4->network, entry_v4->netmask)) {
				//DbgPrint("Matched ip: %08x, network: %08x, netmask: %08x, country: %s\n", ipv4, entry_v4->network, entry_v4->netmask, entry_v4->country_name);
				return entry_v4;
			}
		}	
		
	}

	return NULL;
}