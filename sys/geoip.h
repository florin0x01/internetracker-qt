#pragma once
#include <ntddk.h>
#include <wdf.h>

#pragma warning(push)
#pragma warning(disable:4201)       // unnamed struct/union

#include <fwpsk.h>

#pragma warning(pop)

#include <fwpmk.h>

#include <ws2ipdef.h>
#include <in6addr.h>
#include <ip2string.h>

#include "inspect.h"
#include "public.h"
#include "jhash.h"

typedef struct _list_geoip_entry_v4 {
	LIST_ENTRY listEntry;
	geoip_entry_v4* v4;
}list_geoip_entry_v4;


void geoip_init();
void geoip_destroy();
void geoip_reset_config();
void geoip_config_done();
void geoip_add_v4(geoip_entry_v4 *entry);
geoip_entry_v4 *geoip_find_v4(UINT32 ipv4);

int geoip_configured();

extern UINT32 stats_geoip_max_bucket_size;