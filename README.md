TrackMyInternet APP
======================================

1. Install driver: right click the .inf file and click Install
2. Start driver: sc start TrackMyInternet
3. Make sure the GeoIP DB is in the tmilib_test.exe working directory (same folder).
4. Run tmilib_test to see all connections / run tmilib_test PID to see connections of a specific process

