#include "stdafx.h"
#include "TMIFlowExt.h"


TMIFlowExt::TMIFlowExt():TMIFlow() {
}


TMIFlowExt::~TMIFlowExt() {
}

void TMIFlowExt::Update(flow *f, flow_ext_v1 *ext) {
	TMIFlow::Update(f);
	UpdateExt(ext);
}

void TMIFlowExt::Update(flow *f) {
	TMIFlow::Update(f);
	flow_ext_v1 *ext = (flow_ext_v1*)((char*)f + sizeof(flow));
	UpdateExt(ext);	
}

void TMIFlowExt::UpdateExt(flow_ext_v1 *ext) {
	process_path = ext->process_path;
	process_name = "";
	auto i = process_path.find_last_of('\\');
	if (i != string::npos) {
		process_name = process_path.substr(i + 1);
	}
	
	start.QuadPart = ext->start.QuadPart;
	end.QuadPart = ext->end.QuadPart;
	last_action.QuadPart = ext->end.QuadPart;	
	
	double intervalMs = NS_TO_MS(end.QuadPart - start.QuadPart);
	
	if (intervalMs == 0.0) {
		intervalMs = 1000.0;
	}

	speedStats.uplinkBps = _flow.totalTx / (intervalMs / 1000.0);
	speedStats.downlinkBps = _flow.totalRx / (intervalMs / 1000.0);
	db_id = ext->db_id;
	groupByCount = ext->count;
}


