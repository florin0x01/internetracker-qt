#pragma once
#include "stdafx.h"
#include <memory>
#include <unordered_map>
#include <set>
#include <map>
#include <algorithm>    // std::sort
#include <vector>
#include "datasource.h"

class DBClient;
using namespace std;

// 100NS intervals to MS
#define NS_TO_MS(ns) ((ns)/(10 * 1000))
#define MS_TO_S(ms) ((ms)/1000)
#define NS_TO_S(ns) MS_TO_S(NS_TO_MS(ns))
#define NS_TO_H(ns) MS_TO_S(NS_TO_MS((ns) / 3600))

#define S_TO_NS(s) (((UINT64)s) * 10000000)
#define H_TO_NS(s) (((UINT64)s) * 10000000*3600)

static inline time_t nanoseconds1600_to_time_t(LARGE_INTEGER time) {
	time_t start = time.QuadPart;	
	start = MS_TO_S(NS_TO_MS(start - 116444736000000000));	
	return start;
}

#define B_TO_KB(b) ((b)/1024)
#define FLOW_BUFFER_SIZE 512
#define FLOW_AGE_INTERVAL 5000 /*every 5 second*/
#define FLOW_DB_DUMP_INTERVAL 10000 /*every 10 second*/
#define FLOW_AGE_INACTIVE_MAX 60000 /*1 min*/

#define FLOW_TICK_INTERVAL 1000 /* every 1 second*/
#define FLOW_TICK_BUFFER_SIZE 5 /* keep 5 second history for speeds*/

struct DeltaStats {
	UINT32 packetsRx;
	UINT32 packetsTx;
	UINT64 bytesRx;
	UINT64 bytesTx;
};

// holds time and rx/tx 1 second avg instant speed
struct TroughputStats {
	UINT64 downlinkBps;
	UINT64 uplinkBps;
	LARGE_INTEGER time;
};

struct SpeedStats {
	UINT64 _uplinkBps;
	UINT64 _downlinkBps;	
	UINT64 lastBytesTx;
	UINT64 lastBytesRx;	
	LARGE_INTEGER lastUpdate;

	UINT64 uplinkBps;
	UINT64 downlinkBps;
};

struct TmiSettings
{
	static double MapTransferKBs;
	static double TreeUpdateMsec;
	static double MapSpeedKBps;
	static double MapUpdateMs;
};

class TMIFlow {
	public: 
		TMIFlow();
		virtual ~TMIFlow();

	    virtual void Update(flow *f);		
		
		DWORD GetPID() { return (DWORD)_flow.processId; }
		
		// in bytes
		UINT64 GetFlowRx() { return _flow.totalRx; };
		UINT64 GetFlowTx() { return _flow.totalTx; };

		UINT32 GetFlowPacketsRx() { return _flow.totalPacketRx;  }
		UINT32 GetFlowPacketsTx() { return _flow.totalPacketTx; }

		UINT32 _GetLocalAddr() { return ntohl(_flow.ipv4LocalAddr); }
		UINT32 _GetRemoteAddr() { return ntohl(_flow.ipv4RemoteAddr); }

		UINT16 GetLocalPort() { return ntohs(_flow.localPort); }
		UINT16 GetRemotePort() { return ntohs(_flow.remotePort); }

		string GetLocalAddr() { return local_addr_string; }
		string GetRemoteAddr() { return remote_addr_string; }

		string GetProcessName() const { return process_name; }
		string GetProcessPath() const { return process_path; }
		string GetProcessFullPath() const { return process_full_path; }

		string GetTransportProtocol() const { return transport_protocol; }

		UINT8 GetIPPROTO() { return _flow.protocol; }

		// in milliseconds
		UINT64 GetDuration();

		LARGE_INTEGER GetStartTime() const { return start; }
		LARGE_INTEGER GetLastActionTime() const { return last_action; }

		UINT64 GetActiveDuration() {
			return NS_TO_MS(last_action.QuadPart - start.QuadPart);
		};

		bool   IsFlowClosed() { return _flow.flow_ended == 1; }

		void   End(LARGE_INTEGER time) { end.QuadPart = time.QuadPart; }
		
		bool   IsFlowAged(PLARGE_INTEGER time) { 			
			return NS_TO_MS(time->QuadPart - last_action.QuadPart) >= FLOW_AGE_INACTIVE_MAX;
		}

		bool   IsClient() { return (_flow.flags & FLAG_CLIENT) != 0; }
		bool   IsServer() { return (_flow.flags & FLAG_SERVER) != 0; }

		string GetCountry() { return country_name;  }
		string GetCountryISO() { return country_iso_name; }
		string hash;		

		void *user_data;

		void SetCountry(string country) { country_name = country; }

		DeltaStats getDeltaStats() const { return deltaStats; }
		SpeedStats getSpeedStats() const { return speedStats; }

	
		UINT64 GetAvgSpeedRx() {
			UINT64 duration_s = MS_TO_S(GetActiveDuration());
			UINT64 totalBytesRx = GetFlowRx();
			if (!duration_s) {
				return 0;
			}
			return totalBytesRx / duration_s;
		}

		UINT64 GetAvgSpeedTx() {
			UINT64 duration_s = MS_TO_S(GetActiveDuration());
			UINT64 totalBytesTx = GetFlowTx();
			if (!duration_s) {
				return 0;
			}
			return totalBytesTx / duration_s;
		}

		DataSource *dataSource;
		INT64 getDbID() const { return db_id; }
		UINT32 getDbGroupByCount() const { return groupByCount; }

	protected:		
		string process_name;
		string process_path;
		string process_full_path;
		string transport_protocol;

		string country_name;
		string country_iso_name;
		string local_addr_string;
		string remote_addr_string;

		void Reset();
		flow _flow;
		LARGE_INTEGER start;
		LARGE_INTEGER last_action;
		LARGE_INTEGER end;

		friend class TMIControl;
		DeltaStats deltaStats;
		SpeedStats speedStats;
		vector<TroughputStats> speed;
				
		// DB client stuff
		friend class DBClient;
		INT64 db_id;
		UINT32 groupByCount;
		LARGE_INTEGER lastDBUpdate;

};

class TMIProcessCallback {
	public:
		enum UpdateType {
			None,
			LiveUpdate,
			OfflineUpdate,
		};

		// Implement permanent storage in DB for this flow (called on flow age or end (TCP RST/FIN))
		virtual void OnFlowStore(shared_ptr<TMIFlow> f) = NULL;
		// flow update (called when a flow is updated) - flow contains up to date data
		virtual void OnFlowUpdate(shared_ptr<TMIFlow> f) = NULL;
		// flow age
		virtual void OnFlowAged(shared_ptr<TMIFlow> f) = NULL;
		// called to update the flow's uplink downlink speed
		virtual void OnFlowSpeed(shared_ptr<TMIFlow> f) = NULL;


		// non mandatory callbacks
		// called when data source is changed, the registered client receiving this callback should reset to default state (clear tree)
		virtual void OnDataSourceChanged(UpdateType type) {}

		UpdateType type = None;
		
};

class TMIMapper {
public:
	TMIMapper(TMIProcessCallback::UpdateType type) :type(type) {}
	~TMIMapper();
	
	TMIProcessCallback::UpdateType type;

	unordered_map <string, shared_ptr<TMIFlow>> fm; // by hex(sid)
	unordered_map <DWORD, unordered_map <string, shared_ptr<TMIFlow>>> fm_p;
	unordered_map <string, unordered_map <string, shared_ptr<TMIFlow>>> fm_c;
};

class TMIControl {
	public:
		static TMIControl& Instance()
		{
			static TMIControl inst;
			return inst;
		}

		virtual ~TMIControl();

		bool OpenDriver();				
		int Process();		
		vector <shared_ptr<TMIFlow>>  GetLiveSnapshot() { return GetSnapshot(TMIProcessCallback::LiveUpdate); }
		vector <shared_ptr<TMIFlow>>  GetOfflineSnapshot() { return GetSnapshot(TMIProcessCallback::OfflineUpdate); }
		vector <shared_ptr<TMIFlow>>  GetSnapshot(TMIProcessCallback::UpdateType type); // all
		vector <shared_ptr<TMIFlow>>  GetLiveSnapshot(DWORD pid); // by PID
		vector <shared_ptr<TMIFlow>>  GetLiveSnapshot(string country); // by ISO COUNTRY NAME		
		
		// full path is needed for app blocker
		static CHAR* GetProcessPath(UINT64 pid, string &fullPath);		

		void switchDataSource(TMIProcessCallback::UpdateType type);
		int setDataSourceFilter(string filter, string count_query = "");

		void RegisterClient(TMIProcessCallback *cb) {
			clients.push_back(cb);
		}

		void UnregisterClietnt(TMIProcessCallback *cb) {
			auto iterator = find(clients.begin(), clients.end(), cb);
			if (iterator != clients.end()) {
				clients.erase(iterator);
			}
		}

		UINT64 GetTotalBytesRX() const { return totalBytesRx; }
		UINT64 GetTotalBytesTX() const { return totalBytesTx; }
		UINT32 GetDownlinkSpeed() const { return downlinkSpeed; }
		UINT32 GetUplinkSpeed() const { return uplinkSpeed; }

		map<string, double>  GetSnapshotCountriesPercentages(TMIProcessCallback::UpdateType, map<string,string>& processes, const map<string, bool>& inclusions);
		map<string, double>  GetSnapshotCountriesPerTransfer(TMIProcessCallback::UpdateType, map<string, string>& processes, const map<string, bool>& inclusions);

		DataSource *getDataSource() const { return dataSource; }
		LiveSource *getLiveDataSource() const { return liveDataSource; }
		DBSource *getOfflineDataSource() const { return dbDataSource; }

		shared_ptr<TMIFlow> getOldestFlow();		

	protected:
		int Process(DataSource *dataSource);
		shared_ptr<TMIFlow> currentFlow = NULL;
		void OnFlowStore(int updateType);
		void OnFlowUpdate(int updateType);
		void OnFlowAged(int updateType);
		void OnFlowSpeed(int updateType);
		void OnDataSourceChanged(int updateType);

		

	private:
		friend class DBSource;
		friend class LiveSource;
		TMIControl();
		DBClient *dbClient;
		DataSource *dataSource;
		
		LiveSource *liveDataSource;
		DBSource *dbDataSource;

		flow *flow_buffer;
		vector <TMIProcessCallback *> clients;

		void ProcessAging();
		void UpdateGlobalStats(flow &f);
		void RemoveFlow(shared_ptr<TMIFlow> flow);
		void UpdateMaps(TMIMapper &mapper, shared_ptr<TMIFlow> flow);
				
		LARGE_INTEGER last_aging;

		unordered_map<int, TMIMapper *> maps;
		TMIMapper *liveMap;
		TMIMapper *offlineMap;

		LARGE_INTEGER lastTick;
		void FlowTick(int updateType);

		void FlowSpeedAddPoint(shared_ptr<TMIFlow> flow, LARGE_INTEGER time);

		UINT64 totalBytesRx;
		UINT64 totalBytesTx;
		UINT32 downlinkSpeed;
		UINT32 uplinkSpeed;
};

VOID __stdcall DoStartSvc();
VOID __stdcall DoStopSvc();