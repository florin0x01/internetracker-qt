#include "stdafx.h"
#include <fstream>
#include <iterator>
#include <algorithm>
#include <ctime>
#include <sstream>
#include "datasource.h"
#include "sqlite3.h"
#include "tmi.h"
#include "../sys/cmd.h"
#include "CSVReader.h"


static PUCHAR saveGeoipBin(UINT32 &buffer_size);

DataSource::DataSource()
{	
}


DataSource::~DataSource()
{
}


LiveSource::LiveSource() {
	UINT32 buffersize = 0;
	driver_available = false;
	DoStartSvc();	
}

LiveSource::~LiveSource() {
	if (hDevice != INVALID_HANDLE_VALUE) {
		CloseHandle(hDevice);
	}

	DoStopSvc();
}

int LiveSource::getUpdateType() {
	return TMIProcessCallback::LiveUpdate;
}

vector <string> LiveSource::getBlockedApps() {
	vector<string> block_list;
	driver_command_v1 list_cmd;
	string buffer;
	list_cmd.cmd = CMD_LIST_BLOCKED_APPS;
	list_cmd.data_size = 0;

	if (write(string((char*)&list_cmd, sizeof(list_cmd))) == sizeof(list_cmd)) {
		read(buffer, 256);

		if (!buffer.size()) {
			return block_list;
		}

		PCHAR reply_buffer = (PCHAR)buffer.c_str();
		blocked_apps *hdr = (blocked_apps *)reply_buffer;		
		reply_buffer += sizeof(blocked_apps);

		for (int i = 0; i < hdr->count; i++) {			
			blocked_app_reply *app = (blocked_app_reply *)reply_buffer;
			reply_buffer += sizeof(blocked_app_reply);
			char *char_buffer = new char[app->size/2];
			int ret = wcstombs(char_buffer, (wchar_t*)reply_buffer, app->size);

			block_list.push_back(string(char_buffer, app->size/2));
			reply_buffer += app->size;
		}
	}

	return block_list;
}

void LiveSource::blockApp(string path, int cmd) {	
	// remove terminator, as the driver expects none
	if (path.at(path.size() - 1) == 0) {
		path = path.substr(0, path.size() - 1);
	}

	// convert path to wide string
	int wchars_num = MultiByteToWideChar(CP_UTF8, 0, path.c_str(), -1, NULL, 0);
	wchar_t* wstr = new wchar_t[wchars_num];
	MultiByteToWideChar(CP_UTF8, 0, path.c_str(), -1, wstr, wchars_num);

	UCHAR *buffer = new UCHAR[sizeof(driver_command_v1) + wchars_num * sizeof(wchar_t)];
	driver_command_v1 *block_cmd = (driver_command_v1 *)buffer;	
	UCHAR *wpath = buffer + sizeof(driver_command_v1);

	block_cmd->cmd = cmd;
	block_cmd->data_size = wchars_num * sizeof(wchar_t);			
	memcpy(wpath, wstr, block_cmd->data_size);
	
	write(string((char*)buffer, block_cmd->data_size + sizeof(driver_command_v1)));
	
	delete[] wstr;
	delete[] buffer;
}

void LiveSource::unBlockApp(string path) {
	blockApp(path, CMD_UNBLOCK_APP);
}


vector <string> LiveSource::getBlockedCountries() {
	vector<string> block_list;
	driver_command_v1 list_cmd;
	string buffer;
	list_cmd.cmd = CMD_LIST_BLOCKED_COUNTRIES;
	list_cmd.data_size = 0;

	if (write(string((char*)&list_cmd, sizeof(list_cmd))) == sizeof(list_cmd)) {
		read(buffer, 0);
		PCHAR reply_buffer = (PCHAR)buffer.c_str();
		blocked_countries *hdr = (blocked_countries *)reply_buffer;
		block_country *country_list = (block_country *)(reply_buffer + sizeof(blocked_countries));

		for (int i = 0; i < hdr->count; i++, country_list++) {
			block_list.push_back(country_list->country_iso);
		}
	}

	return block_list;
}

void LiveSource::blockCountry(string iso, int cmd) {
	UCHAR buffer[sizeof(driver_command_v1) + sizeof(block_country)];

	driver_command_v1 *block_cmd = (driver_command_v1 *)buffer;
	block_country *country = (block_country *)(buffer + sizeof(driver_command_v1));
	block_cmd->cmd = cmd;
	block_cmd->data_size = sizeof(block_country);
	
	memcpy(country->country_iso, iso.c_str(), 2);
	write(string((char*)buffer, sizeof(buffer)));
}

void LiveSource::unblockCountry(string iso) {
	blockCountry(iso, CMD_UNBLOCK_COUNTRY);
}

bool LiveSource::open() {
	hDevice = CreateFile(DEVICE_NAME,
		GENERIC_READ | GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_FLAG_NO_BUFFERING,
		NULL);

	if (hDevice == INVALID_HANDLE_VALUE) {
		printf("Error: CreatFile Failed : %d\n", GetLastError());
		driver_available = false;
		return false;
	}

	driver_available = true;
	return driver_available;
}

int LiveSource::setFilter(string filter) {
	return -1;
}

int LiveSource::write(string &buffer) {
	DWORD bytesWritten = 0;
	if (!WriteFile(hDevice, buffer.c_str(), buffer.size(), &bytesWritten, NULL)) {		
		return -1;
	}

	return bytesWritten;
}

int LiveSource::read(string &buffer, int maxElements) {	
	DWORD bytesRead = 0;
	if (maxElements == 0) {
		maxElements = 128;
	}
	char *tmpBuffer = new char[maxElements * sizeof(flow)];

	if (!ReadFile(hDevice, tmpBuffer , maxElements * sizeof(flow), &bytesRead, NULL)) {
		printf("Error: ReadFile failed with error 0x%x\n", GetLastError());
		delete tmpBuffer;
		return -1;
	}

	buffer.assign(tmpBuffer, bytesRead);
	delete tmpBuffer;

	return bytesRead / sizeof(flow);
}

static PUCHAR loadGeoipBin(UINT32 &buffer_size) {
	std::ifstream input("geoip.bin", std::ios::binary);

	if (!input.is_open()) {
		return NULL;
	}
	std::streampos init_pos = input.tellg();
	input.seekg(0, std::ios::end);
	buffer_size = input.tellg() - init_pos;
	
	input.seekg(0, std::ios::beg);

	PUCHAR buffer = (PUCHAR) malloc(buffer_size);

	input.read((char*)buffer, buffer_size);

	input.close();
	return buffer;
}



static PUCHAR saveGeoipBin(UINT32 &buffer_size) {
	// GeoIP CSV maps
	vector< pair<string, UINT32>> networks;
	unordered_map<UINT32, pair<string, string>> network_countries;

	try {
		io::CSVReader<2> in("geoip_ipv4.csv");
		in.read_header(io::ignore_extra_column, "network", "registered_country_geoname_id");
		std::string network; int id;
		while (in.read_row(network, id)) {
			// do stuff with the data
			networks.push_back(pair<string, UINT32>(network, id));
		}
	} catch (io::error::can_not_open_file err) {
		return NULL;
	}


	try {
		io::CSVReader<3> in2("geoip_ipv4_countries.csv");
		in2.read_header(io::ignore_extra_column, "geoname_id", "country_iso_code", "country_name");

		int geoname_id;
		string country_iso;
		string country_name;
		while (in2.read_row(geoname_id, country_iso, country_name)) {
			// do stuff with the data
			network_countries[geoname_id] = pair<string, string>(country_iso, country_name);
		}
	}
	catch (io::error::can_not_open_file err) {
		return NULL;
	}

	std::ofstream output("geoip.bin", std::ios::binary);

	if (!output.is_open()) {
		return NULL;
	}


	vector <geoip_entry_v4 *> geoip_entries;

	for (int i = 0; i < networks.size(); i++) {
		geoip_entry_v4 *entry = new geoip_entry_v4;
		memset(entry, 0, sizeof(geoip_entry_v4));

		string netmask = networks[i].first;
		string bits = netmask.substr(netmask.find('/') + 1);
		netmask = netmask.substr(0, netmask.find('/'));

		UINT32 network32 = inet_addr(netmask.c_str());
		entry->network = htonl(network32);
		entry->netmask = ~(~((UINT32)(0)) >> stoi(bits));
				
		string country_iso = network_countries[networks[i].second].first;
		string country_name = network_countries[networks[i].second].second;
		strncpy(entry->country_iso, country_iso.c_str(), 2);
		strncpy(entry->country_name, country_name.c_str(), sizeof(entry->country_name) - 1);
		geoip_entries.push_back(entry);		
	}

	buffer_size = geoip_entries.size() * sizeof(geoip_entry_v4) + sizeof(driver_command_v1) + sizeof(geoip_config);
	PUCHAR buffer = (PUCHAR)malloc(buffer_size);

	driver_command_v1 *cmd = (driver_command_v1 *)buffer;
	cmd->cmd = CMD_CONFIG_GEOIP;
	cmd->data_size = buffer_size - sizeof(driver_command_v1);

	geoip_config *config = (geoip_config *)(buffer + sizeof(driver_command_v1));
	config->entry_count = geoip_entries.size();
	config->version = std::time(nullptr);

	PUCHAR data = buffer + sizeof(driver_command_v1) + sizeof(geoip_config);

	for (int i = 0; i < geoip_entries.size(); i++, data += sizeof(geoip_entry_v4)) {
		memcpy(data, geoip_entries[i], sizeof(geoip_entry_v4));
	}

	output.write((char*)buffer, buffer_size);
	output.close();

	return buffer;
}

void LiveSource::config_geoip() {
	UINT32 buffer_size = 0;
	PUCHAR buffer = loadGeoipBin(buffer_size);
	DWORD written = 0;

	if (!buffer) {
		buffer = saveGeoipBin(buffer_size);
	}

	if (buffer) {
		if (!WriteFile(hDevice, buffer, buffer_size, &written, NULL)) {
			free(buffer);
			return;
		}
	}
	free(buffer);
}

void LiveSource::sync() {
	TMIControl &control = TMIControl::Instance();
	// feed all current map items to the listeners
	auto flows = control.GetLiveSnapshot();
	for (auto it = flows.begin(); it != flows.end(); it++) {
		control.currentFlow = *it;
		control.OnFlowUpdate(getUpdateType());
	}
}

// these are static
std::thread DBSource::workerThread;
std::mutex DBSource::workerMutex;
//std::mutex DBSource::dbMutex;

DBSource::DBSource() {
	done = true;

}

DBSource::~DBSource() {
	if (workerThread.joinable()) {
		workerThread.join();
	}

	sqlite3_close_v2(db);
	sqlite3_close_v2(dbSync);
}


bool iequals(const string& a, const string& b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}
/*
"`id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,"
"`hash`	VARCHAR(12) NOT NULL,"
"`start`	INTEGER NOT NULL,"
"`end`	INTEGER,"
"`ipProtocol`	SMALLINT,"
"`remoteIP`	INTEGER,"
"`localIP`	INTEGER,"
"`remotePort`	SMALLINT,"
"`localPort`	SMALLINT,"
"`remoteHost`	VARCHAR(256),"
"`packetsRx`	INTEGER,"
"`packetsTx`	INTEGER,"
"`bytesRx`	INTEGER,"
"`bytesTx`	INTEGER,"
"`app`	TEXT,"
"`country_iso`	VARCHAR(2),"
"`country`	VARCHAR(64)"

*/
void deserialize_sql(int argc, char **argv, char **azColName, flow &f, flow_ext_v1 &ext) {
	memset(&f, 0, sizeof(f));
	memset(&ext, 0, sizeof(ext));
	ext.db_id = -1;

	for (int i = 0; i < argc; i++) {

		// lazy loading support, this is the number of entries and if it greater than 1 it signals that there are more items to load
		if (iequals(azColName[i], "groupByCount")) {
			ext.count = strtoull(argv[i], NULL, 10);
			continue;
		}

		if (iequals(azColName[i], "id")) {
			ext.db_id = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "start")) {
			ext.start.QuadPart = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "end")) {
			ext.end.QuadPart = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "ipProtocol")) {
			f.protocol = atoi(argv[i]);
			continue;
		}
		if (iequals(azColName[i], "remoteIP")) {
			f.ipv4RemoteAddr = htonl(strtoul(argv[i], NULL, 10));
			continue;
		}
		if (iequals(azColName[i], "localIP")) {
			f.ipv4LocalAddr = htonl(strtoul(argv[i], NULL, 10));
			continue;
		}
		if (iequals(azColName[i], "remotePort")) {
			f.remotePort = htons(atoi(argv[i]));
			continue;
		}
		if (iequals(azColName[i], "localPort")) {
			f.localPort = htons(atoi(argv[i]));
			continue;
		}
		if (iequals(azColName[i], "packetsRx")) {
			f.totalPacketRx = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "packetsTx")) {
			f.totalPacketTx = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "bytesRx")) {
			f.totalRx = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "bytesTx")) {
			f.totalTx = strtoull(argv[i], NULL, 10);
			continue;
		}
		if (iequals(azColName[i], "app")) {
			strncpy_s(ext.process_path, argv[i], sizeof(ext.process_path));
			continue;
		}
		if (iequals(azColName[i], "country_iso")) {
			strncpy_s(f.country_iso, argv[i], sizeof(f.country_iso) - 1);
			continue;
		}
		if (iequals(azColName[i], "country")) {
			strncpy_s(f.country_name, argv[i], sizeof(f.country_name) - 1);
			continue;
		}
	}

	f.sid[0] = f.ipv4LocalAddr;
	f.sid[1] = f.ipv4RemoteAddr;
	f.sid[2] = (f.localPort << 16) + f.remotePort;

	f.flags |= FLAG_FLOW_EXT;

	f.processId = 0xdeadbeef;

}


int DBSource::callback_async(void *param, int argc, char **argv, char **azColName) {
	DBSource *dbs = (DBSource*)param;
	flow f;
	flow_ext_v1 ext;

	deserialize_sql(argc, argv, azColName, f, ext);
	
	workerMutex.lock();

	// async query
	dbs->flows.push(pair<flow, flow_ext_v1>(f, ext));

	workerMutex.unlock();

	return 0;
}

int DBSource::callback_sync(void *param, int argc, char **argv, char **azColName) {
	queue < pair < flow, flow_ext_v1>> *syncQueue = (queue < pair < flow, flow_ext_v1>> *)param;
	flow f;
	flow_ext_v1 ext;

	deserialize_sql(argc, argv, azColName, f, ext);
	
	syncQueue->push(pair<flow, flow_ext_v1>(f, ext));

	return 0;
}

int DBSource::worker() {
	string error;
	_executeQuery(query, error);
	done = true;
	return 0;
}


int DBSource::setFilterSync(string filter, queue < pair < flow, flow_ext_v1>> &flows) {
	string error;	
	_executeQuerySync(filter, error, flows);

	return flows.size();
}

bool DBSource::executeQuery(string query) {
	if (!done) {
		workerThread.join();
	}

	done = false;
	this->query = query;
	
	// clear flows
	this->flows = queue < pair < flow, flow_ext_v1>>();
	if (workerThread.joinable()) {
		workerThread.join();
	}

	workerThread = std::thread(&DBSource::worker, this);

	return true;
}

bool DBSource::_executeQuerySync(string query, string &error, queue < pair < flow, flow_ext_v1>> &flows) {
	int rc;
	char *zErrMsg = 0;

	//dbMutex.lock();

	rc = sqlite3_exec(dbSync, query.c_str(), callback_sync, &flows, &zErrMsg);
	if (rc != SQLITE_OK) {
		if (zErrMsg) {
			error = zErrMsg;
			sqlite3_free(zErrMsg);
		}
		//dbMutex.unlock();
		return false;
	}

	//dbMutex.unlock();
	return true;
}

// execute query on a specified thread
bool DBSource::_executeQuery(string query, string &error) {
	int rc;
	static int running = 0;
	char *zErrMsg = 0;

	rc = sqlite3_exec(db, query.c_str(), callback_async, this, &zErrMsg);
	if (rc != SQLITE_OK) {
		if (zErrMsg) {
			error = zErrMsg;
			sqlite3_free(zErrMsg);
		}
		return false;
	}

	return true;
}

int DBSource::count(string query) {
	int count;
	sqlite3_stmt *res;
	int rc;

	rc = sqlite3_prepare_v2(db, query.c_str(), -1, &res, 0);

	if (rc != SQLITE_OK) {
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);

		return 1;
	}

	rc = sqlite3_step(res);

	if (rc == SQLITE_ROW) {
		count = strtoul((const char*)sqlite3_column_text(res, 0), NULL, 10);
	}

	sqlite3_finalize(res);
	return count;
}

void DBSource::sync() {
	TMIControl &tmi = TMIControl::Instance();

	// we need to clear maps in case of DB feed
	tmi.maps[getUpdateType()]->fm.clear();
	tmi.maps[getUpdateType()]->fm_c.clear();
	tmi.maps[getUpdateType()]->fm_p.clear();
}

bool DBSource::open() {
	string error;
	int result = sqlite3_open_v2("file:trackmyinternet.db", &db, SQLITE_OPEN_URI | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL);
	result = sqlite3_open_v2("file:trackmyinternet.db", &dbSync, SQLITE_OPEN_URI | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL);
	return result == 0;
}

int DBSource::setFilter(string filter) {
	return executeQuery(filter);
}

int DBSource::read(string &buffer, int maxElements) {
	int i;
	maxElements = 128;
	if (workerMutex.try_lock()) {
		for (i = 0; i < maxElements && !flows.empty(); i++) {
			flow f = flows.front().first;
			flow_ext_v1 ext = flows.front().second;
			buffer.append(string((char*)&f, sizeof(f)));
			buffer.append(string((char*)&ext, sizeof(ext)));
			flows.pop();
		}
		workerMutex.unlock();
		return i;
	}

	return 0;
}

int DBSource::getUpdateType() {
	return TMIProcessCallback::OfflineUpdate;
}



DBFilterBuilder::~DBFilterBuilder() {

}

DBFilterBuilder &DBFilterBuilder::getInstance() {
	static DBFilterBuilder instance;
	return instance;
}

string DBFilterBuilder::getFilter() {
	stringstream ss;
	/*
	sprintf(buffer, "Select *,count(id) as groupByCount, sum(bytesRx) as bytesRx, sum(bytesTx) as bytesTx, sum(packetsRx) as packetsRx, sum(packetsTx) as packetsTx from flows "
	" where (app LIKE '%%%s%%' or country LIKE '%%%s%%' or remoteIP_str LIKE '%%%s%%') "
	"group by app, country_iso ORDER by start", text.toStdString().c_str(), text.toStdString().c_str(), text.toStdString().c_str());
	*/
	
	ss << "Select *, count(id) as groupByCount, sum(bytesRx) as bytesRx, sum(bytesTx) as bytesTx, sum(packetsRx) as packetsRx, sum(packetsTx) as packetsTx from flows ";
	//ss << " where (start > " << timeRef - S_TO_NS(timeWindow) << " and end < " << timeRef + S_TO_NS(timeWindow) << ")";
	ss << " where (start > " << timeRef - S_TO_NS(timeWindow) << " and start < " << timeRef + S_TO_NS(timeWindow) << ")";
	ss << " and (app LIKE '%" << searchCrit << "%' or country LIKE '%" << searchCrit << "%' or remoteIP_str LIKE '%" << searchCrit << "%') ";
	ss << " group by app, country_iso ORDER by start";

	return ss.str();
}

void DBFilterBuilder::setTimeRef(UINT64 time) {
	timeRef = time;
}

void DBFilterBuilder::setSearchCrit(string searchCrit) {
	this->searchCrit = searchCrit;
}

// in seconds
void DBFilterBuilder::setTimeWindow(UINT32 window) {
	timeWindow = window;
}


DBFilterBuilder::DBFilterBuilder() {

}
