// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _WINSOCK_DEPRECATED_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1

#include <winsock2.h>
#include "targetver.h"
#include <DriverSpecs.h>
_Analysis_mode_(_Analysis_code_type_user_code_)

#pragma warning(disable:4201)  // nameless struct/union
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <strsafe.h>
#include <tchar.h>
#include <basetsd.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winioctl.h>



typedef unsigned char UCHAR;
typedef unsigned short USHORT;
typedef unsigned long ULONG;

typedef struct FWP_BYTE_ARRAY16_
{
	UINT8 byteArray16[16];
} 	FWP_BYTE_ARRAY16;


#pragma warning(disable:4201)       // unnamed struct/union
#include "../sys/public.h"


// TODO: reference additional headers your program requires here
