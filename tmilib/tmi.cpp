#include "stdafx.h"
#include "tmi.h"
#pragma warning(disable:4201)       // unnamed struct/union
#include "../sys/public.h"
#include "Psapi.h"
#include "../TrackMyInternet/Utils.h"
#include "datasource.h"
#include "dbclient.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include "TMIFlowExt.h"
#include "CSVReader.h"

using namespace std;

#define GEOIP_DB "GeoLite2-City.mmdb"



double TmiSettings::MapTransferKBs = 512; //1024 MB max
double TmiSettings::TreeUpdateMsec = 100;
double TmiSettings::MapSpeedKBps = 512 ; //1 MB max
double TmiSettings::MapUpdateMs = 500;


struct CountrySort
{
	int operator()(const std::shared_ptr<TMIFlow>& a, const std::shared_ptr<TMIFlow>& b) {
		UINT64 left_throughput = a->getSpeedStats().downlinkBps + a->getSpeedStats().uplinkBps;
		UINT64 right_throughput = b->getSpeedStats().downlinkBps + b->getSpeedStats().uplinkBps;

		return left_throughput > right_throughput;
	}
};


map<string, double> TMIControl::GetSnapshotCountriesPercentages(TMIProcessCallback::UpdateType type, map<string,string>& processes, const map<string, bool>& inclusions)
{
	map<string, double> res;

	vector<shared_ptr<TMIFlow>> shot = GetSnapshot(type);
	if (shot.size() == 0) {
		return res;
	}

	// make 2MB max speed
	UINT64 max = TmiSettings::MapSpeedKBps * 1024; //bytes
	
	if (inclusions.size() == 0)
	{
		return res;
	}

	for (int i = 0; i < shot.size(); i++)
	{
		shared_ptr<TMIFlow> elemenet = shot[i];

		if (elemenet->country_name == "Unknown")
			continue;

		if (inclusions.find(elemenet->process_name) == inclusions.end())
			continue;

		processes.insert(make_pair(elemenet->process_path, elemenet->process_name));

		UINT64 cur = elemenet->getSpeedStats().downlinkBps + elemenet->getSpeedStats().uplinkBps;
		double result = (((double)cur / (double)max));
		res[elemenet->GetCountryISO()]+= result;
	}

	return res;
}

void TMIControl::switchDataSource(TMIProcessCallback::UpdateType type) {
	if (dataSource && type == dataSource->getUpdateType()) {
		return;
	}

	if (type == TMIProcessCallback::LiveUpdate) {
		dataSource = liveDataSource;
	}
	else {
		dataSource = dbDataSource;
	}
	
	OnDataSourceChanged(type);
	
	// do specific stuff when synchronizing
	dataSource->sync();

}

//db data source supported only
int TMIControl::setDataSourceFilter(string filter, string count_query) {
	int count = 0;

	if (count_query != "") {
		count = dbDataSource->count(count_query);
	}

	dbDataSource->setFilter(filter);

	return count;
}

map<string, double> TMIControl::GetSnapshotCountriesPerTransfer(TMIProcessCallback::UpdateType type, map<string,string>  & processes, const map<string, bool>& inclusions)
{
	map<string, double> res;

	vector<shared_ptr<TMIFlow>> shot = GetSnapshot(type);
	if (shot.size() == 0) {
		return res;
	}

	// make 1024MB max transfered
	UINT64 max = TmiSettings::MapTransferKBs * 1024; //bytes
	bool inclusionsActive = false;

	if (inclusions.size() == 0)
	{
		return res;
	}

	for (int i = 0; i < shot.size(); i++)	
	{
		shared_ptr<TMIFlow> elemenet = shot[i];
		if (elemenet->country_name == "Unknown")
			continue;

		if (inclusions.find(elemenet->process_name) == inclusions.end())
			continue;

		processes.insert(make_pair(elemenet->process_path,elemenet->process_name));

		UINT64 cur = elemenet->GetFlowRx() + elemenet->GetFlowTx();
		
		double result = (((double)cur / (double)max));
		res[elemenet->GetCountryISO()]+= result;
	}

	return res;
}


TMIControl::TMIControl()
{
	last_aging.QuadPart = 0;
	totalBytesRx = totalBytesTx = 0;
	downlinkSpeed = uplinkSpeed = 0;

	
	/*int status = MMDB_open(GEOIP_DB, MMDB_MODE_MMAP, &mmdb);

	if (MMDB_SUCCESS != status) {
		fprintf(stderr, "\n  Can't open %s - %s\n", GEOIP_DB, MMDB_strerror(status));

		if (MMDB_IO_ERROR == status) {
			fprintf(stderr, "    IO error: %d\n", errno);
		}
		
	}
	else {
		geoip_available = true;
	}*/

	memset(&lastTick, 0, sizeof(lastTick));

	dataSource = NULL;
	dbClient = new DBClient();
	liveDataSource = new LiveSource();
	dbDataSource = new DBSource();
	
	liveMap = new TMIMapper(TMIProcessCallback::LiveUpdate);
	offlineMap = new TMIMapper(TMIProcessCallback::OfflineUpdate);

	maps[liveMap->type] = liveMap;
	maps[offlineMap->type] = offlineMap;


	//also open the db data source
	dbDataSource->open();

}


TMIControl::~TMIControl()
{
	delete liveMap;
	delete offlineMap;

	delete liveDataSource;
	delete dbDataSource;
}

bool TMIControl::OpenDriver() {	
	switchDataSource(TMIProcessCallback::LiveUpdate);	
	if (dataSource->open()) {
		liveDataSource->config_geoip();
		//liveDataSource->blockApp("\\device\\HardDiskvolume1\\program files (x86)\\mozilla firefox\\firefox.exe");
		//liveDataSource->blockApp("\\device\\HardDiskvolume1\\program files (x86)\\mozilla firefox\\firefox2.exe");
		//liveDataSource->blockApp("\\device\\HardDiskvolume1\\program files (x86)\\mozilla firefox\\firefox3.exe");
		//liveDataSource->unBlockApp("\\device\\HardDiskvolume1\\program files (x86)\\mozilla firefox\\firefox3.exe");
		//liveDataSource->blockCountry("CN");
		//liveDataSource->blockCountry("RO");
		return true;
	}

	return false;
}

TMIFlow::TMIFlow() {
	Reset();
}

void TMIFlow::Reset() {
	memset(&_flow, 0, sizeof(flow));
	start.QuadPart = 0;
	last_action.QuadPart = 0;
	lastDBUpdate.QuadPart = 0;
	end.QuadPart = 0;
	local_addr_string = remote_addr_string = process_name = "";
	user_data = NULL;
	memset(&deltaStats, 0, sizeof(deltaStats));
	memset(&speedStats, 0, sizeof(speedStats));		
	db_id = -1;
	groupByCount = 0;
	dataSource = NULL;
}

TMIFlow::~TMIFlow() {
	
}

CHAR* GetDriveLetter(LPSTR lpDevicePath)
{
	CHAR d = 'A';
	static char buffer[MAX_PATH*4];
	while (d <= 'Z')
	{
		CHAR szDeviceName[3] = { d,(':'),('\0') };
		CHAR szTarget[512] = { 0 };
		if (QueryDosDevice(szDeviceName, szTarget, 511) != 0)
			if (!memcmp(lpDevicePath, szTarget, strlen(szTarget))) {
				strncpy_s(buffer, szDeviceName, MAX_PATH*4);
				strncat_s(buffer, lpDevicePath + strlen(szTarget), MAX_PATH * 4);
				return buffer;
			}
				
		d++;
	}
	return NULL;
}
CHAR* TMIControl::GetProcessPath(UINT64 pid, string &fullPath) {
	static CHAR Buffer[MAX_PATH] = "";
	static CHAR Buffer2[MAX_PATH] = "";
	HANDLE Handle = OpenProcess(
		PROCESS_QUERY_LIMITED_INFORMATION,
		FALSE,
		(DWORD)pid);

	if (Handle)	{		
		if (GetProcessImageFileName(Handle, Buffer, MAX_PATH)) {
			// At this point, buffer contains the full path to the executable
			fullPath = Buffer;
			CloseHandle(Handle);
			CHAR *goodPathname = GetDriveLetter(Buffer);			
			if (goodPathname) {
				return goodPathname;
			}

			return Buffer;				
		} else {			
			return NULL;
		}		
	}
	else {
		DWORD err = GetLastError();
		printf("Unable to open process: %u, err %d\n", (DWORD)pid, err);
	}

	return NULL;
}

void TMIFlow::Update(flow *f) {	
	if (start.QuadPart == 0) {
		// copy all fields
		memcpy(&_flow, f, sizeof(flow));

		switch (f->protocol) {
		case IPPROTO_UDP:
			transport_protocol = "UDP";
			break;
		case IPPROTO_TCP:
			transport_protocol = "TCP";
			break;
		case IPPROTO_ICMP:
			transport_protocol = "ICMP";
			break;
		default:
			transport_protocol = "other";
			break;
		}

		IN_ADDR addr;
		addr.S_un.S_addr = f->ipv4LocalAddr;
		local_addr_string = inet_ntoa(addr);

		addr.S_un.S_addr = f->ipv4RemoteAddr;
		remote_addr_string = inet_ntoa(addr);

		// set start time
		start.QuadPart = f->start_time.QuadPart;				
	} else {
		// update stats
		_flow.totalPacketRx += f->totalPacketRx;
		_flow.totalPacketTx += f->totalPacketTx;
		_flow.totalRx += f->totalRx;
		_flow.totalTx += f->totalTx;
	}

	last_action.QuadPart = f->time.QuadPart;	

	// update delta stats
	deltaStats.bytesRx = f->totalRx;
	deltaStats.bytesTx = f->totalTx;
	deltaStats.packetsRx = f->totalPacketRx;
	deltaStats.packetsTx = f->totalPacketTx;

	if (f->flow_ended) {
		end.QuadPart = f->time.QuadPart;
	}


	if (f->processId != 0 && process_path == "") {
		// setup process information

		CHAR *_process_name = TMIControl::GetProcessPath(_flow.processId, process_full_path);
				
		if (_process_name && strlen(_process_name)) {
			process_path = _process_name;

			CHAR *baseName = strrchr(_process_name, '\\');
			if (baseName) {
				process_name = string(baseName + 1);
			}
			else {
				process_name = string(_process_name);
			}
			
		}
		else {
			char buff[100];
			snprintf(buff, sizeof(buff), "[%u] Unavailable", (UINT32)_flow.processId);
			std::string buffAsStdStr = buff;
			process_name =  buff;
		}
		
	}

}

vector <shared_ptr<TMIFlow>> TMIControl::GetSnapshot(TMIProcessCallback::UpdateType type) {
	vector <shared_ptr<TMIFlow>> flows;

	flows.clear();
	for (auto it = maps[type]->fm.begin(); it != maps[type]->fm.end(); ++it) {
		if (it->second) {
			flows.push_back(it->second);
		}
	}		
	return flows;
}

vector <shared_ptr<TMIFlow>> TMIControl::GetLiveSnapshot(DWORD pid) {
	vector <shared_ptr<TMIFlow>> flows;
	
	flows.clear();
	for (auto it = maps[TMIProcessCallback::LiveUpdate]->fm_p[pid].begin(); it != maps[TMIProcessCallback::LiveUpdate]->fm_p[pid].end(); ++it) {
		if (it->second) {
			flows.push_back(it->second);
		}
	}
	return flows;
}


vector <shared_ptr<TMIFlow>> TMIControl::GetLiveSnapshot(string country) {
	vector <shared_ptr<TMIFlow>> flows;

	flows.clear();
	for (auto it = maps[TMIProcessCallback::LiveUpdate]->fm_c[country].begin(); it != maps[TMIProcessCallback::LiveUpdate]->fm_c[country].end(); ++it) {
		if (it->second) {
			flows.push_back(it->second);
		}
	}
	return flows;
}


static void parseBuffer(PUCHAR readBuf, ULONG bytesRead) {
	for (UINT32 i = 0; i < bytesRead / sizeof(flow); i++) {
		char proto[32];
		flow *f = (flow*)readBuf;
		readBuf += sizeof(flow);

		IN_ADDR addr1;
		addr1.S_un.S_addr = f->ipv4LocalAddr;
		IN_ADDR addr2;
		addr2.S_un.S_addr = f->ipv4RemoteAddr;


		switch (f->protocol) {
		case IPPROTO_UDP:
			strcpy_s(proto, "UDP");
			break;
		case IPPROTO_TCP:
			strcpy_s(proto, "TCP");
			break;
		case IPPROTO_ICMP:
			strcpy_s(proto, "ICMP");
			break;
		default:
			strcpy_s(proto, "other");
			break;
		}

		char buf1[128], buf2[128];

		strcpy_s(buf1, inet_ntoa(addr1));
		strcpy_s(buf2, inet_ntoa(addr2));

		printf("[%llu] %s %s:%hu <-> %s:%hu [%s] rx: %u tx:%u, rxBytes:%llu txBytes:%llu\n", f->processId, f->flow_ended ? "END" : "", buf1, ntohs(f->localPort), buf2, ntohs(f->remotePort), proto, f->totalPacketRx, f->totalPacketTx, f->totalRx, f->totalTx);
	}
}

void TMIControl::UpdateMaps(TMIMapper &mapper, shared_ptr<TMIFlow> flow) {
	mapper.fm[flow->hash] = flow;
	mapper.fm_p[flow->GetPID()][flow->hash] = flow;
	mapper.fm_c[flow->country_iso_name][flow->hash] = flow;
}

void TMIControl::OnFlowStore(int updateType) {
	for (auto i = 0; i < clients.size(); i++) {
		if (clients[i]->type == updateType) {
			clients[i]->OnFlowStore(currentFlow);
		}		
	}
}

void TMIControl::OnFlowUpdate(int updateType) {
	for (auto i = 0; i < clients.size(); i++) {
		if (clients[i]->type == updateType) {
			clients[i]->OnFlowUpdate(currentFlow);
		}
	}

}
void TMIControl::OnFlowAged(int updateType) {
	for (auto i = 0; i < clients.size(); i++) {
		if (clients[i]->type == updateType) {
			clients[i]->OnFlowAged(currentFlow);
		}
	}

}
void TMIControl::OnFlowSpeed(int updateType) {
	for (auto i = 0; i < clients.size(); i++) {
		if (clients[i]->type == updateType) {
			clients[i]->OnFlowSpeed(currentFlow);
		}
	}
}

void TMIControl::OnDataSourceChanged(int updateType) {
	for (auto i = 0; i < clients.size(); i++) {
		clients[i]->OnDataSourceChanged((TMIProcessCallback::UpdateType) updateType);
	}
}

void TMIControl::UpdateGlobalStats(flow &f) {

	// update global RX/TX
	totalBytesRx += f.totalRx;
	totalBytesTx += f.totalTx;

}
shared_ptr<TMIFlow> TMIControl::getOldestFlow() {
	queue < pair < flow, flow_ext_v1>> flows;
	shared_ptr<TMIFlowExt> flow	= make_shared<TMIFlowExt>();
	
	if (dbDataSource) {
		dbDataSource->setFilterSync("SELECT * from flows ORDER by start asc limit 1", flows);
		if (flows.size() == 1) {
			flow->Update(&flows.front().first, &flows.front().second);
		}
	}	
	
	return flow;
}


int TMIControl::Process(DataSource *dataSource) {
	DWORD bytesRead = 0;
	FILETIME _time;
	LARGE_INTEGER time;
	string buffer;
	GetSystemTimeAsFileTime(&_time);

	time.LowPart = _time.dwLowDateTime;
	time.HighPart = _time.dwHighDateTime;

	int elements = dataSource->read(buffer, FLOW_BUFFER_SIZE);


	const char *ptr = buffer.c_str();
	flow *flow_buffer = (flow*)ptr;

	int i;
	uint16_t offset = 0;
	for (i = 0; i < elements; i++, ptr += offset) {
		flow_buffer = (flow*)ptr;
		flow_ext_v1 *ext = NULL;
		offset = sizeof(flow);
		
		// hide display of blocked connections for now
		if (flow_buffer->flags & FLAG_BLOCKED_COUNTRY) {
			continue;
		}

		// discard flows with src == dst (this is loopback interface traffic only)
		if (!memcmp(&flow_buffer->localAddr, &flow_buffer->remoteAddr, sizeof(flow_buffer->localAddr))) {
			if (flow_buffer->flags & FLAG_FLOW_EXT) {
				offset += sizeof(flow_ext_v1);
			}
			continue;
		}

		if (flow_buffer->flags & FLAG_FLOW_EXT) {
			ext = (flow_ext_v1*)(ptr + offset);
		}

		string hash = HashFlow(flow_buffer, ext);
		shared_ptr<TMIFlow> flow = maps[dataSource->getUpdateType()]->fm[hash];

		UpdateGlobalStats(*flow_buffer);
		// only create new flows for known process ids excluding system(4)
		if (!flow && (flow_buffer->processId != 0 && flow_buffer->processId != 4)) {
			
			// if flow has extension flag set, create a TMIFlowExt
			if (flow_buffer->flags & FLAG_FLOW_EXT) {
				currentFlow = flow = make_shared<TMIFlowExt>();
				offset += sizeof(flow_ext_v1);
			}
			else {
				currentFlow = flow = make_shared<TMIFlow>();
			}			
			flow->dataSource = dataSource;

			flow->Update(flow_buffer);
			flow->hash = hash;			
			if (strlen(flow_buffer->country_iso)) {
				flow->country_iso_name = flow_buffer->country_iso;
				flow->country_name = flow_buffer->country_name;
			}
			else {
				flow->country_iso_name = "N/A";
				flow->country_name = "Unknown";
			}
			UpdateMaps(*maps[dataSource->getUpdateType()],flow);
			// notify client
			OnFlowUpdate(dataSource->getUpdateType());

			if (flow_buffer->flow_ended) {
				RemoveFlow(flow);
				OnFlowStore(dataSource->getUpdateType());
			}
		}
		else if (flow) {
			currentFlow = flow;
			flow->Update(flow_buffer);
			if (flow_buffer->flags & FLAG_FLOW_EXT) {
				offset += sizeof(flow_ext_v1);
			}

			// notify client
			OnFlowUpdate(dataSource->getUpdateType());
			if (flow->IsFlowClosed()) {
				RemoveFlow(flow);
				OnFlowStore(dataSource->getUpdateType());
			}
		}
	}

	return elements;
}

// returns number of flows processed
int TMIControl::Process() {
	int ret = 0;

	ProcessAging();

	ret += Process(dbDataSource);
	ret += Process(liveDataSource);

	FlowTick(liveDataSource->getUpdateType());

	//auto apps = liveDataSource->getBlockedApps();

	return ret;
}

UINT64 TMIFlow::GetDuration() {
	if (end.QuadPart > 0) {
		return NS_TO_MS(end.QuadPart - start.QuadPart);
	}
	else {
		FILETIME _time;
		LARGE_INTEGER time;

		GetSystemTimeAsFileTime(&_time);
		time.LowPart = _time.dwLowDateTime;
		time.HighPart = _time.dwHighDateTime;

		return NS_TO_MS(time.QuadPart - start.QuadPart);
	}
	
};

template<typename T>
void pop_front(std::vector<T>& vec)
{
	if (vec.size() > 0) {
		vec.erase((vec.begin() + 0));
	}
}
void TMIControl::FlowSpeedAddPoint(shared_ptr<TMIFlow> flow, LARGE_INTEGER time) {
	if (!flow) {
		return;
	}
	
	SpeedStats &stats = flow->speedStats; 
	
	TroughputStats point;
	point.downlinkBps = stats._downlinkBps;
	point.uplinkBps = stats._uplinkBps;
	point.time = time;

	flow->speed.push_back(point);

	if (flow->speed.size() == FLOW_TICK_BUFFER_SIZE) {
		pop_front(flow->speed);
	}

	stats.downlinkBps = 0;
	stats.uplinkBps = 0;

	for (auto i = 0; i < flow->speed.size(); i++) {
		stats.downlinkBps += flow->speed[i].downlinkBps;
		stats.uplinkBps += flow->speed[i].uplinkBps;
	}

	stats.downlinkBps /= flow->speed.size();
	stats.uplinkBps /= flow->speed.size();

}

// generate OnFlowSpeed updates for non idle flows
void TMIControl::FlowTick(int updateType) {
	FILETIME _time;
	LARGE_INTEGER time;

	GetSystemTimeAsFileTime(&_time);

	time.LowPart = _time.dwLowDateTime;
	time.HighPart = _time.dwHighDateTime;

	if (lastTick.QuadPart == 0) {
		lastTick.QuadPart = time.QuadPart;
	}

	UINT64 intervalMs = NS_TO_MS(time.QuadPart - lastTick.QuadPart);
	if (intervalMs < FLOW_TICK_INTERVAL) {
		return;
	}

	lastTick.QuadPart = time.QuadPart;

	for (auto it = maps[updateType]->fm.begin(); it != maps[updateType]->fm.end(); ++it) {
		shared_ptr<TMIFlow> f = it->second;
		if (!f) {
			continue;
		}

		currentFlow = f;

		// check if flow is idle
		if (f->speedStats.lastBytesRx == f->GetFlowRx() && f->speedStats.lastBytesTx == f->GetFlowTx()) {
			f->speedStats._downlinkBps = 0;
			f->speedStats._uplinkBps = 0;	
		}
		else {
			f->speedStats._downlinkBps = (f->GetFlowRx() - f->speedStats.lastBytesRx) / (intervalMs / 1000);
			f->speedStats._uplinkBps = (f->GetFlowTx() - f->speedStats.lastBytesTx) / (intervalMs / 1000);
			f->speedStats.lastBytesRx = f->GetFlowRx();
			f->speedStats.lastBytesTx = f->GetFlowTx();			
		}
		FlowSpeedAddPoint(f, time);

		OnFlowSpeed(updateType);
	}



}

void TMIControl::RemoveFlow(shared_ptr<TMIFlow> flow) {
	if (flow) {
		TMIProcessCallback::UpdateType dataSourceType =(TMIProcessCallback::UpdateType) flow->dataSource->getUpdateType();
		maps[dataSourceType]->fm.erase(flow->hash);
		maps[dataSourceType]->fm_p[flow->GetPID()].erase(flow->hash);
		maps[dataSourceType]->fm_c[flow->country_iso_name].erase(flow->hash);
		//delete flow;
	}
}

void TMIControl::ProcessAging(){
	FILETIME _time;
	LARGE_INTEGER time;	
	vector <shared_ptr<TMIFlow>> remove_list;
	
	GetSystemTimeAsFileTime(&_time);
	
	time.LowPart = _time.dwLowDateTime;
	time.HighPart = _time.dwHighDateTime;
	
	if (last_aging.QuadPart == 0) {
		last_aging.QuadPart = time.QuadPart;
	}

	if (NS_TO_MS(time.QuadPart - last_aging.QuadPart) < FLOW_AGE_INTERVAL) {
		return;
	}
	

	last_aging.QuadPart = time.QuadPart;
	
	//last_aging = GetTickCount64()
	for (auto it = maps[TMIProcessCallback::LiveUpdate]->fm.begin(); it != maps[TMIProcessCallback::LiveUpdate]->fm.end(); ++it) {
		shared_ptr<TMIFlow> f = it->second;
		if (!f) {
			continue;
		}
		currentFlow = f;

		if (f->IsFlowAged(&time)) {			
			f->End(time);
			OnFlowAged(TMIProcessCallback::LiveUpdate);
			remove_list.push_back(f);			
		}
	}

	for (auto i = 0; i < remove_list.size(); i++) {
		currentFlow = remove_list[i];
		RemoveFlow(remove_list[i]);
		OnFlowStore(TMIProcessCallback::LiveUpdate);
	}
}


TMIMapper::~TMIMapper() {
}
