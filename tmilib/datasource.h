#pragma once
#include <string>
#include "sqlite3.h"
#include <queue>
#include <thread>
#include <mutex>
#include "../sys/public.h"
#include "../sys/cmd.h"
using namespace std;

class TMIFlow;
class DataSource
{
public:
	DataSource();
	~DataSource();

	virtual bool open() = 0;
	// async variant
	virtual int setFilter(string filter) = 0;
	// blocking variant
	virtual int setFilterSync(string filter, queue < pair < flow, flow_ext_v1>> &flows) { return 0;  };

	virtual int read(string &buffer, int maxElements) = 0;

	virtual string getName() { return "invalid"; }
	virtual int getUpdateType() = 0;
	
	// no if items available for read
	virtual int count(string query) const { return 0; }
		
	// reset and sync datasource
	virtual void sync() {}
};

class DBFilterBuilder {
	public:
		~DBFilterBuilder();

		static DBFilterBuilder &getInstance();
		string getFilter();

		void setTimeRef(UINT64 time);
		void setSearchCrit(string searchCrit);

		// in seconds
		void setTimeWindow(UINT32 window);

	private: 
		DBFilterBuilder();
		string filter;
		UINT64 timeRef;
		UINT32 timeWindow;
		string searchCrit;		
};

class DBSource : public DataSource {
public:
	DBSource();
	~DBSource();
	virtual bool open();
	virtual int setFilter(string filter);
	virtual int read(string &buffer, int maxElements);
	virtual int setFilterSync(string filter, queue < pair < flow, flow_ext_v1>> &flows);

	virtual int getUpdateType();
	virtual string getName() { return "Archive"; }
	virtual void sync();
	virtual int count(string query);

private:	
	bool executeQuery(string query);

	bool _executeQuery(string query, string &error);
	bool _executeQuerySync(string query, string &error, queue < pair < flow, flow_ext_v1>> &flows);

	static int callback_async(void *NotUsed, int argc, char **argv, char **azColName);
	static int callback_sync(void *NotUsed, int argc, char **argv, char **azColName);

	sqlite3 *db = NULL;
	
	// use this for sync queries
	sqlite3 *dbSync = NULL;

	queue < pair < flow, flow_ext_v1>> flows;

	int worker();
	static std::thread workerThread;
	static std::mutex workerMutex;

	static std::mutex dbMutex;

	bool done;
	std::string query;
};

class LiveSource : public DataSource {
public:
	LiveSource();
	~LiveSource();
	virtual bool open();
	virtual int setFilter(string filter);
	virtual int read(string &buffer, int maxElements);
	virtual int write(string &buffer);

	virtual int getUpdateType();
	virtual string getName() { return "Live"; }

	virtual void sync();

	void config_geoip();
	vector <string> getBlockedCountries();
	void blockCountry(string iso, int cmd = CMD_BLOCK_COUNTRY);
	void unblockCountry(string iso);

	vector <string> getBlockedApps();
	void blockApp(string path, int cmd = CMD_BLOCK_APP);
	void unBlockApp(string path);
private:
	HANDLE  hDevice;
	bool driver_available;

};