#include "stdafx.h"
#include "dbclient.h"
#include <sstream>
#include <iomanip>
#include <iostream>
#include <thread>
#include "sqlite3.h"

using namespace std;

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	/*int i;
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");*/
	return 0;
}


DBClient::DBClient() {
	type = LiveUpdate;
	exit = false;
	count = 0;
	//return;

	workerThread = std::thread(&DBClient::worker, this);

	int result = sqlite3_open_v2("file:trackmyinternet.db", &db, SQLITE_OPEN_URI | SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, NULL);

	if (db) {
		char *sql = 
			"CREATE TABLE IF NOT EXISTS FLOWS ("
			"`id`	INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,"
			"`start`	INTEGER NOT NULL,"
			"`end`	INTEGER,"
			"`ipProtocol`	SMALLINT,"
			"`remoteIP`	INTEGER,"
			"`remoteIP_str`	VARCHAR(32),"
			"`localIP`	INTEGER,"
			"`localIP_str`	VARCHAR(32),"
			"`remotePort`	SMALLINT,"
			"`localPort`	SMALLINT,"
			"`remoteHost`	VARCHAR(256),"
			"`packetsRx`	INTEGER,"
			"`packetsTx`	INTEGER,"
			"`bytesRx`	INTEGER,"
			"`bytesTx`	INTEGER,"
			"`app`	TEXT,"
			"`country_iso`	VARCHAR(2),"
			"`country`	VARCHAR(64)"
			");";


		char *sql_triggers = "CREATE TRIGGER delete_tail AFTER INSERT ON flows "
			"BEGIN "
			"DELETE FROM flows WHERE id % 128000 = NEW.id % 128000 AND id != NEW.id; "
			"END;";


		char *sql_queries[] = {
			sql,
			sql_triggers,
			"CREATE INDEX `app` ON `FLOWS` (`app` ASC)",
			"CREATE INDEX `country` ON `FLOWS` (`country` )",
			"CREATE INDEX `localIP_str` ON `FLOWS` (`localIP_str` )",
			"CREATE INDEX `remoteIP_str` ON `FLOWS` (`remoteIP_str` )",
			"CREATE INDEX `country_iso` ON `FLOWS` (`country_iso` )",
			"CREATE INDEX `id` ON `FLOWS` (`id` ASC)",
			"CREATE INDEX `start` ON `FLOWS` (`start` DESC)",
			"CREATE INDEX `end` ON `FLOWS` (`end` DESC)",
			NULL
		};

		for (int i = 0; sql_queries[i]; i++) {
			string error = "";
			if (!executeQuery(sql_queries[i], error)) {
				fprintf(stderr, "Error running query: %s\n", error.c_str());
			}
		}
		//sqlite3_close_v2(db);
	}

}


DBClient::~DBClient(){
	exit = true;
	workerThread.join();
	if (db != NULL) 
	{
		sqlite3_close_v2(db);
		db = NULL;
	}
}

/*
"INSERT INTO ONE(ID, NAME, LABEL, GRP, FILE)"
" VALUES(NULL, 'fedfsdfss', NULL, NULL, ?)"
*/
bool DBClient::insert(string query, string blob, string &error) {

	sqlite3_stmt *stmt = NULL;
	int rc = sqlite3_prepare_v2(db,
		query.c_str(),
		-1, &stmt, NULL);
	if (rc != SQLITE_OK) {
		cerr << "prepare failed: " << sqlite3_errmsg(db) << endl;
	}
	else {
		// SQLITE_STATIC because the statement is finalized
		// before the buffer is freed:
		rc = sqlite3_bind_blob(stmt, 1, blob.c_str(), blob.size(), SQLITE_STATIC);
		if (rc != SQLITE_OK) {
			cerr << "bind failed: " << sqlite3_errmsg(db) << endl;
		}
		else {
			rc = sqlite3_step(stmt);
			if (rc != SQLITE_DONE)
				cerr << "execution failed: " << sqlite3_errmsg(db) << endl;
		}
	}
	sqlite3_finalize(stmt);

	return true;
}
bool DBClient::executeQuery(string query, string &error) {
	int rc;
	char *zErrMsg = 0;

	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		error = zErrMsg;
		sqlite3_free(zErrMsg);
		return false;
	} 

	return true;
}

int DBClient::worker() {
	FILETIME _time;
	LARGE_INTEGER time;

	while (!exit) {
		Sleep(20);
		GetSystemTimeAsFileTime(&_time);

		time.LowPart = _time.dwLowDateTime;
		time.HighPart = _time.dwHighDateTime;

		if (workerMutex.try_lock()) {
			while (!workerQueue.empty()) {
				shared_ptr<TMIFlow> flow = workerQueue.front();
				workerQueue.pop();

				// insert or update ?
				if (flow->db_id == -1) {
					insert(flow);
					flow->lastDBUpdate.QuadPart = time.QuadPart;
				} else {
					int sec = NS_TO_MS(time.QuadPart - flow->lastDBUpdate.QuadPart);
					if (sec< FLOW_DB_DUMP_INTERVAL) {
						continue;
					}
					update(flow);
					flow->lastDBUpdate.QuadPart = time.QuadPart;
				}
				
				
			}
			workerMutex.unlock();
		}
	}

	return 1;
}

/* 
INSERT INTO `FLOWS` (id,hash,start,end,remoteIP,localIP,remotePort,localPort,remoteHost,packetsRx,packetsTx,bytesRx,bytesTx,downlinkAvg,uplinkAvg,app,country_iso) 
VALUES (1,'',0,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
*/

string quote(string src) {
	return string("'") + src + string("'");
}

void DBClient::update(shared_ptr<TMIFlow> f) {
	// UPDATE COMPANY SET ADDRESS = 'Texas' WHERE ID = 6;
	query << "UPDATE FLOWS SET "; 
	query << "end = '" << f->end.QuadPart << "',";
	query << "packetsRx = '" << f->GetFlowPacketsRx()<< "',";
	query << "packetsTx = '" << f->GetFlowPacketsTx()<< "',";
	query << "bytesRx = '" << f->GetFlowRx() << "',";
	query << "bytesTx = '" << f->GetFlowTx() << "' ";
	
	query << "WHERE ID = " << f->db_id << ";";

	string error;
	if (!executeQuery(query.str(), error)) {
		fprintf(stderr, "Error running query: %s\n", error.c_str());
	}

	count = 0;
	query.clear();

}

//http://stackoverflow.com/questions/1711631/improve-insert-per-second-performance-of-sqlite
void DBClient::insert(shared_ptr<TMIFlow> f) {
		
	query << "INSERT INTO `FLOWS` (start,end,remoteIP,remoteIP_str,localIP, localIP_str,remotePort,localPort,packetsRx,packetsTx,bytesRx,bytesTx,app,country_iso,country, ipProtocol) ";
	query << "VALUES";
	query << "(";
	query << "'" << f->start.QuadPart << "'" << ", ";
	query << "'" << f->end.QuadPart << "'" << ", ";
	query << "'" << f->_GetRemoteAddr() << "'" << ", ";
	query << "'" << f->GetRemoteAddr() << "'" << ", ";
	query << "'" << f->_GetLocalAddr() << "'" << ", ";
	query << "'" << f->GetLocalAddr() << "'" << ", ";
	query << "'" << f->GetRemotePort() << "'" << ", ";
	query << "'" << f->GetLocalPort() << "'" << ", ";
	query << "'" << f->GetFlowPacketsRx() << "'" << ", ";
	query << "'" << f->GetFlowPacketsTx() << "'" << ", ";
	query << "'" << f->GetFlowRx() << "'" << ", ";
	query << "'" << f->GetFlowTx() << "'" << ", ";
	query << quote(f->process_path) << ", ";
	query << quote(f->country_iso_name) << ", ";
	query << quote(f->country_name) << ", ";
	query << "'" << f->GetIPPROTO() << "')";


	string error;
	if (!executeQuery(query.str(), error)) {
		fprintf(stderr, "Error running query: %s\n", error.c_str());
	}
	else {
		f->db_id = sqlite3_last_insert_rowid(db);			
		//f->db_id = -1;
	}

	count = 0;
	query.str("");


}

void DBClient::OnFlowStore(shared_ptr<TMIFlow> f) {
	workerMutex.lock();
	workerQueue.push(f);
	workerMutex.unlock();
}

void DBClient::OnFlowUpdate(shared_ptr<TMIFlow> f) {
	workerMutex.lock();
	workerQueue.push(f);
	workerMutex.unlock();
}

void DBClient::OnFlowAged(shared_ptr<TMIFlow> f) {
}

void DBClient::OnFlowSpeed(shared_ptr<TMIFlow> f) {
}
