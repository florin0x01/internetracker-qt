#pragma once
#include "stdafx.h"
#include "tmi.h"
#include "sqlite3.h"
#include <thread>
#include <mutex>
#include <queue>
#include <sstream>

class DBClient : public TMIProcessCallback{
public:
	DBClient();
	~DBClient();

	void OnFlowStore(shared_ptr<TMIFlow> f);
	void OnFlowUpdate(shared_ptr<TMIFlow> f);
	void OnFlowAged(shared_ptr<TMIFlow> f);
	void OnFlowSpeed(shared_ptr<TMIFlow> f);

private:
	sqlite3 *db = NULL;
	stringstream query; 
	int max_entries = 0;
	int count = 0;

	void insert(shared_ptr<TMIFlow> f);
	void update(shared_ptr<TMIFlow> f);
	bool insert(string query, string blob, string &error);

	bool executeQuery(string query, string &error);

	int worker();
	std::thread workerThread;
	std::mutex workerMutex;
	std::queue<shared_ptr<TMIFlow>> workerQueue;

	bool exit;
};

