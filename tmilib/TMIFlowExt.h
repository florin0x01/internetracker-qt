#pragma once
#include "tmi.h"

class TMIFlowExt : public TMIFlow {

public:
	TMIFlowExt();
	virtual ~TMIFlowExt();

	virtual void Update(flow *f);
	void UpdateExt(flow_ext_v1 *ext);
	void Update(flow *f, flow_ext_v1 *ext);
};

