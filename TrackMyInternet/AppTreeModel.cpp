#include "stdafx.h"
#include "AppTreeModel.h"
#include <shellapi.h>
#include <QtWinExtras\qwinfunctions.h>
#include "CountryIconProvider.h"
#include <qdnslookup.h>
#include <QHostInfo>
#include "Utils.h"
#include <iostream>
#include "guicommon.h"
#include <stack>

uint AppTreeModel::deleteCount = 0;
TreeItem::TreeItem(AppTreeModel *model) {
	flow = NULL;
	type = ROOT;
	hIcon = NULL;
	this->model = model;
	lookupHostId = -1;
	hiddenCount = 0;
	parent = parentApp = parentConnection = parentCountry = parent;
	memset(&stats, 0, sizeof(stats));
	for (int i = 0; i < 5;i++)
		index[i] = QModelIndex();
}

TreeItem::TreeItem(TreeItem *parent, AppTreeModel *model)
 {
	 flow = NULL;
	 type = NONE;
	 hIcon = NULL;
	 this->model = model;
	 parent = parentApp = parentConnection = parentCountry = parent;
	 memset(&stats, 0, sizeof(stats));
	 //index[0] = QModelIndex(model->createIndex(parent->children.size(), 0, this));
	 lookupHostId = -1;
	 hiddenCount = 0;
	 for (int i = 0; i < 5; i++) {
		 index[i] = QModelIndex(model->createIndex(parent->children.size(), i, this));
	 }		 
}

TreeItem::~TreeItem() {
	for (int i = 0; i < children.size(); i++) {
		/*if (children[i])
		delete children[i];*/
	}
	//DecParentStats(stats);
	children.clear();
	if (hIcon) {
		DestroyIcon(hIcon);
	}

	if (flow) {
		AppTreeModel::deleteCount++;
		flow.reset();
	}
	
	if (lookupHostId != -1) {
		QHostInfo::abortHostLookup(lookupHostId);
	}
	
}


bool TreeItem::operator < (const TreeItem* other) const
{
	string s1 = other->process.substr(0, 3);
	string s2 = process.substr(0, 3);
	std::transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
	std::transform(s2.begin(), s2.end(), s2.begin(), ::tolower);

	bool result = s1 < s2;
	return model->currentSortOrder == Qt::AscendingOrder ? result : !result;
}
AppTreeModel::AppTreeModel(QObject *parent) :QAbstractItemModel(parent)
{
	rootItem = new TreeItem(this);
	rootItem->SetType(TreeItem::ROOT);
	rootItem->parent = NULL;
	
}

void TreeItem::ResolveIP() {
	QHostInfo::lookupHost(flow->GetRemoteAddr().c_str(), this, SLOT(DnsResolved(QHostInfo)));
}

void TreeItem::DnsResolved(QHostInfo hostInfo) {
	lookupHostId = -1;
	if (hostInfo.error() != QHostInfo::NoError) {
		
		return;
	}
	
	hostName = hostInfo.hostName();	
	emit model->dataChanged(index[0], index[4]);
	emit ItemRefreshed(this);
}

bool AppTreeModel::removeRows(int row, int count, const QModelIndex & parent) {

	return true;
}

void AppTreeModel::FreeParents(TreeItem *item) {
	TreeItem *parent;

	// decrement speed from parents as we are removing the node
	item->UpdateSpeedStats(true);

	while (item->parent) {
		beginRemoveRows(item->parent->index[0], item->row(), item->row());
		// this will invalidate all indexes after item, so we must make sure we fix this before exiting the row removal section
		item->parent->RemoveChild(item);		
		// recompute the indexes for all remaining children
		item->parent->RegeneratedChildrenIndexes();
		endRemoveRows();		

		//this->changePersistentIndex(item->parent->index[i], QModelIndex());
		switch(item->type) {
			case TreeItem::CONNECTION:				
			break;
			case TreeItem::COUNTRY: 
				{
					auto iterator = countries.find(item->process);
					if (iterator != countries.end()) {
						iterator->second.erase(item->country_iso);
					}
					else {
						
					}
				}
				
				break;
			case TreeItem::APP:
				apps.erase(item->process);				
				break;
		}	
		
		parent = item->parent;
		
		if (parent->children.size() == 0) {
			// this is needed in the parent country/app
			parent->process = item->process;
			parent->country_iso = item->country_iso;			
		}

		

		emit item->ItemDeleted(item);
		item->deleted = true;		
		delete item;

		item = parent;
		emit dataChanged(item->index[0], item->index[4]);

		if (item->children.size() > 0) {
			break;
		} 

		item->unhide();
	}
	
}

void AppTreeModel::resetFlowUserData(TreeItem *item) {
	if (item->flow) {
		item->flow->user_data = NULL;
	}
	for (int i = 0; i < item->children.size(); i++) {
		resetFlowUserData(item->children[i]);		
	}
}

void AppTreeModel::reset() {	
	QAbstractItemModel::beginResetModel();
	resetFlowUserData(rootItem);

	for (int i = 0; i < rootItem->children.size(); i++) {
		delete rootItem->children[i];
	}	
	rootItem->children.clear();
	countries.clear();
	apps.clear();
	QAbstractItemModel::endResetModel();

	
}

void AppTreeModel::RemoveFlow(shared_ptr<TMIFlow> flow) {
	if (!flow || flow->GetPID() == 0 || flow->GetPID() == 4) {
		return;
	}

	storeCallbacks++;
	TreeItem *item = (TreeItem *)flow->user_data;
		

	if (!item) {
		return;
	}

	FreeParents(item);	
}

bool TreeItem::CompareSpeed(TreeItem* i, TreeItem *j) { return (i->stats.totalSpeedRX + i->stats.totalSpeedTX > j->stats.totalSpeedRX + j->stats.totalSpeedTX); }

void TreeItem::UpdateSpeedStats(bool negative) {
	// propagate to parents
	for (auto p = parent; p; p = p->parent) {
		//substract old speed
		p->stats.totalSpeedRX -= stats.totalSpeedRX;
		p->stats.totalSpeedTX -= stats.totalSpeedTX;

		if (!negative) {
			// add new speed
			p->stats.totalSpeedRX += this->flow->getSpeedStats().downlinkBps;
			p->stats.totalSpeedTX += this->flow->getSpeedStats().uplinkBps;
		}
	}

	// update current node
	stats.totalSpeedRX = this->flow->getSpeedStats().downlinkBps;
	stats.totalSpeedTX = this->flow->getSpeedStats().uplinkBps;

}
void AppTreeModel::UpdateFlowSpeed(shared_ptr<TMIFlow> flow) {
	if (!flow) {
		return;
	}

	auto iterator = apps.find(flow->GetProcessName());
	if (iterator != apps.end()) {
		TreeItem *appItem = iterator->second;

		TreeItem *country = countries[flow->GetProcessName()][flow->GetCountryISO()];
		if (country) {
			// check for item
			TreeItem *flowItem = (TreeItem *)flow->user_data;
			// item is updated, trigger the update
			if (flowItem) {
				flowItem->UpdateSpeedStats();
				emit dataChanged(flowItem->index[0], flowItem->index[4]);
			}
		}
	}

};

void AppTreeModel::ProcessFlow(shared_ptr<TMIFlow> flow) {
	// check app/country if it is there
	auto iterator = apps.find(flow->GetProcessName());	
	if (iterator != apps.end()) {
		TreeItem *appItem = iterator->second;

		TreeItem *country = countries[flow->GetProcessName()][flow->GetCountryISO()];
		if (country) {
			// check for item
			TreeItem *flowItem = (TreeItem *)flow->user_data;
			if (flowItem) {
				flowItem->Update();
				emit dataChanged(flowItem->index[0], flowItem->index[4]);
				emit dataChanged(country->index[0], country->index[4]);
				emit dataChanged(appItem->index[0], appItem->index[4]);
			}
			else {				
				beginInsertRows(country->index[0], country->children.size(), country->children.size());
				TreeItem *newFlow = new TreeItem(country, this);

				country->children.push_back(newFlow);
				newFlow->SetFlow(flow, country);	
				newFlow->Update();
				flow->user_data = newFlow;			
				endInsertRows();
				emit dataChanged(country->index[0], country->index[4]);
				emit dataChanged(appItem->index[0], appItem->index[4]);				
			}
		} else {
			beginInsertRows(appItem->index[0], appItem->children.size(), appItem->children.size());
			TreeItem *country = new TreeItem(appItem, this);
			country->SetCountry(flow->GetCountry(), flow->GetCountryISO(), appItem);
			appItem->children.push_back(country);
			countries[flow->GetProcessName()][flow->GetCountryISO()] = country;
			endInsertRows();

			beginInsertRows(country->index[0], country->children.size(), country->children.size());
			TreeItem *newFlow = new TreeItem(country, this);
			// first insert so setflow can update al parents correctly
			country->children.push_back(newFlow);
			newFlow->SetFlow(flow, country);	
			newFlow->Update();
			flow->user_data = newFlow;					
			endInsertRows();
			
			emit dataChanged(appItem->index[0], appItem->index[4]);			

		}
	} else {
		beginInsertRows(rootItem->index[0], rootItem->children.size(), rootItem->children.size());
		TreeItem *appItem = new TreeItem(rootItem, this);
		appItem->SetApp(flow->GetProcessPath(),flow->GetProcessFullPath(), flow->GetPID(), rootItem);
		rootItem->children.push_back(appItem);
		apps[flow->GetProcessName()] = appItem;
		
		endInsertRows();

		beginInsertRows(appItem->index[0], appItem->children.size(), appItem->children.size());
		TreeItem *newCountry = new TreeItem(appItem, this);
		newCountry->SetCountry(flow->GetCountry(), flow->GetCountryISO(), appItem);
		appItem->children.push_back(newCountry);
		countries[flow->GetProcessName()][flow->GetCountryISO()] = newCountry;
		endInsertRows();
		
		beginInsertRows(newCountry->index[0], newCountry->children.size(), newCountry->children.size());
		
		TreeItem *newFlow = new TreeItem(newCountry, this);
		flow->user_data = newFlow;
		// first insert so setflow can update al parents correctly
		newCountry->children.push_back(newFlow);
		newFlow->SetFlow(flow, newCountry);		
		newFlow->Update();
		flow->user_data = newFlow;				
		endInsertRows();
		emit dataChanged(rootItem->index[0], rootItem->index[4]);

	}
}


AppTreeModel::~AppTreeModel()
{
	delete rootItem;
}

QModelIndex AppTreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
	TreeItem *parentItem = childItem->parent;
	
	//if (!parentItem || parentItem == rootItem)
	if (parentItem == rootItem)
		return rootItem->index[0];

	//parentItem->index = QModelIndex(createIndex(parentItem->row(), 0, parentItem));
	return parentItem->index[0];
}

TreeItem *AppTreeModel::getItem(const QModelIndex &index) const
{
	if (index.isValid()) {
		TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
		if (item)
			return item;
	}
	return rootItem;
}

QVariant AppTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return rootItem->data(section, role);

	return QVariant();
}

int AppTreeModel::columnCount(const QModelIndex & /* parent */) const
{
	return rootItem->columnCount();
}

QVariant AppTreeModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	TreeItem *item = getItem(index);

	return item->data(index.column(), role);
}

Qt::ItemFlags AppTreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	return QAbstractItemModel::flags(index);
}

int AppTreeModel::rowCount(const QModelIndex &parent) const
{
	TreeItem *parentItem = getItem(parent);

	return parentItem->children.size();
}

QModelIndex AppTreeModel::index(int row, int column, const QModelIndex &parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	TreeItem *parentItem;

	if (!parent.isValid())
		parentItem = rootItem;
	else
		parentItem = static_cast<TreeItem*>(parent.internalPointer());

	TreeItem *childItem = parentItem->children[row];

	return childItem->index[column];
}

void AppTreeModel::_sort(TreeItem *parent, int column, Qt::SortOrder order) {
	bool (*sortFunc)(TreeItem *i, TreeItem *j); 

	// select the proper sort function
	if (column == 0) {
		switch (parent->GetType()) {
		case TreeItem::ROOT:
			sortFunc = processSort;
			break;
		case TreeItem::APP:
			sortFunc = countrySort;
			break;
		case TreeItem::COUNTRY:
			sortFunc = connectionSort;
			break;
		};
	} else if (column == 1) {
		sortFunc = activeConnectionSort;
	} else if (column == 2) {
		sortFunc = totalTrafficSort;
	} else if (column == 3) {
		sortFunc = totalSpeedSort;
	} else if (column == 4) {
		sortFunc = durationSort;
	}
	
	if (parent->children.size() > 0) {
		// sort only connections for duration column
		if (column < 5) {
			QList<QPersistentModelIndex> list = { parent->index[0], parent->index[1], parent->index[2], parent->index[3], parent->index[4] };
			layoutAboutToBeChanged(list, VerticalSortHint);
			std::sort(parent->children.begin(), parent->children.end(), sortFunc);
			parent->RegeneratedChildrenIndexes(false);
			layoutChanged(list, VerticalSortHint);
		}
				
		// sort children
		for (int i = 0; i < parent->children.size(); i++) {
			_sort(parent->children[i], column, order);
		}
	}

	
}

void AppTreeModel::sort(int column, Qt::SortOrder order) {
	currentSortColumn = column;
	currentSortOrder = order;

	_sort(rootItem, column, order);
}

void TreeItem::SetFlow(shared_ptr<TMIFlow> flow, TreeItem *parent) {
	this->parent = parent;
	this->flow = flow;
	this->process = flow->GetProcessName();
	this->country = flow->GetCountry();
	this->country_iso = flow->GetCountryISO();
	stats.totalConnections = 1;
	stats.activeConnections = 1;
	if (flow->GetIPPROTO() == IPPROTO_UDP)
		this->icon = QIcon(":/icons/icons/UDP.png");
	else 
		this->icon = QIcon(":/icons/icons/TCP.png");
	type = CONNECTION;

}

void TreeItem::SetCountry(string country, string country_iso, TreeItem *parent) {
	this->type = COUNTRY;	
	this->parent = parent;
	this->country = country;	
	this->country_iso = country_iso;
	
	this->icon = *IsoCountryProvider->getCountryByIso(country_iso.c_str());

	Update();
	//IncAllParentStats(stats);
}


void TreeItem::SetApp(string process_path, string process_full_path, UINT32 pid, TreeItem *parent) {	
	char _process_name[MAX_PATH];
	this->type = APP;
	this->parent = parent;	
	this->process_path = process_path;	
	this->process_full_path = process_full_path;
	strncpy(_process_name, process_path.c_str(), MAX_PATH);

	if (_process_name && strlen(_process_name)) {
		CHAR *baseName = strrchr(_process_name, '\\');
		if (baseName) {
			this->process = string(baseName + 1);
		}
		else {
			this->process = string(_process_name);
		}
	}

	Update();			
	hIcon = GetIconForFile(process_path, SHGFI_LARGEICON);
	QPixmap pixmap = QtWin::fromHICON(hIcon);
	this->icon = QIcon(pixmap);

}

int TreeItem::columnCount() const
{
	return 5;
}

// recompute the indexes for all remaining children

void TreeItem::RegeneratedChildrenIndexes(bool remove) {
	for (int i = 0; i < children.size(); i++) {
		for (int j = 0; j < 5; j++) {
			QModelIndex newIndex = QModelIndex(model->createIndex(i, j, children[i]));

			if (!remove) {
				model->changePersistentIndex(children[i]->index[j], newIndex);
			}
			children[i]->index[j] = newIndex;
		}
	}
}

void TreeItem::RemoveChild(TreeItem *item) {
	std::vector<TreeItem*>::iterator iter = find(children.begin(), children.end(), item);
	if (iter != children.end()) {
		children.erase(iter);
	}
	else {
		return;
	}
}

QVariant TreeItem::GetTextColor4() {
	if (B_TO_KB(stats.totalSpeedRX) == 0 && B_TO_KB(stats.totalSpeedTX) == 0) {
		return QColor(120, 120, 200);
	} else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 2048) {
		return QColor(200, 120, 120);
	} else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 1024) {
		return QColor(200, 200, 120);
	} else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 0) {
		return QColor(120, 200, 120);
	}
	return QVariant();
}

QVariant TreeItem::GetSortRole(int column) {
	string sortString = "";
	switch (column) {
		case 0:
			switch (type) {
				case CONNECTION:
					sortString = (GetHostname() != "") ? GetHostname().toStdString() : this->flow->GetRemoteAddr();
					// sort by 3 letters only to increase speed
					return QString(sortString.substr(0, 3).c_str());
				case COUNTRY:
					return QString(this->country_iso.c_str());
				case APP:
					return process.c_str();
			};
		case 1:
			return (UINT32)stats.activeConnections;
		case 2:
			return (UINT64)(stats.totalBytesRX + stats.totalBytesTX);
		case 3:
			return (UINT64)(stats.totalSpeedRX + stats.totalSpeedTX);
	}

	return QVariant();
}

QVariant TreeItem::data(int column, int role)
{
	if (role == Qt::BackgroundRole) {
		QColor color = GetBackgroundColor();

		QLinearGradient lgrad(QPointF(0, 0), QPointF(1, 0));		
		if (column == 0) {
			// set base color for first one
			lgrad.setColorAt(0.0, QColor(15, 15, 15));
		} else {
			lgrad.setColorAt(0.0, color);
		}			
		lgrad.setCoordinateMode(QGradient::ObjectBoundingMode);		
		lgrad.setColorAt(1.0, color);
		return QBrush(lgrad);
	}

	// sort role
	if (role == Qt::UserRole) {
		return GetSortRole(column);
	}
	switch (column) {
	case 0:
			if (role == Qt::DisplayRole)
				return GetText();
			if (role == Qt::DecorationRole)
				return GetIcon();
			break;
	case 1:
			if (role == Qt::DisplayRole)
				return GetText2();
			break;
	case 2:
		if (role == Qt::DisplayRole)
			return GetText3();
		break;
	case 3:
		if (role == Qt::DisplayRole)
			return GetText4();
		else if (role == Qt::TextColorRole) {
			return GetTextColor4();
		}
		break;
	case 4:
		if (role == Qt::DisplayRole)
			return GetText5();
		break;
	}

	
	return QVariant();
}

void TreeItem::SetParentType(TreeItemType type) {
	switch (type) {
		case CONNECTION:
			this->parentConnection = parent;
			break;
		case COUNTRY:
			this->parentCountry = parent;
			break;
		case APP:
			this->parentApp = parent;
			break;			
		case DETAIL:			
			// unused
			break;
	}
}


void TreeItem::IncStats() {	
	DeltaStats deltaStats = flow->getDeltaStats();	
	for (auto p = parent; p; p = p->parent) {
		// update total connections only once
		if (stats.totalPacketsRX == 0 && stats.totalPacketsTX == 0) {
			p->stats.totalConnections++;
		}

		p->stats.totalBytesRX += deltaStats.bytesRx;
		p->stats.totalBytesTX += deltaStats.bytesTx;
		p->stats.totalPacketsRX += deltaStats.packetsRx;
		p->stats.totalPacketsTX += deltaStats.packetsTx;

		switch (p->type) {
			case CONNECTION:
				// never happens
				break;
			case COUNTRY:

				break;
			case APP:

				break;
			case ROOT:

				break;
		}
	}
}


void TreeItem::Update() {
	switch (type) {
		case COLLAPSED:
			SetFlow(flow, parent);
			type = CONNECTION;
			break;
		case CONNECTION:
			if (flow->getDbID() != -1 && flow->getDbGroupByCount() > 0) {
				type = COLLAPSED;
			}
			IncStats();
			stats.totalBytesRX = flow->GetFlowRx();
			stats.totalBytesTX = flow->GetFlowTx();
			stats.totalPacketsRX = flow->GetFlowPacketsRx();
			stats.totalPacketsTX = flow->GetFlowPacketsTx();				
			break;
		case COUNTRY:
			// Lazy db load support: set number of items equal to groupByCount
			if (children.size() == 1 && children[0]->GetType() == COLLAPSED) {
				stats.totalConnections = stats.activeConnections = children[0]->GetFlow()->getDbGroupByCount();
			}
			else {
				stats.activeConnections = children.size();
			}			
			break;
		case APP:						
			stats.totalCountries = children.size();			
			break;
		case ROOT:			
			stats.totalApps = children.size();			
		case DETAIL:
			// unused
			break;
	}	

	emit ItemRefreshed(this);

	if (parent) {
		parent->Update();
	}
}

QColor TreeItem::GetBackgroundColor() {
	if (B_TO_KB(stats.totalSpeedRX) == 0 && B_TO_KB(stats.totalSpeedTX) == 0) {
		return QColor(50, 50, 90).darker().darker();
	}
	else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 2048) {
		return QColor(200, 120, 120).darker().darker();
	}
	else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 1024) {
		return QColor(200, 200, 120).darker().darker();
	}
	else if (B_TO_KB(stats.totalSpeedRX + stats.totalSpeedTX) > 0) {
		return QColor(120, 200, 120).darker().darker();
	}

	return QColor();
}
QString TreeItem::GetText()  {
	switch (type) {
		case COLLAPSED:
			return QString("Loading, please wait ...");
			break;
		case CONNECTION:
			if (flow) {
				if (hostName.length() != 0) {
					return QString("%1:%2").arg(hostName).arg(QString::number(flow->GetRemotePort()));
				}
				else {
					ResolveIP();
					return QString("%1:%2").arg(flow->GetRemoteAddr().c_str()).arg(QString::number(flow->GetRemotePort()));
				}
				
			}
			else {
				return QString("early");
			}			
			break;
		case COUNTRY:
			return QString(country.c_str());			
			break;
		case APP:
			return QString("%1 (%2)").arg(process.c_str()).arg(children.size());
			break;
		case ROOT:
			return QString("Apps (%1)").arg(children.size());
			break;
		case DETAIL:
			// unused
			break;
	}

	return QString("");
}

QVariant TreeItem::GetText2() {
	switch (type) {
	case CONNECTION:
		//return QString("%1").arg(flow->transport_protocol.c_str());
		break;
	case COUNTRY:
		return QString("%1 (%2 total)").arg(QString::number(stats.activeConnections)).arg(QString::number(stats.totalConnections));
		break;
	case APP:
		stats.activeConnections = 0;
		for (auto i = 0; i < children.size(); i++) {
			stats.activeConnections += children[i]->stats.activeConnections;
		}
		return QString("%1").arg(QString::number(stats.activeConnections));
		break;
	case ROOT:
		return QString("Connection(s)");
		break;

	case DETAIL:
		// unused
		break;
	}

	return QVariant();
}

QString TreeItem::GetText3() {
	switch (type) {
	case CONNECTION:		
	case COUNTRY:				
	case APP:
		return QString("%1 / %2").arg(QString::fromStdString(prettyBytes(stats.totalBytesRX))).arg(QString::fromStdString(prettyBytes(stats.totalBytesTX)));
		break;
	case ROOT:
		return QString("Recv/Sent bytes");
		break;
	case DETAIL:
		// unused
		break;
	}

	return QString("");
}


QString TreeItem::GetText4() {
	switch (type) {
		case COUNTRY:
		case APP:
		case CONNECTION:
			if (stats.totalSpeedRX == 0 && stats.totalSpeedTX == 0) {
				return QString("Idle");
			}
			else {
				return QString("%1ps / %2ps").arg(QString::fromStdString(prettyBytes(stats.totalSpeedRX)), QString::fromStdString(prettyBytes(stats.totalSpeedTX)));
			}
			
			break;
		case ROOT:
			return QString("Download/Upload");
			break;

		default:
			// unused
		break;
	}

	return QString("");
}


QString TreeItem::GetText5() {
	
	
	switch (type) {
	case CONNECTION: {
		if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
			return QString("%1").arg(prettySeconds(MS_TO_S(GetFlow()->GetDuration())));
		}
		else {
			QDateTime date = QDateTime::fromTime_t(nanoseconds1600_to_time_t(GetFlow()->GetStartTime()));
			return QString("%1").arg(date.toString());
		}
		
		break;
	}
	case COUNTRY:
	case APP:
//		return QString("%1 / %2").arg(QString::number(stats.totalPacketsRX), QString::number(stats.totalPacketsTX));
		break;
	case ROOT:
		if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
			return QString("Duration");
		} else {
			return QString("Time");
		}
		
		break;
	case DETAIL:
		// unused
		break;
	}

	return QString("");
}

QFileIconProvider provider;

QVariant TreeItem::GetIcon() const {
	switch (type) {
	case COLLAPSED:
		break;
	case CONNECTION:
		return icon;
		break;
	case COUNTRY:
		return icon;		
		break;
	case APP:		
		return icon;				
		break;
	case DETAIL:
		// unused
		break;
	}

	return QVariant();
}