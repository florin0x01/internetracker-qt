#pragma once
#include <qabstractitemmodel.h>
#include "tmi.h"
#include <QtWidgets>
#include "AppTreeModel.h"
#include <qhostinfo.h>

typedef struct _TreeItemStats {
	uint activeConnections;
	// these refer to all elements under this root
	// these will be updated when elements areadded/removed from parents
	uint totalConnections;
	uint totalCountries;
	uint totalApps;
	uint totalBytesRX;
	uint totalBytesTX;
	uint totalSpeedRX;
	uint totalSpeedTX;
	uint totalPacketsRX;
	uint totalPacketsTX;

}TreeItemStats;

class AppTreeModel;
class TreeItem: public QObject {
	Q_OBJECT
public:
	// COLLAPSED is used for lasy db loading 
	enum TreeItemType {NONE, ROOT, APP, COUNTRY, CONNECTION, DETAIL, COLLAPSED};

public:
	TreeItem(TreeItem *parent, AppTreeModel *model);
	TreeItem(AppTreeModel *model);

	~TreeItem();

	TreeItemType GetType() const { return type; }
	void SetType(TreeItemType type) { this->type = type; }	
	shared_ptr<TMIFlow> GetFlow() const { return flow; }
	
	void SetFlow(shared_ptr<TMIFlow> flow, TreeItem *parent);
	void SetCountry(string country, string country_iso, TreeItem *parent);
	void SetApp(string process, string process_full_path, UINT32 pid, TreeItem *parent);
	void SetParentType(TreeItemType type);
	QString GetHostname() const { return hostName; }
	QIcon icon;
	HICON hIcon;
	
	QVariant GetSortRole(int column);
	QString GetText();	
	QVariant GetText2();
	QString GetText3();
	QString GetText4();
	QVariant GetTextColor4();
	QColor GetBackgroundColor();
	QString GetText5();
	QVariant GetIcon() const;

	int columnCount() const;
	QVariant data(int column, int role);
	
	int getChildrenCount() const { return children.size(); }
	TreeItem *getChild(int row) const { return children[row]; }

	int row() const {
		if (parent) {
			std::vector<TreeItem*>::iterator iter = find(parent->children.begin(), parent->children.end(), this);			
			if (iter != parent->children.end()) {
				return distance(parent->children.begin(), iter);				
			}
		}
		return 0;
	}
	// updates stats depending on node type
	void Update();	
	bool deleted = false;
	QModelIndex index[5];
	bool operator < (const TreeItem* other) const;

	string getProcessPath() const { return process_path; }
	string getProcessFullPath() const { return process_full_path; }

	string getProcessName() const { return process; }
	string getProcessCountry() const { return country; }
	string getProcessCountryIso() const { return country_iso; }

	TreeItem *getParent() const { return parent; }

	void hide() { hiddenCount++; }
	void unhide() { hiddenCount--; }
	void hideReset() { hiddenCount = 0; }
	int getHiddenCount() { return hiddenCount; }
	
Q_SIGNALS:
	void ItemRefreshed(TreeItem *item);
	void ItemDeleted(TreeItem *item);

private:	
	friend class AppTreeModel;
	TreeItemType type;
	// CONNECTION associated flow item
	shared_ptr<TMIFlow> flow;
	TreeItem *parent;
	string process;
	string process_path;
	string process_full_path;
	string country, country_iso;
	TreeItem *parentApp;
	TreeItem *parentConnection;
	TreeItem *parentCountry;
	AppTreeModel *model;
	int hiddenCount;
	QString hostName;
	vector <TreeItem *> children;

	TreeItemStats stats;
	
	// update parent with current node stats
	void IncStats();
	void UpdateSpeedStats(bool negative = false);
	static bool CompareSpeed(TreeItem* i, TreeItem *j);

	void RemoveChild(TreeItem *item);
	void RegeneratedChildrenIndexes(bool remove = true);

	void StatsAdd(TreeItemStats &src, TreeItemStats &dst) {
		dst.activeConnections += src.activeConnections;
		dst.totalConnections += src.totalConnections;
		//dst.totalCountries += src.totalCountries;
		//dst.totalApps += src.totalApps;
		dst.totalBytesRX += src.totalBytesRX;
		dst.totalBytesTX += src.totalBytesTX;
		dst.totalPacketsRX += src.totalPacketsRX;
		dst.totalPacketsTX += src.totalPacketsTX;

	}

	void ResolveIP();	
	int lookupHostId;


	private slots:
		void DnsResolved(QHostInfo);

};


class AppTreeModel :
	public QAbstractItemModel
{
	Q_OBJECT

public:
	friend class TreeItem;
	AppTreeModel(QObject *parent = 0);
	~AppTreeModel();

	QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
	QVariant headerData(int section, Qt::Orientation orientation,int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	QModelIndex index(int row, int column,const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	TreeItem *getItem(const QModelIndex &index) const;

	void UpdateFlowSpeed(shared_ptr<TMIFlow> flow);
	void ProcessFlow(shared_ptr<TMIFlow> flow);
	void RemoveFlow(shared_ptr<TMIFlow> flow);

	// DEBUG
	uint storeCallbacks = 0;
	static uint deleteCount;
	//bool hasChildren(const QModelIndex &parent = QModelIndex()) const;
	
	virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

	void reset();
	void resetFlowUserData(TreeItem *item);
	
	string getFilter() const { return filter; }
	void setFilter(const string &newFilter) { filter = newFilter; }

private:	
	//QModelIndex index(int row, int column, const TreeItem *parent);
	unordered_map<string, TreeItem *> apps;
	unordered_map<string, unordered_map<string, TreeItem *>> countries;

	void FreeParents(TreeItem *item);
	TreeItem *rootItem;

	int currentSortColumn;
	Qt::SortOrder currentSortOrder;

	//search filter
	string filter;

protected:
	bool AppTreeModel::removeRows(int row, int count, const QModelIndex & parent);
	virtual void _sort(TreeItem *parent, int column, Qt::SortOrder order = Qt::AscendingOrder);

	// sorting routines for various columns and node 
	// column 0

	static bool AppTreeModel::processSort(TreeItem *i, TreeItem *j) {
		if (i == j) {
			return false;
		}

		string s1 = i->process.substr(0, 3);
		string s2 = j->process.substr(0, 3);
		std::transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
		std::transform(s2.begin(), s2.end(), s2.begin(), ::tolower);
		
		return i->model->currentSortOrder == Qt::AscendingOrder ? s1.compare(s2) < 0 : s1.compare(s2) > 0;
	}
	static bool AppTreeModel::countrySort(TreeItem *i, TreeItem *j) {
		if (i == j) {
			return false;
		}

		return i->model->currentSortOrder == Qt::AscendingOrder ? i->country_iso.compare(j->country_iso) < 0 : i->country_iso.compare(j->country_iso) > 0;
	}
	static bool AppTreeModel::connectionSort(TreeItem *i, TreeItem *j) {

		string c1 = (i->GetHostname() == "") ? i->flow->GetRemoteAddr() : i->GetHostname().toStdString();
		string c2 = (j->GetHostname() == "") ? j->flow->GetRemoteAddr() : j->GetHostname().toStdString();

		std::transform(c1.begin(), c1.end(), c1.begin(), ::tolower);
		std::transform(c2.begin(), c2.end(), c2.begin(), ::tolower);

		bool result;
		if (i->model->currentSortOrder == Qt::AscendingOrder)
			result = c1.compare(c2) < 0;
		else
			result = c2.compare(c1) < 0;
		return result;
	}

	// column 1
	static bool AppTreeModel::activeConnectionSort(TreeItem *i, TreeItem *j) { 
		return i->model->currentSortOrder == Qt::AscendingOrder ? i->stats.activeConnections < j->stats.activeConnections : i->stats.activeConnections > j->stats.activeConnections;;
	}

	// column 2
	static 	bool AppTreeModel::totalTrafficSort(TreeItem *i, TreeItem *j) { 
		return i->model->currentSortOrder == Qt::AscendingOrder ? 
			i->stats.totalBytesRX + i->stats.totalBytesTX < j->stats.totalBytesRX + j->stats.totalBytesTX :
			i->stats.totalBytesRX + i->stats.totalBytesTX > j->stats.totalBytesRX + j->stats.totalBytesTX;
	}

	// column 3
	static bool AppTreeModel::totalSpeedSort(TreeItem *i, TreeItem *j) { 
		return i->model->currentSortOrder == Qt::AscendingOrder ? 
			i->stats.totalSpeedRX + i->stats.totalSpeedTX < j->stats.totalSpeedRX + j->stats.totalSpeedTX : 
			i->stats.totalSpeedRX + i->stats.totalSpeedTX > j->stats.totalSpeedRX + j->stats.totalSpeedTX;
	}


	// column 5
	static bool AppTreeModel::durationSort(TreeItem *i, TreeItem *j) {
		if (i->GetType() != TreeItem::CONNECTION) {
			return 0;
		}
		return i->model->currentSortOrder == Qt::AscendingOrder ? i->GetFlow()->GetDuration()< j->GetFlow()->GetDuration() : i->GetFlow()->GetDuration() > j->GetFlow()->GetDuration();
	}

};

