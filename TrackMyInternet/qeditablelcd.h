#ifndef QEDITABLELCD_H
#define QEDITABLELCD_H

#include "stdafx.h"
#include <QLCDNumber>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QRectF>
#include <QWheelEvent>

class qeditablelcd : public QLCDNumber
{
	Q_OBJECT
public:
	explicit qeditablelcd(QWidget *parent = 0);

signals:

	public slots :

protected:
	void mousePressEvent(QMouseEvent * e);
	void keyPressEvent(QKeyEvent * event);
	void paintEvent(QPaintEvent * e);
	void wheelEvent(QWheelEvent * event);

private:
	void setDigitAreaPressed(QMouseEvent * e, const int areaId = -1);
	void positionValueIncrement(const int numSteps);
	QRectF mDigitRect;
	int mDigitIdToChange;
};

#endif // QEDITABLELCD_H
