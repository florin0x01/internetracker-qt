#include "stdafx.h"
#include "AppSettings.h"

void Settings::save() {
	TmiSettings::MapSpeedKBps = MapSpeedKBps;
	TmiSettings::MapTransferKBs = MapTransferKBs;
	TmiSettings::MapUpdateMs = MapUpdateMsec;
	TmiSettings::TreeUpdateMsec = TreeUpdateMsec;

	setValue("MapSpeedKBps", MapSpeedKBps);
	setValue("MapTransferKBs", MapTransferKBs);
	setValue("MapUpdateMsec", MapUpdateMsec);
	setValue("TreeUpdateMsec", TreeUpdateMsec);
	sync();

	beginWriteArray("blockedCountries");
	for (int i = 0; i < blockedCountries.size(); ++i) {
		setArrayIndex(i);
		setValue("country_iso",QString::fromStdString(blockedCountries[i]));
	}
	endArray();

	beginWriteArray("blockedApps");
	for (int i = 0; i < blockedApps.size(); ++i) {
		setArrayIndex(i);
		setValue("app", QString::fromStdString(blockedApps[i]));
	}
	endArray();

}

void Settings::load() {
	MapSpeedKBps = value("MapSpeedKBps", TmiSettings::MapSpeedKBps).toDouble();
	MapTransferKBs = value("MapTransferKBs", TmiSettings::MapTransferKBs).toDouble();
	MapUpdateMsec = value("MapUpdateMsec", TmiSettings::MapUpdateMs).toDouble();
	TreeUpdateMsec = value("TreeUpdateMsec", TmiSettings::TreeUpdateMsec).toDouble();

	int size = beginReadArray("blockedCountries");
	for (int i = 0; i < size; ++i) {
		setArrayIndex(i);
		blockedCountries.push_back(value("country_iso").toString().toStdString());		
	}
	endArray();
	
	size = beginReadArray("blockedApps");
	for (int i = 0; i < size; ++i) {
		setArrayIndex(i);
		blockedCountries.push_back(value("app").toString().toStdString());
	}
	endArray();


}