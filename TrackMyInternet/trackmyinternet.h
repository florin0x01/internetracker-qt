#ifndef TRACKMYINTERNET_H
#define TRACKMYINTERNET_H

#include <QtWidgets/QMainWindow>
#include <QTimer>
#include <qprogressbar.h>
#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <mutex>
#include "ui_trackmyinternet.h"
#include "tmi.h"
#include <qt_windows.h>
#include <QSystemTrayIcon>
#include "sqlite3.h"
#include "AppsView.h"
#include "dbclient.h"

class AppsView;
class TrackMyInternet : public QMainWindow
{
	Q_OBJECT	
public:
	TrackMyInternet(QWidget *parent = 0);
	~TrackMyInternet();

	QSystemTrayIcon *trayIcon;
	

	private slots:
		void UpdateControl();
		void UpdateDbPlayer();
		void onMapTransferUnitChanged(const QString&);

		void buttonPlayClicked(bool checked = false);
		void buttonForwardClicked(bool checked = false);
		void buttonBackClicked(bool checked = false);
		void onSliderValueChanged(int);

private:
	Ui::TrackMyInternetClass ui;
	AppsView *appsView;
	
	TMIControl& control;
	QTimer control_timer;
	QTimer db_player_timer;

	QAction *minimizeAction;
	QAction *maximizeAction;
	QAction *restoreAction;
	QAction *quitAction;

	void createActions();
	void createTrayIcon();
	void showMap();
	
	QMenu *trayIconMenu;
	QLabel *statusLabel;
	
	// we make this available to other apps
	friend class AppsView;
	QProgressBar *progressBar;

	DBClient dbClient;

	TMIProcessCallback::UpdateType currentMode;

	// DB player aka VLC player control
	shared_ptr<TMIFlow> oldestFlow;
	LARGE_INTEGER currentTime;
	void goOnline();
	void goOffline(UINT64 time);

};

#endif // TRACKMYINTERNET_H
