#pragma once

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QTime>
#include <QPainter>
#include <qevent.h>
#include <QDebug>
#include <QGLWidget>

#include "tmi.h"

class MapWidget;

class MapLabel : public QLabel
{
	
	Q_OBJECT
public:
	enum WorkingMode {
		SaveMode = 0,
		HoverMode = 1
	};

	MapLabel(QWidget* parent);
	void setWidget(MapWidget* w);
	void setMode(WorkingMode mode);
	
	virtual ~MapLabel();

protected:
	MapWidget *w;
	WorkingMode mode;
	
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent * event) Q_DECL_OVERRIDE;
};