#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QTime>
#include <QPainter>
#include <QGLWidget>
#include <QGroupBox>
#include <QDebug>
#include <QStandardItemModel>
#include <set>
#include <unordered_map>
#include "MapLabel.h"
#include "tmi.h"
#include "maphelper.h"
#include <iostream>
#include <QTimer>
#include "ui_MapForm.h"
#include <string>


typedef struct _ProcessInfo {
	QListWidgetItem *item;
	UINT32 refCount;
	std::string processName;
}ProcessInfo;

class MapWidget : public QWidget, TMIProcessCallback
{
	Q_OBJECT

public:
	// process path to Process Info map
	friend class MapLabel;
	typedef unordered_map<string, ProcessInfo> procSet;

	void OnFlowStore(shared_ptr<TMIFlow> f);

	// flow update (called when a flow is updated) - flow contains up to date data
	void OnFlowUpdate(shared_ptr<TMIFlow> f);
	
	// flow age
	void OnFlowAged(shared_ptr<TMIFlow> f);

	// called to update the flow's uplink downlink speed
	void OnFlowSpeed(shared_ptr<TMIFlow> f);

	void OnDataSourceChanged(UpdateType type);

	explicit MapWidget(QWidget *parent = 0, bool enableClick = false);
	QSize sizeHint() const;
	void lightCountry(const QString& country, QColor c = QColor(255, 32, 0));
	void lightsOffCountry(const QString& country);
	const Ui::MapForm& getUi()
	{
		return ui;
	}
	  
	virtual ~MapWidget();


signals:

	public slots :
		void onClick();
		void UpdateControl();
		void CheckRadio(bool);
		void CheckRadioTraffic(bool);
		void onItemClicked(QListWidgetItem*);
		void onSelectAllClicked(bool);
		void onSettingsChanged();
		void onSliderSpeedChanged(int);
		void onSliderTransferChanged(int);
		

private:

	friend class MapLabel;
	QTimer control_timer;
	procSet processesSet;

	//resets maps used for UI logic
	void resetUIMaps();

	void loadBins();
	void _lightCountry(QPaintDevice& img, const QString& country, QColor c = QColor(255, 32, 0));
	void _lightsOffCountry(QPaintDevice& img, const QString& country);
	void initIsoToCountry();

	int floodFill(const QPoint& point, const QColor& color);
	bool eventFilter(QObject *o, QEvent *e);
	void floodFill(QImage& image, int x, int y, const QColor &curColor);
	void rfloodFill(QImage& image, int x, int y, const QColor &curColor, QImage& dstImg);
	bool Similar(const QColor& col1, const QColor& col2);
	void invert();
	void processBoundingBox(const QString&);
	void addProcessItem(const string& name, const string& path, ProcessInfo &info);
	void removeProcessItem(ProcessInfo &info);
	
	

	double xRap, yRap;
	double origX, origY;
	Ui::MapForm ui;

	map<string, string> isoToCountry;
	map<string, map<string, UINT32>> appCountryCount;
	map<string, UINT32> connCountryCount;

	QStandardItemModel *model;
	
	QMap<QString, QPixmap*> pixmaps;
	QMap<QString, QRect> countryRects;
	QMap<QString, QListWidgetItem *> listDetailsMap;

	QLabel legendLabel;

	bool selectionTraffic = false;

	TMIControl& control;

	QPixmap* img;
	QPixmap* orig;
	QImage* legend;
	QMainWindow* parent;

	map<string, bool> includedProcs;
	QMap<QRgb, QString> legendMap;
	QPixmap* maskPixmap;

	QTimer timer;
	QPainter painter, painter2;
	const int update_time;
	int total_elapsed;
	int draw_time;
	QTime time;
};



#endif // MAPWIDGET_H
