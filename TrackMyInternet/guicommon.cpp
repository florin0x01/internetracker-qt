#include "stdafx.h"
#include "guicommon.h"

std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

HICON GetIconForFile(std::string filename, UINT size)
{
	SHFILEINFO shinfo = { 0 };

	std::wstring stemp = s2ws(filename);
	LPCWSTR result = stemp.c_str();
	DWORD_PTR p = SHGetFileInfo(result, 0, &shinfo, sizeof(SHFILEINFO), SHGFI_DISPLAYNAME | SHGFI_ICON | size);

	return shinfo.hIcon;
}