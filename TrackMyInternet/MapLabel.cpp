#include "stdafx.h"
#include "MapLabel.h"
#include "mapwidget.h"
#include "CountryIconProvider.h"

MapLabel::MapLabel(QWidget* parent=0): QLabel(parent)
{
	
	setMouseTracking(true);
	QLabel::setMouseTracking(true);
	parent->setMouseTracking(true);
	
}

void MapLabel::setWidget(MapWidget* w)
{
	this->w = w;
	this->mode = WorkingMode::HoverMode;
}

void MapLabel::setMode(WorkingMode mode)
{
	this->mode = mode;
}

MapLabel::~MapLabel()
{

}

void MapLabel::resizeEvent(QResizeEvent *event) 
{
	QLabel::resizeEvent(event);
}

void MapLabel::mouseMoveEvent(QMouseEvent * event)
{
	if (this->mode == WorkingMode::SaveMode)
	{
		QLabel::mouseMoveEvent(event);
		return;
	}

	uint32_t numProc = 0;
	uint32_t numCon = 0;

	//qDebug() << "EV: " << event->pos().x() << ", " << event->pos().y() << "\n";
	double xRap = (double)w->img->width() / (double)w->ui.mapLabel->width();
	double yRap = (double)w->img->height() / (double)w->ui.mapLabel->height();

	double x = event->pos().x() * xRap;
	double y = event->pos().y() * yRap;

	//QList<int> splitterWidgetSizes = { 1, 1 };
	//w->ui.splitter->setSizes(splitterWidgetSizes);

	QRgb pixel = w->legend->pixel(x, y);
	if (pixel != 0) {
		//	qDebug() << "Clicked on " << qRed(pixel) << legendMap[qRed(pixel)] << ", " << pixel << "|x,y: " << x << " " << y << "\n";
	}


	CountryIconProvider c;
	QPixmap px;


	QString selCountryIso = w->legendMap[qRed(pixel)];
	QIcon* ic = c.getCountryByIso(selCountryIso);


	QString clickedCountry = QString::fromStdString(w->isoToCountry[selCountryIso.toStdString()]);

	//qDebug() << "Clicked country: " << clickedCountry << "\n";

	if (clickedCountry != "")
	{
		if (event->type() != QEvent::Paint)
		{
			w->ui.countryLabel->setVisible(true);
			w->ui.flagLabel->setVisible(true);
			
			
			const int curFontSize = 13;

			QFont font = w->ui.countryLabel->font();
			if (clickedCountry.length() > 18) 
				font.setPointSize(curFontSize - 3);
			else 
				font.setPointSize(curFontSize);
			
			w->ui.appsNumberLabel->setText("Total apps");
			w->ui.connectionsNumberLabel->setText("Total connections");

			w->ui.countryLabel->setFont(font);
			w->ui.countryLabel->setText(clickedCountry);
			w->ui.flagLabel->setPixmap(ic->pixmap(ic->actualSize(QSize(32, 32))));
			//w->ui.countryGroupbox->setTitle(clickedCountry + " details ");
			//w->ui.countryGroupbox->setVisible(true);
			this->setCursor(Qt::PointingHandCursor);
		
			if (w->appCountryCount.find(clickedCountry.toStdString()) == w->appCountryCount.end())
			{
				w->ui.appsNumber->setText(QString::number(numProc));
			}
			else
			{
				map<string, UINT32> curStats = w->appCountryCount[clickedCountry.toStdString()];

				for (map<string, UINT32>::iterator it = curStats.begin(); it != curStats.end(); ++it)
				{
					if (it->second > 0)
					{
						numProc++;
					}
				}

				w->ui.appsNumber->setText(QString::number(numProc));
			}


			if (w->connCountryCount.find(clickedCountry.toStdString()) == w->connCountryCount.end())
			{
				w->ui.connectionsNumber->setText(QString::number(numCon));
			}
			else
			{
				numCon = w->connCountryCount[clickedCountry.toStdString()];
				w->ui.connectionsNumber->setText(QString::number(numCon));
			}
		
		}
	}

	else
	{
		//this->setCursor(Qt::ArrowCursor);
		//w->ui.countryGroupbox->setVisible(false);
		//w->ui.countryGroupbox->setTitle("No country selected");
		w->ui.appsNumber->setText("");
		w->ui.appsNumberLabel->setText("");
		w->ui.connectionsNumberLabel->setText("");
		w->ui.connectionsNumber->setText("");

		w->ui.countryLabel->setText("");
		w->ui.flagLabel->setText("");
		w->ui.flagLabel->clear();
		
		this->setCursor(Qt::ArrowCursor);
	}


	
}

