#pragma once

#include "QtWinExtras/qwinfunctions.h"
#include <shellapi.h>

std::wstring s2ws(const std::string& s);
HICON GetIconForFile(std::string filename, UINT size);

static inline QString prettySeconds(quint32 duration) {
	QString res;
	int seconds = (int)(duration % 60);
	duration /= 60;
	int minutes = (int)(duration % 60);
	duration /= 60;
	int hours = (int)(duration % 24);
	int days = (int)(duration / 24);

	if ((hours == 0) && (days == 0))
		return res.sprintf("%02d:%02d", minutes, seconds);

	if (days == 0)
		return res.sprintf("%02d:%02d:%02d", hours, minutes, seconds);

	return res.sprintf("%dd%02d:%02d:%02d", days, hours, minutes, seconds);
}

