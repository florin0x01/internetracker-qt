#include "stdafx.h"
#include "mapwidget.h"
//#include "ui_mapwidget.h"
#include <QMouseEvent>
#include <QTime>
#include <QDebug>
#include <QThread>
#include <QStack>
#include <QtMath>
#include <QInputDialog>
#include <QPainter>
#include <QBitmap>
#include <QRadioButton>
#include <QLinearGradient>
#include <QDiriterator>
#include <QMessageBox>
#include <QDockWidget>
#include <QListWidgetItem>
#include <iostream>
#include "ui_mapForm.h"
#include "CountryIconProvider.h"
#include "guicommon.h"
#include "maphelper.h"
#include "AppSettings.h"

static const QString userPath = ":/TrackMyInternet/maps/";
static int debugMode = 0;
static int selectionMode = 0;
static bool useCountryRects = true;


MapWidget::MapWidget(QWidget *parent, bool enableClick) :
	QWidget(parent), control(TMIControl::Instance()),update_time(TmiSettings::MapUpdateMs)
{
	ui.setupUi(this);
    img = NULL;
	total_elapsed = 0;
	this->parent = static_cast<QMainWindow*>(parent);
	
	img = new QPixmap(QString(":/TrackMyInternet/maps/world.jpg"));
	orig = new QPixmap(QString(":/TrackMyInternet/maps/world.jpg"));
	legend = new QImage(img->width(), img->height(), QImage::Format_ARGB32);
	legend->fill(QColor(0, 0, 0));

	qDebug() << "Map width: " <<img->width() << "\n";
	qDebug() << "Map height: " << img->height() << "\n";

	draw_time = 0;

	maskPixmap = new QPixmap(QString(":/TrackMyInternet/maps/world.jpg"));
	ui.selectAll->setChecked(true);
	ui.mapLabel->setWidget(this);
	//Settings->MapSpeedKBps = 100;
	
	//ui.mapLabel->installEventFilter(this);

	if (!selectionMode)
		this->invert();
	else
		ui.mapLabel->setMode(MapLabel::WorkingMode::SaveMode);
	
	ui.mapLabel->setPixmap(*img);

	if (!selectionMode)
		loadBins();

	ui.radioSpeed->setChecked(true);
//	ui.mapLabel->setPixmap(img->scaled(QSize(1280, 635), Qt::KeepAspectRatio));
	
	selectionTraffic = false;
	//QList<int> splitterWidgetSizes = { 1, 0 };
	//ui.splitter->setSizes(splitterWidgetSizes);

	xRap = 1;
	yRap = 1;
	origX = 1280;
	origY = 635;
	 
	//

	initIsoToCountry();
	Settings::Instance().MapSpeedKBps = Settings::Instance().MapSpeedKBps;
	if (!selectionMode)
	{
		
		this->type = TMIProcessCallback::LiveUpdate;
		QObject::connect(ui.radioSpeed, SIGNAL(toggled(bool)), this, SLOT(CheckRadio(bool)));
		QObject::connect(ui.radioTraffic, SIGNAL(toggled(bool)), this, SLOT(CheckRadioTraffic(bool)));
		QObject::connect(ui.selectAll, SIGNAL(toggled(bool)), this, SLOT(onSelectAllClicked(bool)));

		bool val = connect(ui.procView, SIGNAL(itemClicked(QListWidgetItem *)), this, SLOT(onItemClicked(QListWidgetItem*)));
		
		qDebug() << "MAP THR ID: " << QThread::currentThreadId();

		if (!val)
			qDebug() << "Failure connect item list widget\n";

		val = connect(&Settings::Instance(), SIGNAL(SettingsChanged()), this, SLOT(onSettingsChanged()));

		if (!val)
			qDebug() << "Failure settings changed\n";
		
		
		ui.procView->setIconSize(QSize(32, 32));
		ui.countryLabel->setText("");
		ui.flagLabel->setText("");

		//ui.countryGroupbox->setVisible(false);

		ui.speedSlider->setMinimum(8);
		ui.speedSlider->setMaximum(10 * 1024);
		ui.speedSlider->setSingleStep(64);

		double speedKbs = Settings::Instance().MapSpeedKBps;
		double transferKbs = Settings::Instance().MapTransferKBs;

		//SLIDERS
		ui.trafficSlider->setMinimum(10);
		ui.trafficSlider->setMaximum(100 * 1024);
		ui.trafficSlider->setSingleStep(64);

		ui.speedSlider->setValue(ceil(speedKbs));
		ui.speedSlider->setSliderPosition(ceil(speedKbs));
		onSliderSpeedChanged(ceil(speedKbs));
		
		ui.trafficSlider->setValue(ceil(transferKbs));
		ui.trafficSlider->setSliderPosition(ceil(transferKbs));
		onSliderTransferChanged(ceil(transferKbs));

		ui.trafficLabelFrame->setVisible(false);
		ui.trafficFrame->setVisible(false);

		QObject::connect(ui.speedSlider, SIGNAL(valueChanged(int)), SLOT(onSliderSpeedChanged(int)));
		QObject::connect(ui.trafficSlider, SIGNAL(valueChanged(int)), SLOT(onSliderTransferChanged(int)));

		QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(UpdateControl()));
		timer.start(Settings::Instance().MapUpdateMsec);

		control.RegisterClient(this);		
	}
	else {
		ui.mapLabel->setMouseTracking(false);
		ui.mapLabel->installEventFilter(this);
	}

}

void MapWidget::onSliderSpeedChanged(int val)
{

	double newVal = val;

	Settings::Instance().MapSpeedKBps = newVal;
	Settings::Instance().ChangeSettings();

	if (val >= 1024)
	{
		newVal /= 1024;
	}

	if (newVal == val)
		ui.speedSensitivtyLabel->setText("Speed Sensitivity: " + QString::number(newVal, 'g', 3) + " KB/s ");
	else
		ui.speedSensitivtyLabel->setText("Speed Sensitivity: " + QString::number(newVal, 'g', 3) + " MB/s ");
}

void MapWidget::onSliderTransferChanged(int val)
{
	double newVal = val;

	Settings::Instance().MapTransferKBs = newVal;
	Settings::Instance().ChangeSettings();

	if (val >= 1024)
	{
		newVal /= 1024;
	}


	if (newVal == val)
		ui.trafficSensitivtyLabel->setText("Traffic Sensitivity: " + QString::number(newVal,'g',4) + " KB ");
	else
		ui.trafficSensitivtyLabel->setText("Traffic Sensitivity: " + QString::number(newVal,'g',4) + " MB ");
}

void MapWidget::OnFlowStore(shared_ptr<TMIFlow> f)
{
	string procPath = f->GetProcessPath();
	if (procPath == "") return;

	auto iterator = processesSet.find(procPath);

	if (iterator != processesSet.end())
	{
		iterator->second.refCount--;

		connCountryCount[f->GetCountry()]--;
		appCountryCount[f->GetCountry()][f->GetProcessName()]--;

		if (iterator->second.refCount == 0)
		{
			if (includedProcs.find(iterator->second.processName) != includedProcs.end())
				includedProcs.erase(iterator->second.processName);

			removeProcessItem(iterator->second);
			processesSet.erase(iterator);
		}

	}
	else {
		string procP = procPath;
		std::cout << "NOT FOUND: " << procP << "\n";
	}
}

void MapWidget::OnFlowUpdate(shared_ptr<TMIFlow> f)
{
	string procPath = f->GetProcessPath();
	string procName = f->GetProcessName();

	auto iterator = processesSet.find(procPath);

	appCountryCount[f->GetCountry()][f->GetProcessName()]++;
	connCountryCount[f->GetCountry()]++;

	if (iterator == processesSet.end())
	{
		ProcessInfo info;
		addProcessItem(procName, procPath, info);
		processesSet[procPath] = info;

	}
	else {
		iterator->second.refCount++;
	}
}


void MapWidget::OnFlowAged(shared_ptr<TMIFlow> f)
{

}

void MapWidget::OnFlowSpeed(shared_ptr<TMIFlow> f)
{

}

void MapWidget::resetUIMaps()
{
	processesSet.clear();
	connCountryCount.clear();
	appCountryCount.clear();
	ui.procView->clear();
	listDetailsMap.clear();
}

void MapWidget::OnDataSourceChanged(UpdateType type)
{
	resetUIMaps();
	this->type = type;
}

void MapWidget::initIsoToCountry()
{
	isoToCountry["ax"] = "Aland Islands";
	isoToCountry["af"] = "Afghanistan";
	isoToCountry["al"] = "Albania";
	isoToCountry["dz"] = "Algeria";
	isoToCountry["as"] = "American Samoa";
	isoToCountry["ad"] = "Andorra";
	isoToCountry["ao"] = "Angola";
	isoToCountry["ai"] = "Anguilla";
	isoToCountry["aq"] = "Antarctica";
	isoToCountry["ag"] = "Antigua And Barbuda";
	isoToCountry["ar"] = "Argentina";
	isoToCountry["am"] = "Armenia";
	isoToCountry["aw"] = "Aruba";
	isoToCountry["au"] = "Australia";
	isoToCountry["at"] = "Austria";
	isoToCountry["az"] = "Azerbaijan";
	isoToCountry["bs"] = "Bahamas";
	isoToCountry["bh"] = "Bahrain";
	isoToCountry["bd"] = "Bangladesh";
	isoToCountry["bb"] = "Barbados";
	isoToCountry["by"] = "Belarus";
	isoToCountry["be"] = "Belgium";
	isoToCountry["bz"] = "Belize";
	isoToCountry["bj"] = "Benin";
	isoToCountry["bm"] = "Bermuda";
	isoToCountry["bt"] = "Bhutan";
	isoToCountry["bo"] = "Bolivia";
	isoToCountry["ba"] = "Bosnia Herzegowina";
	isoToCountry["bw"] = "Botswana";
	isoToCountry["bv"] = "Bouvet Island";
	isoToCountry["br"] = "Brazil";
	isoToCountry["bn"] = "Brunei";
	isoToCountry["bg"] = "Bulgaria";
	isoToCountry["bf"] = "Burkina Faso";
	isoToCountry["bi"] = "Burundi";
	isoToCountry["kh"] = "Cambodia";
	isoToCountry["cm"] = "Cameroon";
	isoToCountry["ca"] = "Canada";
	isoToCountry["cv"] = "Cape Verde";
	isoToCountry["ky"] = "Cayman Islands";
	isoToCountry["cf"] = "Central African Republic";
	isoToCountry["td"] = "Chad";
	isoToCountry["cl"] = "Chile";
	isoToCountry["cn"] = "China";
	isoToCountry["cx"] = "Christmas Island";
	isoToCountry["cc"] = "Cocos (keeling) Islands";
	isoToCountry["co"] = "Colombia";
	isoToCountry["km"] = "Comoros";
	isoToCountry["cd"] = "Congo Democratic Republic";
	isoToCountry["cg"] = "Republic Of Congo";
	isoToCountry["ck"] = "Cook Islands";
	isoToCountry["cr"] = "Costa Rica";
	isoToCountry["ci"] = "Cote D'ivoire";
	isoToCountry["hr"] = "Croatia";
	isoToCountry["cu"] = "Cuba";
	isoToCountry["cy"] = "Cyprus";
	isoToCountry["cz"] = "Czech Republic";
	isoToCountry["dk"] = "Denmark";
	isoToCountry["dj"] = "Djibouti";
	isoToCountry["dm"] = "Dominica";
	isoToCountry["do"] = "Dominican Republic";
	isoToCountry["ec"] = "Ecuador";
	isoToCountry["eg"] = "Egypt";
	isoToCountry["sv"] = "El Salvador";
	isoToCountry["gq"] = "Equatorial Guinea";
	isoToCountry["er"] = "Eritrea";
	isoToCountry["ee"] = "Estonia";
	isoToCountry["et"] = "Ethiopia";
	isoToCountry["fk"] = "Falkland Islands";
	isoToCountry["fo"] = "Faroe Islands";
	isoToCountry["fj"] = "Fiji";
	isoToCountry["fi"] = "Finland";
	isoToCountry["fr"] = "France";
	isoToCountry["gf"] = "French Guiana";
	isoToCountry["pf"] = "French Polynesia";
	isoToCountry["ga"] = "Gabon";
	isoToCountry["gm"] = "Gambia";
	isoToCountry["ge"] = "Georgia";
	isoToCountry["de"] = "Germany";
	isoToCountry["gh"] = "Ghana";
	isoToCountry["gi"] = "Gibraltar";
	isoToCountry["gr"] = "Greece";
	isoToCountry["gl"] = "Greenland";
	isoToCountry["gd"] = "Grenada";
	isoToCountry["gp"] = "Guadeloupe";
	isoToCountry["gu"] = "Guam";
	isoToCountry["gt"] = "Guatemala";
	isoToCountry["gn"] = "Guinea";
	isoToCountry["gw"] = "Guinea-bissau";
	isoToCountry["gy"] = "Guyana";
	isoToCountry["ht"] = "Haiti";
	isoToCountry["hn"] = "Honduras";
	isoToCountry["hk"] = "Hong Kong";
	isoToCountry["hu"] = "Hungary";
	isoToCountry["is"] = "Iceland";
	isoToCountry["in"] = "India";
	isoToCountry["id"] = "Indonesia";
	isoToCountry["ir"] = "Iran";
	isoToCountry["iq"] = "Iraq";
	isoToCountry["ie"] = "Ireland";
	isoToCountry["il"] = "Israel";
	isoToCountry["it"] = "Italy";
	isoToCountry["jm"] = "Jamaica";
	isoToCountry["jp"] = "Japan";
	isoToCountry["jo"] = "Jordan";
	isoToCountry["kz"] = "Kazakhstan";
	isoToCountry["ke"] = "Kenya";
	isoToCountry["ki"] = "Kiribati";
	isoToCountry["kp"] = "Democratic People's Republic Of Korea";
	isoToCountry["kr"] = "Republic Of Korea";
	isoToCountry["kw"] = "Kuwait";
	isoToCountry["kg"] = "Kyrgyzstan";
	isoToCountry["la"] = "Lao People's Democratic Republic";
	isoToCountry["lv"] = "Latvia";
	isoToCountry["lb"] = "Lebanon";
	isoToCountry["ls"] = "Lesotho";
	isoToCountry["lr"] = "Liberia";
	isoToCountry["ly"] = "Libyan Arab Jamahiriya";
	isoToCountry["li"] = "Liechtenstein";
	isoToCountry["lt"] = "Lithuania";
	isoToCountry["lu"] = "Luxembourg";
	isoToCountry["mo"] = "Macau";
	isoToCountry["mk"] = "Macedonia";
	isoToCountry["mg"] = "Madagascar";
	isoToCountry["mw"] = "Malawi";
	isoToCountry["my"] = "Malaysia";
	isoToCountry["mv"] = "Maldives";
	isoToCountry["ml"] = "Mali";
	isoToCountry["mt"] = "Malta";
	isoToCountry["mh"] = "Marshall Islands";
	isoToCountry["mq"] = "Martinique";
	isoToCountry["mr"] = "Mauritania";
	isoToCountry["mu"] = "Mauritius";
	isoToCountry["yt"] = "Mayotte";
	isoToCountry["mx"] = "Mexico";
	isoToCountry["fm"] = "Micronesia Federated States Of";
	isoToCountry["md"] = "Moldova Republic Of";
	isoToCountry["mc"] = "Monaco";
	isoToCountry["mn"] = "Mongolia";
	isoToCountry["ms"] = "Montserrat";
	isoToCountry["ma"] = "Morocco";
	isoToCountry["mz"] = "Mozambique";
	isoToCountry["mm"] = "Myanmar";
	isoToCountry["na"] = "Namibia";
	isoToCountry["nr"] = "Nauru";
	isoToCountry["np"] = "Nepal";
	isoToCountry["nl"] = "Netherlands";
	isoToCountry["an"] = "Netherlands Antilles";
	isoToCountry["nc"] = "New Caledonia";
	isoToCountry["nz"] = "New Zealand";
	isoToCountry["ni"] = "Nicaragua";
	isoToCountry["ne"] = "Niger";
	isoToCountry["ng"] = "Nigeria";
	isoToCountry["nu"] = "Niue";
	isoToCountry["nf"] = "Norfolk Island";
	isoToCountry["mp"] = "Northern Mariana Islands";
	isoToCountry["no"] = "Norway";
	isoToCountry["om"] = "Oman";
	isoToCountry["pk"] = "Pakistan";
	isoToCountry["pw"] = "Palau";
	isoToCountry["pa"] = "Panama";
	isoToCountry["pg"] = "Papua New Guinea";
	isoToCountry["py"] = "Paraguay";
	isoToCountry["pe"] = "Peru";
	isoToCountry["ph"] = "Philippines";
	isoToCountry["pn"] = "Pitcairn";
	isoToCountry["pl"] = "Poland";
	isoToCountry["pt"] = "Portugal";
	isoToCountry["pr"] = "Puerto Rico";
	isoToCountry["qa"] = "Qatar";
	isoToCountry["re"] = "Reunion";
	isoToCountry["ro"] = "Romania";
	isoToCountry["ru"] = "Russian Federation";
	isoToCountry["rw"] = "Rwanda";
	isoToCountry["sh"] = "Saint Helena";
	isoToCountry["kn"] = "Saint Kitts And Nevis";
	isoToCountry["lc"] = "Saint Lucia";
	isoToCountry["pm"] = "Saint Pierre And Miquelon";
	isoToCountry["vc"] = "Saint Vincent And The Grenadines";
	isoToCountry["ws"] = "Samoa";
	isoToCountry["sm"] = "San Marino";
	isoToCountry["st"] = "Sao Tome And Principe";
	isoToCountry["sa"] = "Saudi Arabia";
	isoToCountry["sn"] = "Senegal";
	isoToCountry["cs"] = "Serbia And Montenegro";
	isoToCountry["sc"] = "Seychelles";
	isoToCountry["sl"] = "Sierra Leone";
	isoToCountry["sg"] = "Singapore";
	isoToCountry["sk"] = "Slovakia";
	isoToCountry["si"] = "Slovenia";
	isoToCountry["sb"] = "Solomon Islands";
	isoToCountry["so"] = "Somalia";
	isoToCountry["za"] = "South Africa";
	isoToCountry["gs"] = "South Georgia And The South Sandwich Islands";
	isoToCountry["es"] = "Spain";
	isoToCountry["lk"] = "Sri Lanka";
	isoToCountry["sd"] = "Sudan";
	isoToCountry["sr"] = "Suriname";
	isoToCountry["sj"] = "Svalbard And Jan Mayen Islands";
	isoToCountry["sz"] = "Swaziland";
	isoToCountry["se"] = "Sweden";
	isoToCountry["ch"] = "Switzerland";
	isoToCountry["sy"] = "Syrian Arab Republic";
	isoToCountry["tw"] = "Taiwan";
	isoToCountry["tj"] = "Tajikistan";
	isoToCountry["tz"] = "Tanzania United Republic Of";
	isoToCountry["th"] = "Thailand";
	isoToCountry["tl"] = "Timor-leste";
	isoToCountry["tg"] = "Togo";
	isoToCountry["tk"] = "Tokelau";
	isoToCountry["to"] = "Tonga";
	isoToCountry["tt"] = "Trinidad And Tobago";
	isoToCountry["tn"] = "Tunisia";
	isoToCountry["tr"] = "Turkey";
	isoToCountry["tm"] = "Turkmenistan";
	isoToCountry["tc"] = "Turks And Caicos Islands";
	isoToCountry["tv"] = "Tuvalu";
	isoToCountry["ug"] = "Uganda";
	isoToCountry["ua"] = "Ukraine";
	isoToCountry["ae"] = "United Arab Emirates";
	isoToCountry["gb"] = "United Kingdom";
	isoToCountry["us"] = "United States";
	isoToCountry["um"] = "United States Minor Outlying Islands";
	isoToCountry["uy"] = "Uruguay";
	isoToCountry["uz"] = "Uzbekistan";
	isoToCountry["vu"] = "Vanuatu";
	isoToCountry["va"] = "Vatican City";
	isoToCountry["ve"] = "Venezuela";
	isoToCountry["vn"] = "Vietnam";
	isoToCountry["vg"] = "Virgin Islands (british)";
	isoToCountry["vi"] = "Virgin Islands (u.s.)";
	isoToCountry["wf"] = "Wallis And Futuna Islands";
	isoToCountry["eh"] = "Western Sahara";
	isoToCountry["ye"] = "Yemen";
	isoToCountry["zm"] = "Zambia";
	isoToCountry["zw"] = "Zimbabwe";

}

void MapWidget::onItemClicked(QListWidgetItem* item)
{
	string itemtext = item->text().toStdString();

	if (item->checkState() == Qt::CheckState::Checked)
		includedProcs[itemtext] = true;
	else 
	{
		if (includedProcs.find(itemtext) != includedProcs.end())
			includedProcs.erase(itemtext);
	}

	qDebug () << "Clicked on " << item->text() << " checked " << item->checkState();

	UpdateControl();

	timer.stop();
	timer.start(Settings::Instance().MapUpdateMsec);
}

void MapWidget::onSettingsChanged()
{
	qDebug() << "On Settings Changed MapWidget\n";
	UpdateControl();
	
	timer.stop();
	timer.start(Settings::Instance().MapUpdateMsec);


}

void MapWidget::CheckRadio(bool isToggled)
{
	selectionTraffic = false;


	ui.speedLabelFrame->setVisible(true);
	ui.speedFrame->setVisible(true);
	ui.trafficLabelFrame->setVisible(false);
	ui.trafficFrame->setVisible(false);

	UpdateControl();

	timer.stop();
	timer.start(Settings::Instance().MapUpdateMsec);
}


void MapWidget::CheckRadioTraffic(bool isToggled)
{	
	selectionTraffic = true;
	UpdateControl();

	ui.trafficLabelFrame->setVisible(true);
	ui.trafficFrame->setVisible(true);
	ui.speedLabelFrame->setVisible(false);
	ui.speedFrame->setVisible(false);

	timer.stop();
	timer.start(update_time);	
}

void MapWidget::onSelectAllClicked(bool is)
{	
	for (QMap<QString, QListWidgetItem*>::iterator it = listDetailsMap.begin(); it != listDetailsMap.end(); ++it)
	{
		if (is) {
			includedProcs[it.key().toStdString()] = true;
			it.value()->setCheckState(Qt::Checked);
		}
		else {
			it.value()->setCheckState(Qt::Unchecked);
			includedProcs.erase(it.key().toStdString());
		}
	}

	UpdateControl();

	timer.stop();
	timer.start(update_time);
}

void MapWidget::loadBins()
{
	QDirIterator it(":/TrackMyInternet/maps", QStringList() << "*2.png", QDir::Files);
	int colorIdx = 1;
	
	while (it.hasNext()) {
		QString map = it.next();
		if (map.contains("world.jpg") || map.contains(".png.bin"))
		{
			continue;
		}

		map = map.mid(map.lastIndexOf("/", map.lastIndexOf("."))+1,2);
	
		this->_lightCountry(*legend, map, QColor(colorIdx, 0, 0));
		this->_lightCountry(*orig, map, QColor(colorIdx + 10, colorIdx + 10, colorIdx + 10));
		QColor cl = QColor(colorIdx, 0, 0);
		
		legendMap[qRed(cl.rgb())] = map;
		colorIdx++;
		
	}
}


QColor interpolate(QColor start, QColor end, double ratio)
{
	int r = (int)(ratio*start.red() + (1 - ratio)*end.red());
	int g = (int)(ratio*start.green() + (1 - ratio)*end.green());
	int b = (int)(ratio*start.blue() + (1 - ratio)*end.blue());
	return QColor::fromRgb(r, g, b);
}

QColor getColor(double value)
{
	QList<QColor> mGradientColors;
	mGradientColors.append(QColor(0, 100, 0));
	mGradientColors.append(QColor(0, 255, 0));
	mGradientColors.append(QColor(180, 0, 0));
	mGradientColors.append(QColor(255, 0, 0));

	if (value > 1.0f) {
		value = 1.0f;
	}

	//Asume mGradientColors.count()>1 and value=[0,1]
	double stepbase = 1.0 / (mGradientColors.count() - 1);
	int interval = mGradientColors.count() - 1; //to fix 1<=0.99999999;

	for (int i = 1; i<mGradientColors.count(); i++)//remove begin and end
	{
		if (value <= i*stepbase) { interval = i; break; }
	}
	double percentage = (value - stepbase*(interval - 1)) / stepbase;
	QColor color(interpolate(mGradientColors[interval], mGradientColors[interval - 1], value));
	return color;
}

void MapWidget::UpdateControl()
{
	
	map<string, double> percs;
	map<string, string> processes;
	map<string, double>::iterator cPercentage;

	if (selectionTraffic == false)
		percs = control.GetSnapshotCountriesPercentages(type, processes, includedProcs);
	else
		percs = control.GetSnapshotCountriesPerTransfer(type, processes, includedProcs);


	*img = orig->copy();

	for (cPercentage = percs.begin(); cPercentage != percs.end(); ++cPercentage)
	{

		QString qCountry = QString::fromStdString(cPercentage->first);
		double position = cPercentage->second;
	//	qDebug() << "Pos: " << position << "\n";
		
		if (position > 0.01) {
			this->lightCountry(qCountry, getColor(position));
			
		}		
		
    }
	ui.mapLabel->setPixmap(*img);
}

void MapWidget::addProcessItem(const string& name, const string& path, ProcessInfo &info)
{
	HICON hIcon = GetIconForFile(path, SHGFI_LARGEICON);
	QPixmap pixmap = QtWin::fromHICON(hIcon);
	QIcon ico = QIcon(pixmap);
	QString qName = QString::fromStdString(name);

	QListWidgetItem *it1 = new QListWidgetItem(ico, qName, ui.procView);	
	it1->setFlags(it1->flags() | Qt::ItemIsUserCheckable); // set checkable flag
	it1->setCheckState(Qt::Checked); // AND initialize check state
	listDetailsMap[qName] = it1;
	includedProcs[name] = true;

	info.item = it1;
	info.processName = name;
	info.refCount = 1;
}

void MapWidget::removeProcessItem(ProcessInfo &info)
{
	ui.procView->removeItemWidget(info.item);
	listDetailsMap.remove(QString::fromStdString(info.processName));
	delete info.item;
	ui.procView->reset();
}

 MapWidget::~MapWidget()
{

  //  if (mapLabel)
    //    delete mapLabel;

    if (img)
        delete img;

	if (orig)
		delete orig;

	if (legend)
		delete legend;

	for (auto it = pixmaps.begin(); it != pixmaps.end(); ++it)
	{
		if (it.value()) delete it.value();
	}

	pixmaps.clear();
}

 QSize MapWidget::sizeHint() const
 {
    QSize size = img->size();
    return size;
 }

 void MapWidget::processBoundingBox(const QString& country)
 {
	 QString countryRectPath = country + QString(".bin");
	 countryRectPath = countryRectPath.replace("2", "");

	 QRect countryRect;

	 if (pixmaps.find(country) == pixmaps.end())
	 {
		 pixmaps[country] = new QPixmap(country,"png",Qt::ImageConversionFlag::ColorOnly);
	 }

	 const QPixmap* cur = pixmaps[country];

	 if (!useCountryRects) 
	 {
		 countryRects[country] = cur->rect();
		 return;
	 }

	/* qDebug() << country << "RECT: " 
				<< cur->rect().topLeft().x() << "," 
				<< cur->rect().topLeft().y() << " | " 
				<< cur->rect().bottomRight().x() << "," << cur->rect().bottomRight().y() << "\n";
				*/

	 if (countryRects.find(country) == countryRects.end() && useCountryRects)
	 {
		 QFile f(countryRectPath);
		 QDataStream in(&f);


		 if (f.open(QIODevice::ReadOnly))
		 {
			
			 QPoint left, bottom;
			 in >> left >> bottom;

			 f.close();

			 countryRect = QRect(left, bottom);

			 countryRects.insert(country, countryRect);
		 }
		 else 
		 {
			 countryRects.insert(country, cur->rect());
		 }
	 }

	// countryRects[":/TrackMyInternet/maps/mk2.png"] = QRect(QPoint(596, 169), QPoint(606, 176));

 }

 void MapWidget::lightCountry(const QString& _country, QColor c)
 {
	 this->_lightCountry(*img, _country, c);
 }

 void MapWidget::lightsOffCountry(const QString& _country)
 {
	 this->_lightsOffCountry(*img, _country);
 }

 void  MapWidget::_lightsOffCountry(QPaintDevice& img, const QString& _country)
{
     const QString country = QString(userPath + _country.toLower() + QString("2.png"));
     
	 processBoundingBox(country);

     painter.begin((QImage*)&img);
	
     painter.setCompositionMode(QPainter::CompositionMode::CompositionMode_SourceOver);
     painter.drawPixmap(countryRects[country], *pixmaps[country], countryRects[country]);

	
	// ui.mapLabel->setPixmap(QPixmap::fromImage(*img));
	 painter.end();
}

 void MapWidget::_lightCountry(QPaintDevice& img, const QString &_country, QColor c)
 {
	 const QString country = QString(userPath + _country.toLower() + QString("2.png"));
	 QLinearGradient gd;
	 
	 processBoundingBox(country);

	 c.setAlpha(255);

	

	 QPixmap maskPixmap = pixmaps[country]->copy();
	
	 if (maskPixmap.width() == 0 || maskPixmap.height() == 0)
	 {
		 qDebug() << "WARN! Pixmap is " << maskPixmap.size() << " for country " << _country << "\n";
		 return;
	 }

	 maskPixmap.fill(c.rgba());
	 painter.begin(&maskPixmap);

	 painter.setCompositionMode(QPainter::CompositionMode::CompositionMode_DestinationAtop);
	 painter.drawPixmap(maskPixmap.rect(), *pixmaps[country], pixmaps[country]->rect());
	 painter.end();
	
	 painter2.begin(&img);
	
	 painter2.setCompositionMode(QPainter::CompositionMode::CompositionMode_SourceOver);
	 painter2.drawPixmap(countryRects[country], maskPixmap, maskPixmap.rect());
	 painter2.end();	 
 }

void MapWidget::invert()
{
   
   painter.begin(img);

    painter.setCompositionMode(QPainter::CompositionMode::RasterOp_NotSource);
	
    painter.drawPixmap(img->rect(), *img, img->rect());

	painter.end();

	*orig = img->copy();
}
  
bool MapWidget::eventFilter(QObject *o, QEvent *event) {

	this->setCursor(Qt::ArrowCursor);

    if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent* MouseEvGrip = (QMouseEvent*)event;
		//this->setCursor(Qt::PointingHandCursor);
     		
		if (selectionMode == 0) {
			
			xRap = (double)img->width() / (double)ui.mapLabel->width();
			yRap = (double)img->height() / (double)ui.mapLabel->height();
			
			double x = MouseEvGrip->pos().x() * xRap;
			double y = MouseEvGrip->pos().y() * yRap;
	
			QList<int> splitterWidgetSizes = { 1, 1 };
			//ui.splitter->setSizes(splitterWidgetSizes);
			QRgb pixel = legend->pixel(x, y);
			if (pixel != 0) {
			//	qDebug() << "Clicked on " << qRed(pixel) << legendMap[qRed(pixel)] << ", " << pixel << "|x,y: " << x << " " << y << "\n";
			}

			
			CountryIconProvider c;
			QPixmap px;
			
			
			QString selCountryIso = legendMap[qRed(pixel)];
			QIcon* ic = c.getCountryByIso(selCountryIso);

			QString clickedCountry = QString::fromStdString(isoToCountry[selCountryIso.toStdString()]);
			
			//qDebug() << "Clicked country: " << clickedCountry << "\n";

			if (clickedCountry != "") 
			{
				if (event->type() != QEvent::Paint ) 
				{
					ui.countryLabel->setText(clickedCountry);
					ui.flagLabel->setPixmap(ic->pixmap(ic->actualSize(QSize(32, 32))));

					//ui.countryGroupbox->setTitle(clickedCountry + " details ");
					//ui.countryGroupbox->setVisible(true);
					this->setCursor(Qt::PointingHandCursor);
				}
				
			}

			else 
			{
				//this->setCursor(Qt::ArrowCursor);
				//ui.countryGroupbox->setVisible(false);
			}
			uint32_t numProc = 0;
			uint32_t numCon = 0;

			if (appCountryCount.find(clickedCountry.toStdString()) == appCountryCount.end())
			{
				
			}
			else 
			{
				map<string, UINT32> curStats = appCountryCount[clickedCountry.toStdString()];

				for (map<string, UINT32>::iterator it = curStats.begin(); it != curStats.end(); ++it)
				{
					if (it->second > 0)
					{
						numProc++;
					}
				}

				
			}

			if (connCountryCount.find(clickedCountry.toStdString()) == connCountryCount.end())
			{
				ui.connectionsNumber->setText(QString::number(numCon));
			}
			else
				numCon = connCountryCount[clickedCountry.toStdString()];
			
			//if (ui.countryGroupbox->title() != "No country selected") 
			//{
				ui.appsNumber->setText(QString::number(numProc));
				ui.connectionsNumber->setText(QString::number(numCon));
			//}
			//else
			//{
				ui.appsNumber->setText("");
				ui.connectionsNumber->setText("");
			//}
		}

		else {
			//editing country save mode - not used in production app

			double x = ui.mapLabel->mapFromParent(MouseEvGrip->pos()).x();
			double y = ui.mapLabel->mapFromParent(MouseEvGrip->pos()).y();

			QImage image = ui.mapLabel->pixmap()->toImage();						 
			QImage dstImg(image.width(), image.height(), QImage::Format_ARGB32);
			QImage redImg(image.width(), image.height(), QImage::Format_ARGB32);
			bool ok;
			QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
				tr("Country name:"), QLineEdit::Normal, "country", &ok);

			if (ok && !text.isEmpty()) {

				QColor c;
				c.setRed(0);
				c.setGreen(0);
				c.setBlue(0);
				c.setAlpha(0);

				//alpha 0 - transparent
				dstImg.fill(c.rgba());
				this->rfloodFill(image, x, y, QColor::fromRgb(image.pixel(x, y)), dstImg);
				dstImg.save(text + QString(".png"));

				//set red background
				c.setRed(255);
				c.setGreen(32);
				c.setBlue(0);
				c.setAlpha(255);
				redImg.fill(c.rgba());


				//obtain red mask
				qDebug() << "RED MASK\n";
				QPainter painter(&redImg);
				painter.setCompositionMode(QPainter::CompositionMode::CompositionMode_DestinationAtop);
				painter.drawPixmap(dstImg.rect(), QPixmap::fromImage(dstImg), dstImg.rect());
				qDebug() << "END RED MASK\n";

				//draw on top of map, combine
				qDebug() << "DRAW on top \n";
				QPainter painter2(&image);
				painter2.setCompositionMode(QPainter::CompositionMode::CompositionMode_Plus);
				painter2.drawPixmap(dstImg.rect(), QPixmap::fromImage(redImg), dstImg.rect());
				qDebug() << "END DRAW on top \n";

				ui.mapLabel->setPixmap(ui.mapLabel->pixmap()->fromImage(image));
			}

		}
		
		return true;
    }
    return false;
}

bool MapWidget::Similar(const QColor &col1, const QColor &col2)
{

    qreal r1 = col1.redF();
    qreal g1 = col1.greenF();
    qreal b1 = col1.blueF();

    qreal r2 = col2.redF();
    qreal g2 = col2.greenF();
    qreal b2 = col2.blueF();

    const qreal offset = 0.35;

    /*
    qDebug() << "Offset:" << offset << "\n";
    qDebug() << "r1,g1,b1: " << r1 << g1 << b1 << "\n";
    qDebug() << "r2,g2,b2: " << r2 << g2 << b2 << "\n";
*/

    if (qFabs(r2-r1) <= offset && qFabs(g2-g1) <= offset && qFabs(b2-b1) <= offset )
    {
        return true;
    }

    return false;



}

void MapWidget::rfloodFill(QImage& image, int x, int y, const QColor &curColor, QImage& dstImg)
{

    const int rgbFill = 1;
    QColor c;
    c.setRed(1);
    c.setGreen(1);
    c.setBlue(1);
    c.setAlpha(255);
//    dstImg.fill(c.rgba());


    if (x >= image.width() || y >= image.width() || x< 0 || y < 0)

       return;

   // qDebug() << "x,y" << x << y << "\n";


    QRgb pixColor = image.pixel(x,y);

    if (pixColor != rgbFill && !Similar(QColor::fromRgb(pixColor),curColor)) {
      //  qDebug() << "COLOR " <<QColor::fromRgb(pixColor) << " != " << curColor <<" \n";
   //     polygon << QPoint(x,y);
        return;
    }



    image.setPixel(x,y, c.rgba());
    dstImg.setPixel(x,y, c.rgba());

//    ui->mapLabel->setPixmap(ui->mapLabel->pixmap()->fromImage(image));


    rfloodFill(image, x + 1, y, curColor, dstImg);
    rfloodFill(image, x - 1, y, curColor, dstImg);
    rfloodFill(image, x, y + 1, curColor, dstImg);
    rfloodFill(image, x, y - 1, curColor, dstImg);
/*
   rfloodFill(image, x+1, y+1, curColor);
    rfloodFill(image, x-1, y-1, curColor);
    rfloodFill(image, x-1, y+1, curColor);
    rfloodFill(image, x+1, y-1, curColor);
    */

}


void MapWidget::floodFill(QImage& image, int x, int y, const QColor &curColor)
{
    QStack<QPoint> stack;
    qDebug() << "x,y" << x << y << "\n";

    /*
    static const int dx[8] = {0, 1, 1, 1, 0, -1, -1, -1}; // relative neighbor x coordinates
    static const int dy[8] = {-1, -1, 0, 1, 1, 1, 0, -1}; // relative neighbor y coordinates
*/
    static const int dx[8] = {1,-1,0,0,1,-1,-1,1};
    static const int dy[8] = {0,0,1,-1,1,-1,1,-1};

    QRgb pixColor = image.pixel(x,y);
    QPoint pt = QPoint(x,y);

    stack.push(pt);

    while(stack.empty() == false) {
        QPoint pt = stack.pop();

        qDebug() << "Pop " << pt.x() << ", " << pt.y() << "\n";

        QRgb pixColor = image.pixel(pt);


        image.setPixel(x,y, 255);

        for(int i = 0; i < 8; i++) {
              int nx = x + dx[i];
              int ny = y + dy[i];

              QPoint newPoint = QPoint(nx, ny);

              if (QColor::fromRgb(image.pixel(newPoint)) != curColor) {
                  qDebug() << "COLOR " <<QColor::fromRgb(pixColor) << " != " << curColor <<" \n";
                  continue;
              }


              if(nx > 0 && ny > 0) {
                qDebug() << "Push " << newPoint.x() << " " << newPoint.y() << "\n";
                  stack.push(newPoint);
                  //if(!push(nx, ny)) return;
              }
            }
    }

}

void MapWidget::onClick()
{

}
