#include "stdafx.h"
#include "trackmyinternet.h"
#include "tmi.h"
#include "AppTreeModel.h"
#include "CountryIconProvider.h"
#include "mapwidget.h"
#include <qt_windows.h>
#include "ui_MapForm.h"
#include "AppSettings.h"
#include "guicommon.h"
#include <QtWinExtras\qwinfunctions.h>


void TrackMyInternet::createActions()
{
	minimizeAction = new QAction(tr("Mi&nimize"), this);
	connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

	maximizeAction = new QAction(tr("Ma&ximize"), this);
	connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

	restoreAction = new QAction(tr("&Restore"), this);
	connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

	quitAction = new QAction(tr("&Quit"), this);
	connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
	
	
	Settings::Instance();

}



void TrackMyInternet::onMapTransferUnitChanged(const QString& bos)
{
	
}

void TrackMyInternet::createTrayIcon()
{
	trayIconMenu = new QMenu(this);
	trayIconMenu->addAction(minimizeAction);
	trayIconMenu->addAction(maximizeAction);
	trayIconMenu->addAction(restoreAction);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(quitAction);

	trayIcon = new QSystemTrayIcon(QIcon(":/icons/icon_on"), this);
	trayIcon->setContextMenu(trayIconMenu);
}

void TrackMyInternet::UpdateControl() {	
	QString string = QString("Processed: %1 flows").arg(control.Process());	
	statusLabel->setText(string);
}

void TrackMyInternet::goOnline() {
	currentMode = TMIProcessCallback::LiveUpdate;
	TMIControl::Instance().switchDataSource(TMIProcessCallback::LiveUpdate);
	ui.slider->setValue(ui.slider->maximum());
	ui.buttonPlay->setIcon(QIcon(":/icons/pause"));
	ui.labelLive->setText("Live");
}

void TrackMyInternet::goOffline(UINT64 time) {
	TMIControl::Instance().switchDataSource(TMIProcessCallback::OfflineUpdate);
	appsView->reset();
	currentMode = TMIProcessCallback::OfflineUpdate;
	DBFilterBuilder::getInstance().setTimeRef(time);
	DBFilterBuilder::getInstance().setTimeWindow(3600);
	/*stringstream ss;
	ss << "Select *,count(id) as groupByCount, sum(bytesRx) as bytesRx, sum(bytesTx) as bytesTx, sum(packetsRx) as packetsRx, sum(packetsTx) as packetsTx from flows ";
	ss << " where start >= " << time - S_TO_NS(3600*4) << " and end <= " << time + S_TO_NS(3600*4);
	ss << " group by app, country_iso ORDER by start";*/

	//qDebug() << "Query: " << QString::fromStdString(ss.str());	
	string filter = DBFilterBuilder::getInstance().getFilter();
	qDebug() << "filter: " << QString::fromStdString(filter);
	TMIControl::Instance().setDataSourceFilter(filter);
	
	ui.buttonPlay->setIcon(QIcon(":/icons/play"));		
	ui.labelLive->setText(QString("%1 hours ago").arg(ui.slider->maximum() - ui.slider->value()));
}

void TrackMyInternet::onSliderValueChanged(int value) {
	if (value == ui.slider->maximum()) {
		goOnline();
	}
	else {
		goOffline(H_TO_NS(ui.slider->value()) + oldestFlow->GetStartTime().QuadPart);
	}
	
}

void TrackMyInternet::UpdateDbPlayer() {
	// get oldest TS
	oldestFlow = control.getOldestFlow();

	// get current TS
	FILETIME _time;	
	GetSystemTimeAsFileTime(&_time);
	currentTime.LowPart = _time.dwLowDateTime;
	currentTime.HighPart = _time.dwHighDateTime;

	// set vlc player min and max
	ui.slider->setMaximum(NS_TO_H(currentTime.QuadPart) - NS_TO_H(oldestFlow->GetStartTime().QuadPart));
	ui.slider->setMinimum(0);
	
	UINT64 max_h = NS_TO_H(currentTime.QuadPart);
	UINT64 min_h = NS_TO_H(oldestFlow->GetStartTime().QuadPart);
	int max = NS_TO_H(currentTime.QuadPart);
	int min = NS_TO_H(oldestFlow->GetStartTime().QuadPart);
	if (currentMode == TMIProcessCallback::LiveUpdate) {
		ui.slider->setValue(ui.slider->maximum());
	} else {
		if (ui.slider->value() < ui.slider->minimum()) {
			ui.slider->setValue(ui.slider->minimum());
		}
	}	

	auto value = ui.slider->value();
}

	
void TrackMyInternet::showMap()
{
	MapWidget* map = new MapWidget(this, false);
	ui.mapLayout->addWidget(map);
}


TrackMyInternet::TrackMyInternet(QWidget *parent)
	: QMainWindow(parent), control(TMIControl::Instance())
{
	ui.setupUi(this);
	ui.tabWidget->setCurrentIndex(0);
	control.OpenDriver();	
	
	appsView = new AppsView(this);
	ui.horizontalLayout->addWidget(appsView);

	createActions();

	connect(&control_timer, SIGNAL(timeout()), this, SLOT(UpdateControl()));
	control_timer.start(TmiSettings::TreeUpdateMsec);

	connect(&db_player_timer, SIGNAL(timeout()), this, SLOT(UpdateDbPlayer()));
	db_player_timer.start(10000);

	createTrayIcon();
	trayIcon->show();
	showMap();

	control.RegisterClient(appsView);
	
	qApp->setStyle(QStyleFactory::create("fusion"));

	QPalette palette;
	palette.setColor(QPalette::Window, QColor(43, 43, 43).darker());
	palette.setColor(QPalette::WindowText, Qt::white);
	palette.setColor(QPalette::Base, QColor(15, 15, 15).darker());
	palette.setColor(QPalette::AlternateBase, QColor(43, 43, 43));
	palette.setColor(QPalette::ToolTipBase, Qt::white);
	palette.setColor(QPalette::ToolTipText, Qt::white);
	palette.setColor(QPalette::Text, Qt::white);
	palette.setColor(QPalette::Button, QColor(43, 43, 43));
	palette.setColor(QPalette::ButtonText, Qt::white);
	palette.setColor(QPalette::BrightText, Qt::red);

	palette.setColor(QPalette::Highlight, QColor(168, 168, 160));
	palette.setColor(QPalette::HighlightedText, Qt::black);
	palette.setColor(QPalette::Disabled, QPalette::Text, Qt::darkGray);
	palette.setColor(QPalette::Disabled, QPalette::ButtonText, Qt::darkGray);

	qApp->setPalette(palette);


	QtWin::extendFrameIntoClientArea(this, -1, -1, -1, -1);
	
	setAttribute(Qt::WA_TranslucentBackground, true);
	setAttribute(Qt::WA_NoSystemBackground, false);	
	QtWin::enableBlurBehindWindow(this);	
	

	// ALPHA BUILD
	ui.statusBar->setVisible(false);

	statusLabel = new QLabel(ui.statusBar);
	ui.statusBar->addWidget(statusLabel);

	progressBar = new QProgressBar(ui.statusBar);
	ui.statusBar->addWidget(progressBar);
	progressBar->setVisible(false);
	ui.overviewFrame->setVisible(false);
	
	QFile file(":/qss/style.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	ui.statusBar->setStyleSheet(styleSheet);
	

	control.RegisterClient(&dbClient);	

	// vlc control setup
	currentMode = TMIProcessCallback::LiveUpdate;
	ui.slider->setMinimum(0);
	ui.slider->setMaximum(100);
	ui.slider->setValue(100);
	ui.buttonPlay->setIcon(QIcon(":/icons/pause"));
	connect(ui.buttonPlay, SIGNAL(clicked(bool)), this, SLOT(buttonPlayClicked(bool)));
	connect(ui.slider, SIGNAL(sliderMoved(int)), SLOT(onSliderValueChanged(int)));
	connect(ui.buttonForward, SIGNAL(clicked(bool)), this, SLOT(buttonForwardClicked(bool)));
	connect(ui.buttonBack, SIGNAL(clicked(bool)), this, SLOT(buttonBackClicked(bool)));
	UpdateDbPlayer();
}

void TrackMyInternet::buttonPlayClicked(bool checked) {
	if (currentMode == TMIProcessCallback::LiveUpdate) {
		goOffline(oldestFlow->GetStartTime().QuadPart);
	} else {		
		goOnline();
	}
}

void TrackMyInternet::buttonForwardClicked(bool checked) {
	ui.slider->setValue(ui.slider->value() + 1);
	onSliderValueChanged(ui.slider->value());
}

void TrackMyInternet::buttonBackClicked(bool checked) {
	ui.slider->setValue(ui.slider->value() - 1);
	onSliderValueChanged(ui.slider->value());
}

TrackMyInternet::~TrackMyInternet()
{	
		
}
