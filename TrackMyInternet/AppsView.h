#ifndef APPSVIEW_H
#define APPSVIEW_H

#include <QWidget>
#include "tmi.h"
#include "AppTreeModel.h"

class TrackMyInternet;

class AppsTreeView : public QTreeView {
public:
	AppsTreeView(QWidget *parent = 0) :QTreeView(parent) {
	}
	void rowsInserted(const QModelIndex &parent, int start, int end) Q_DECL_OVERRIDE;
	QString getFilter() const { return filterString; }
	void setFilter(const QString &newFilter) { filterString = newFilter; }
	
	// search functionality // hide nodes
	void filter(TreeItem *root, const QString &filter);
	void filter2(TreeItem *root, const QString &filter);

	// search
	void _filter(TreeItem *item, string &filter);

	// optimized variant
	void _filter2(TreeItem *item, string &filter);
	
private:
	QString filterString;
};

#include "ui_AppsView.h"

class AppsView : public QWidget, public TMIProcessCallback
{
	Q_OBJECT

public:
	AppsView(TrackMyInternet *parent = 0);
	~AppsView();

	void OnFlowStore(shared_ptr<TMIFlow> f) {
		ui.treeView->setUpdatesEnabled(false);
		model->RemoveFlow(f);
		ui.treeView->setUpdatesEnabled(true);
	}

	void OnFlowUpdate(shared_ptr<TMIFlow> f);
	void OnFlowAged(shared_ptr<TMIFlow> f) {
	}

	void OnFlowSpeed(shared_ptr<TMIFlow> f) {
		ui.treeView->setUpdatesEnabled(false);
		model->UpdateFlowSpeed(f);
		ui.treeView->setUpdatesEnabled(true);
	}

	virtual void OnDataSourceChanged(UpdateType type);
	void reset() { model->reset(); }
private:
	Ui::AppsView ui;
	TrackMyInternet *appWindow;
	// use with progressbar
	int dbRowsToLoad;
	int dbRowsLoaded;
	QSortFilterProxyModel *proxyModel;
	AppTreeModel *model;
	
	// tree
	unordered_map<string, shared_ptr<TMIFlow>> app_flows;
	unordered_map<string, shared_ptr<TMIFlow>>::const_iterator flow;
	QFileIconProvider provider;

	QTimer currentDisplayTimer;
	QTimer sortTimer;
	TreeItem *currentDisplayItem;

	QMenu* contextMenu;
	string contextFolder;
	string contextCountryIso;
	string contextAppFullPath;
	QString contextClipboardData;
	QAction* openFolderAction;
	QAction* blockCountryAction;

	private slots:
		void AppTreeSelectionChanged(const QItemSelection & selected, const QItemSelection & deselected);
		void AppTreeItemUpdated(TreeItem *item);
		void AppTreeItemDeleted(TreeItem *item);
		void UpdateCurrentDisplayItem();
		void UpdateSort();

		void goLiveClicked(bool checked);
		void goOfflineClicked(bool checked);
		void treeViewItemExpanded(const QModelIndex &index);

		void onContextMenu(const QPoint &point);

		void onContextOpenFolder();
		void onContextBlockCountry();
		void onContextBlockApp();
		void onContextUnBlockCountry();

		void onCopyToClipboard();

		// search box
		void searchBoxChanged(const QString &text);

		// tool buttons
		void buttonExpandAllClicked(bool checked = false);
		void buttonCollapseAllClicked(bool checked = false);


};

#endif // APPSVIEW_H
