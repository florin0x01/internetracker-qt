#include "stdafx.h"
#include "trackmyinternet.h"
#include "CountryIconProvider.h"


CountryIconProvider::CountryIconProvider()
{
	
}

CountryIconProvider::~CountryIconProvider()
{
	qDeleteAll(icons);
	icons.clear();
}

QIcon* CountryIconProvider::getCountryByIso(const QString& _iso)
{
	QString iso = _iso.toLower();

	QMap<QString, QIcon*>::const_iterator icon = icons.find(iso);

	if (icon != icons.end())
		return *icon;

	QString path = QString(":/TrackMyInternet/images/") + iso + QString(".png");

	QIcon *country = new QIcon(path);
	if (country->isNull())
	{
		return NULL;
	}

	icons.insert(iso, country);

	return country;
}