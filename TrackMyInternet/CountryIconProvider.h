#pragma once

#include <QMap>
#include <QImage>

class CountryIconProvider : public QObject
{
public:
	CountryIconProvider();
	virtual ~CountryIconProvider();
	QIcon* getCountryByIso(const QString& iso);

private:
	QMap<QString, QIcon*> icons;
};

Q_GLOBAL_STATIC(CountryIconProvider, IsoCountryProvider)

