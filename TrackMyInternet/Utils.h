#pragma once
#include "stdafx.h"
#include <sstream>
#include <string>
#include <iomanip>
#include <string>
#include "tmi.h"

using namespace std;

static inline string prettyBytes(UINT64 _bytes) {
	string unit = "B";
	double bytes = (double)_bytes;

	if (bytes > 1000 && bytes <= 1000 * 1000) {
		bytes /= 1000;
		unit = "KB";
	} else if (bytes > 1000 * 1000) {
		bytes /= 1000 * 1000;
		unit = "MB";
	}

	std::ostringstream stringStream;
	stringStream << fixed << setprecision(2) << bytes << " " << unit;

	return stringStream.str();
}

// hash the SID to HEX 
static inline string HashFlow(flow *f, flow_ext_v1 *ext = NULL) {
	stringstream stream;
	unsigned char *buffer = (unsigned char*)f->sid;
	for (int i = 0; i < sizeof(f->sid); i += 2) {
		string data = string(((const char*)&f->sid) + i, 2);
		stream << hex << setw(2) << setfill('0') << (SHORT)(buffer[i]);
	}

	if (ext) {
		// hash start time also 
		buffer = (unsigned char*)&ext->start.QuadPart;
		for (int i = 0; i < sizeof(ext->start.QuadPart); i += 2) {
			string data = string(((const char*)&f->sid) + i, 2);
			stream << hex << setw(2) << setfill('0') << (SHORT)(buffer[i]);
		}
	}

	//CryptBinaryToString((BYTE*)&(f->sid), sizeof(f->sid), CRYPT_STRING_HEX | CRYPT_STRING_NOCRLF | CRYPT_STRING_NOCR, hash, &size);
	return stream.str();
}

