#pragma once
#include <QObject>
#include <QDebug>
#include <QThread>
#include <QSettings>
#include <qcoreapplication.h>
#include "tmi.h"

class Settings : public QSettings
{
	Q_OBJECT
public:
	static Settings& Instance()
	{
		static Settings set;

		return set;
	}

	void ChangeSettings()
	{
		save();
		emit SettingsChanged();
	}

	double MapSpeedKBps;
	double MapTransferKBs;
	double MapUpdateMsec;
	double TreeUpdateMsec;

	void save();
	void load();

private:
	Settings():QSettings("MondoHost","TrackMyInternet")
	{
		QCoreApplication::setOrganizationName("MondoHost");
		QCoreApplication::setOrganizationDomain("mondoserv.com");
		QCoreApplication::setApplicationName("TrackMyInternet");
		load();
	}

	vector<string> blockedCountries;
	vector<string> blockedApps;	

signals:
void SettingsChanged();

};

