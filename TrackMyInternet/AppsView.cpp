#include "stdafx.h"
#include "AppsView.h"
#include "CountryIconProvider.h"
#include "Utils.h"
#include "trackmyinternet.h"
#include <qclipboard.h>

static QFileIconProvider provider;

void AppsView::onContextOpenFolder() {

#ifdef Q_WS_MAC
	QStringList args;
	args << "-e";
	args << "tell application \"Finder\"";
	args << "-e";
	args << "activate";
	args << "-e";
	args << "select POSIX file \"" + filePath + "\"";
	args << "-e";
	args << "end tell";
	QProcess::startDetached("osascript", args);
#endif

	QStringList args;
	args << "/select," << QDir::toNativeSeparators(QString::fromStdString(contextFolder));
	QProcess::startDetached("explorer", args);

}

void AppsView::onContextBlockCountry() {

}

void AppsView::onContextBlockApp() {
	TMIControl::Instance().getLiveDataSource()->blockApp(contextAppFullPath);
}

void AppsView::onContextUnBlockCountry() {

}

void AppsTreeView::_filter2(TreeItem *item, string &filter) {
	string name, country, ip;	
	bool enableAll = false;
	
	if (filter.length() == 0) {
		enableAll = true;
		return;
	}

	for (TreeItem *parent = item->getParent(); parent; item = parent, parent = item->getParent()) {
		switch (item->GetType()) {
		case TreeItem::ROOT:
			break;
		case TreeItem::APP:
			if (!isRowHidden(item->row(), parent->index[0])) {
				if (item->getChildrenCount() && item->getHiddenCount() == item->getChildrenCount()) {
					parent->hide();
					setRowHidden(item->row(), parent->index[0], true);
				}
			}
			else {
				if (item->getHiddenCount() != item->getChildrenCount()) {
					parent->unhide();
					setRowHidden(item->row(), parent->index[0], false);
				}
			}
			break;
		case TreeItem::COUNTRY:
			if (!isRowHidden(item->row(), parent->index[0])) {
				if (item->getChildrenCount() && item->getHiddenCount() == item->getChildrenCount()) {
					parent->hide();
					setRowHidden(item->row(), parent->index[0], true);
				}
			}
			else {
				if (item->getHiddenCount() != item->getChildrenCount()) {
					parent->unhide();
					setRowHidden(item->row(), parent->index[0], false);
				}
			}
			break;
		case TreeItem::COLLAPSED:
		case TreeItem::CONNECTION:
			name = parent->getParent()->getProcessName();
			country = parent->getProcessCountry();
			ip = item->GetHostname().toStdString();
			if (ip.length()) {
				std::transform(ip.begin(), ip.end(), ip.begin(), ::tolower);
			}
			else {
				ip = item->GetFlow()->GetRemoteAddr();
			}

			std::transform(country.begin(), country.end(), country.begin(), ::tolower);
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);
			if ((name.find(filter) == string::npos) && (ip.find(filter) == string::npos) && (country.find(filter) == string::npos)) {
				if (!isRowHidden(item->row(), parent->index[0])) {
					// increase hidden children count
					parent->hide();
					setRowHidden(item->row(), parent->index[0], true);
				}
			}
			else {
				if (isRowHidden(item->row(), parent->index[0])) {
					parent->unhide();
					setRowHidden(item->row(), parent->index[0], false);
				}
			}

			break;
		};
	}
}


void AppsTreeView::_filter(TreeItem *item, string &filter) {
	string path, name, country, ip;
	bool enableAll = false;
	if (filter.length() == 0) {
		enableAll = true;
	}

	for (int i = 0; i < item->getChildrenCount(); i++) {
		_filter(item->getChild(i), filter);
	}

	if (enableAll && item->GetType() != TreeItem::ROOT) {
		setRowHidden(item->row(), item->getParent()->index[0], false);
		item->hideReset();
		return;
	}

	switch (item->GetType()) {
		case TreeItem::ROOT:		
			break;
		case TreeItem::APP:
			if (!isRowHidden(item->row(), item->getParent()->index[0])) {
				if (item->getChildrenCount() && item->getHiddenCount() == item->getChildrenCount()) {
					item->getParent()->hide();
					setRowHidden(item->row(), item->getParent()->index[0], true);			
				}
			} else {
				if (item->getHiddenCount() < item->getChildrenCount()) {
					item->getParent()->unhide();
					setRowHidden(item->row(), item->getParent()->index[0], false);
				}
			}
			break;		
		case TreeItem::COUNTRY:
			if (!isRowHidden(item->row(), item->getParent()->index[0])) {
				if (item->getChildrenCount() && item->getHiddenCount() == item->getChildrenCount()) {
					item->getParent()->hide();
					setRowHidden(item->row(), item->getParent()->index[0], true);
				}
			}
			else {
				if (item->getHiddenCount() < item->getChildrenCount()) {
					item->getParent()->unhide();
					setRowHidden(item->row(), item->getParent()->index[0], false);
				}
			}
			break;
		case TreeItem::COLLAPSED:
		case TreeItem::CONNECTION:
			//path = item->getParent()->getParent()->getProcessPath();
			name = item->getParent()->getParent()->getProcessName();
			country = item->getParent()->getProcessCountry();
			ip = item->GetHostname().toStdString();
			if (ip.length()) {
				std::transform(ip.begin(), ip.end(), ip.begin(), ::tolower);
			}
			else {
				ip = item->GetFlow()->GetRemoteAddr();
			}
		
			std::transform(country.begin(), country.end(), country.begin(), ::tolower);
			//std::transform(path.begin(), path.end(), path.begin(), ::tolower);
			std::transform(name.begin(), name.end(), name.begin(), ::tolower);		

			if ( (name.find(filter) == string::npos) && (ip.find(filter) == string::npos) && (country.find(filter) == string::npos)) {
				if (!isRowHidden(item->row(), item->getParent()->index[0])) {
					// increase hidden children count				
					setRowHidden(item->row(), item->getParent()->index[0], true);
					item->getParent()->hide();
				}
			} else {
				if (isRowHidden(item->row(), item->getParent()->index[0])) {				
					setRowHidden(item->row(), item->getParent()->index[0], false);
					item->getParent()->unhide();
				}
			}

			break;

	};



}

void AppsTreeView::rowsInserted(const QModelIndex &parent, int start, int end) {
	QTreeView::rowsInserted(parent, start, end);
	if (!parent.isValid())
		return;

	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());

	if (!item) {
		return;
	}
	
	//this->expand(parent);

	if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
		for (int i = start; i <= end; i++) {
			filter2(item->getChild(i), getFilter());
			//this->expand(item->getChild(i)->index[0]);
		}		
		//filter(((AppTreeModel*)this->model())->getItem(QModelIndex()), getFilter());
	}	

	
}

void AppsTreeView::filter(TreeItem *root, const QString &filter) {
	string filterLower = filter.toStdString();
	std::transform(filterLower.begin(), filterLower.end(), filterLower.begin(), ::tolower);
	setFilter(QString::fromStdString(filterLower));	

	if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
		_filter(root, filterLower);
	}
	
}

void AppsTreeView::filter2(TreeItem *root, const QString &filter) {
	string filterLower = filter.toStdString();
	std::transform(filterLower.begin(), filterLower.end(), filterLower.begin(), ::tolower);
	setFilter(QString::fromStdString(filterLower));

	if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
		_filter2(root, filterLower);
	}

}

void AppsView::buttonExpandAllClicked(bool checked) {	
	ui.treeView->expandAll();
}

void AppsView::buttonCollapseAllClicked(bool checked) {
	ui.treeView->collapseAll();
}


void AppsView::searchBoxChanged(const QString &text) {
	ui.treeView->filter(model->getItem(QModelIndex()), text);

	if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::OfflineUpdate) {
		// db implementation
		/*char buffer[4096];
		sprintf(buffer, "Select *,count(id) as groupByCount, sum(bytesRx) as bytesRx, sum(bytesTx) as bytesTx, sum(packetsRx) as packetsRx, sum(packetsTx) as packetsTx from flows "
			" where (app LIKE '%%%s%%' or country LIKE '%%%s%%' or remoteIP_str LIKE '%%%s%%') "
			"group by app, country_iso ORDER by start", text.toStdString().c_str(), text.toStdString().c_str(), text.toStdString().c_str());*/
		DBFilterBuilder::getInstance().setSearchCrit(text.toStdString());
		string filter = DBFilterBuilder::getInstance().getFilter();
		TMIControl::Instance().setDataSourceFilter(filter);
		model->reset();		
	}

}

void AppsView::onCopyToClipboard() {
	QClipboard *clipboard = QApplication::clipboard();
	
	clipboard->setText(contextClipboardData);
}

void AppsView::onContextMenu(const QPoint &point) {
	QModelIndex index = ui.treeView->indexAt(point);
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	QString action;

	if (!item) {
		return;
	}
	contextMenu = new QMenu(ui.treeView);
	contextMenu->setStyleSheet("QMenu { font-family:'Segoe UI Semibold'; font-size: 20px;");	
	contextClipboardData = item->data(index.column(), Qt::DisplayRole).toString();
	switch(item->GetType()) {
		case TreeItem::APP:
			{				
				string path = item->getProcessPath();
				contextFolder = path;
				contextAppFullPath = item->getProcessFullPath();
				path = path.substr(0,path.find_last_of('\\'));
				QString action = QString("Open application folder: ") + QString::fromStdString(path);
				QAction *act = contextMenu->addAction(provider.icon(provider.Folder), action);			
				connect(act, &QAction::triggered, this, &AppsView::onContextOpenFolder);

				action = QString("Block application: ") + QString::fromStdString(item->getProcessName());
				act = contextMenu->addAction(item->icon, action);
				connect(act, &QAction::triggered, this, &AppsView::onContextBlockApp);

			}
			break;
		case TreeItem::COUNTRY: 
			{
				contextCountryIso = item->getProcessCountryIso();
				QString action = QString("Block country: ") + QString::fromStdString(item->getProcessCountry());
				QAction *act = contextMenu->addAction(action);
				connect(act, &QAction::triggered, this, &AppsView::onContextBlockCountry);

			}
			break;
		case TreeItem::CONNECTION:
		{
			string path = item->getParent()->getParent()->getProcessPath();
			contextFolder = path;
			contextAppFullPath = item->getParent()->getParent()->getProcessFullPath();
			path = path.substr(0, path.find_last_of('\\'));
			contextCountryIso = item->getProcessCountryIso();
			action = QString("Open application folder: ") + QString::fromStdString(path);
			QAction *act = contextMenu->addAction(provider.icon(provider.Folder), action);
			connect(act, &QAction::triggered, this, &AppsView::onContextOpenFolder);
			
			contextMenu->addSeparator();
			
			action = QString("Block country: ") + QString::fromStdString(item->getProcessCountry());
			act = contextMenu->addAction(action);
			connect(act, &QAction::triggered, this, &AppsView::onContextBlockCountry);

		}
		break;

	}

	// copy to clipboard
	contextMenu->addSeparator();

	action = QString("Copy to clipboard");
	QAction *act = contextMenu->addAction(action);
	connect(act, &QAction::triggered, this, &AppsView::onCopyToClipboard);

	contextMenu->exec(ui.treeView->mapToGlobal(point));
}

void AppsView::OnFlowUpdate(shared_ptr<TMIFlow> f) {
	ui.treeView->setUpdatesEnabled(false);
	model->ProcessFlow(f);
	appWindow->trayIcon->setToolTip(QString("Tracking %1 apps").arg(model->rowCount()));
	
	if (TMIControl::Instance().getDataSource()->getUpdateType() == OfflineUpdate) {
		dbRowsLoaded++;
		if (dbRowsLoaded < dbRowsToLoad) {
			appWindow->progressBar->setValue(dbRowsLoaded);
			int percentage = ((double)dbRowsLoaded / (double)dbRowsToLoad) * 100;
			appWindow->progressBar->setFormat("Loading history from archive: " + QString::number(percentage) + "%");
		}
		else {
			appWindow->progressBar->setVisible(false);
		}
	}

	ui.treeView->setUpdatesEnabled(true);	
}

void AppsView::goLiveClicked(bool checked) {
	// needs to be first as switch will also feed us data
	//type = TMIProcessCallback::LiveUpdate;
	//TMIControl::Instance().switchDataSource(TMIProcessCallback::LiveUpdate);	
}

void AppsView::goOfflineClicked(bool checked) {	
	/*type = TMIProcessCallback::OfflineUpdate;
	TMIControl::Instance().switchDataSource(TMIProcessCallback::OfflineUpdate);	
	dbRowsToLoad = TMIControl::Instance().setDataSourceFilter("Select *,count(id) as groupByCount, sum(bytesRx) as bytesRx, sum(bytesTx) as bytesTx, sum(packetsRx) as packetsRx, sum(packetsTx) as packetsTx from flows group by app, country_iso ORDER by start");
	dbRowsLoaded = 0;

	// setup the progressbar
	appWindow->progressBar->setMaximum(dbRowsToLoad);
	appWindow->progressBar->setVisible(true);
	appWindow->progressBar->setValue(0);	*/
}

void AppsView::OnDataSourceChanged(UpdateType type) {
	model->reset();
	this->type = type;
	if (type != LiveUpdate) {
		appWindow->progressBar->setMinimum(0);
		appWindow->progressBar->setMaximum(100);
		ui.buttonCollapseAll->setEnabled(false);
		ui.buttonExpandAll->setEnabled(false);
	} else {
		ui.buttonCollapseAll->setEnabled(true);
		ui.buttonExpandAll->setEnabled(true);

	}
}

void AppsView::treeViewItemExpanded(const QModelIndex &index) {
	char query[2048], queryCount[2048];
	if (type == LiveUpdate || !index.isValid()) {
		return;
	}

	TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());

	if (!childItem) {
		return;
	}

	if (childItem->GetType() != TreeItem::COUNTRY) {
		return;
	}
	
	/*
				"`packetsRx`	INTEGER,"
			"`packetsTx`	INTEGER,"
			"`bytesRx`	INTEGER,"
			"`bytesTx`	INTEGER,"
	*/
	sprintf(query, "Select * from flows where app = \"%s\" and country_iso=\"%s\"", childItem->getParent()->getProcessPath().c_str(), childItem->getProcessCountryIso().c_str());
	//sprintf(queryCount, "Select id from flows where app = \"%s\" and country_iso=\"%s\"", childItem->getParent()->getProcessPath().c_str(), childItem->getProcessCountryIso().c_str());
	dbRowsToLoad = TMIControl::Instance().setDataSourceFilter(query);
	dbRowsLoaded = 0;

	// setup the progressbar
	appWindow->progressBar->setMaximum(dbRowsToLoad);
	appWindow->progressBar->setVisible(true);
	appWindow->progressBar->setValue(0);
}

AppsView::AppsView(TrackMyInternet *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	model = new AppTreeModel(this);
	
	// get a ptr to our app window
	appWindow = parent;
	
	this->type = TMIProcessCallback::LiveUpdate;
	//this->type = TMIProcessCallback::OfflineUpdate;

	//proxyModel = new QSortFilterProxyModel(this);
	//proxyModel->setSourceModel(model);

	ui.treeView->setModel(model);
	ui.treeView->setUniformRowHeights(true);
	ui.treeView->setColumnWidth(0, 256);
	ui.treeView->setColumnWidth(1, 120);
	ui.treeView->setColumnWidth(2, 120);
	ui.treeView->setColumnWidth(3, 120);

	QList<int> splitterWidgetSizes = { 1, 0 };
	ui.splitter->setSizes(splitterWidgetSizes);

	QItemSelectionModel *selectionModel = ui.treeView->selectionModel();
	QMetaObject::Connection c = connect(selectionModel, SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(AppTreeSelectionChanged(const QItemSelection &, const QItemSelection &)));	
	ui.treeView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.treeView->setIndentation(30);
	ui.treeView->setAutoFillBackground(false);	
	ui.treeView->setSortingEnabled(false);
	//proxyModel->setSortRole(Qt::UserRole);
	//proxyModel->setDynamicSortFilter(false);
	ui.treeView->setSortingEnabled(true);

	currentDisplayItem = 0;
	connect(&currentDisplayTimer, SIGNAL(timeout()), this, SLOT(UpdateCurrentDisplayItem()));

	connect(&sortTimer, SIGNAL(timeout()), this, SLOT(UpdateSort()));

	sortTimer.setInterval(1000);
	sortTimer.start();

	ui.labelTotalRX->setStyleSheet("QLabel { color : #EFE; }");
	ui.labelTotalTX->setStyleSheet("QLabel { color : #EFE; }");

	connect(ui.goLive, SIGNAL(clicked(bool)), this, SLOT(goLiveClicked(bool)));
	connect(ui.goOffline, SIGNAL(clicked(bool)), this, SLOT(goOfflineClicked(bool)));
	connect(ui.treeView, SIGNAL(expanded(const QModelIndex &)), this, SLOT(treeViewItemExpanded(const QModelIndex &)));

	ui.treeView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui.treeView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onContextMenu(const QPoint &)));

	// search box
	connect(ui.searchComboBox, SIGNAL(editTextChanged(const QString&)), this, SLOT(searchBoxChanged(const QString&)));

	// tool buttons
	connect(ui.buttonCollapseAll, SIGNAL(clicked(bool)), this, SLOT(buttonCollapseAllClicked(bool)));
	connect(ui.buttonExpandAll, SIGNAL(clicked(bool)), this, SLOT(buttonExpandAllClicked(bool)));

	ui.groupBox->setVisible(false);
	
}

AppsView::~AppsView()
{

}

void AppsView::UpdateSort() {
	ui.treeView->setUpdatesEnabled(false);
	if (TMIControl::Instance().getDataSource()->getUpdateType() == TMIProcessCallback::LiveUpdate) {
		model->sort(ui.treeView->header()->sortIndicatorSection(), ui.treeView->header()->sortIndicatorOrder());
		ui.labelTotalRX->setText(prettyBytes(TMIControl::Instance().GetTotalBytesRX()).c_str());
		ui.labelTotalTX->setText(prettyBytes(TMIControl::Instance().GetTotalBytesTX()).c_str());
	}
	ui.treeView->setUpdatesEnabled(true);	
}

void AppsView::UpdateCurrentDisplayItem() {
	if (currentDisplayItem != NULL) {
		AppTreeItemUpdated(currentDisplayItem);
	}
}
void AppsView::AppTreeItemDeleted(TreeItem *item) {
	if (currentDisplayItem == item) {
		QList<int> splitterWidgetSizes = { 1, 0 };
		ui.splitter->setSizes(splitterWidgetSizes);
		currentDisplayItem = NULL;
		
	}
}

void AppsView::AppTreeItemUpdated(TreeItem *item) {
	currentDisplayItem = item;

	if (!currentDisplayItem) {
		return;
	}

	if (currentDisplayItem->GetHostname().length() > 0) {
		ui.destinationHost->setText(currentDisplayItem->GetHostname());
	}
	else if (currentDisplayItem->GetFlow()) {
		ui.destinationHost->setText(currentDisplayItem->GetFlow()->GetRemoteAddr().c_str());
	}

	// get duration
	QTime duration;
	UINT64 durationMs = currentDisplayItem->GetFlow()->GetDuration();
	QTime t = QTime::fromMSecsSinceStartOfDay((static_cast<int>(durationMs)));

	ui.labelDuration->setText(t.toString("hh:mm:ss"));
	ui.labelDestinationPort->setText(QString::number(currentDisplayItem->GetFlow()->GetRemotePort()));
	ui.labelLocalPort->setText(QString::number(currentDisplayItem->GetFlow()->GetLocalPort()));
	ui.labelRemoteIP->setText(QString::fromStdString(currentDisplayItem->GetFlow()->GetRemoteAddr()));

	ui.labelBytesReceived->setText(QString::number(currentDisplayItem->GetFlow()->GetFlowRx()));
	ui.labelBytesSent->setText(QString::number(currentDisplayItem->GetFlow()->GetFlowTx()));

	ui.labelReceivedPackets->setText(QString::number(currentDisplayItem->GetFlow()->GetFlowPacketsRx()));
	ui.labelSentPackets->setText(QString::number(currentDisplayItem->GetFlow()->GetFlowPacketsTx()));

	ui.labelDownlinkSpeed->setText(QString::fromStdString(prettyBytes(currentDisplayItem->GetFlow()->getSpeedStats().downlinkBps)) + "ps");
	ui.labelUplinkSpeed->setText(QString::fromStdString(prettyBytes(currentDisplayItem->GetFlow()->getSpeedStats().uplinkBps)) + "ps");

	ui.labelDestinationCountry->setText(QString::fromStdString(currentDisplayItem->GetFlow()->GetCountry()));

	ui.labelDestinationCountry->setTextFormat(Qt::RichText);

	QString res = QString("<img align='bottom' width='24' height='24' src=\":/TrackMyInternet/images/%1.png\"> <span>%2</span>").arg(QString::fromStdString(currentDisplayItem->GetFlow()->GetCountryISO()).toLower())
		.arg(QString::fromStdString(currentDisplayItem->GetFlow()->GetCountry()));
	ui.labelDestinationCountry->setText(res);
	//ui.labelDestinationCountry->setWindowIcon(*IsoCountryProvider->getCountryByIso(currentDisplayItem->GetFlow()->GetCountryISO().c_str()));
}

void AppsView::AppTreeSelectionChanged(const QItemSelection & selected, const QItemSelection & deselected) {

	foreach(QModelIndex index, selected.indexes()) {
		TreeItem* item = static_cast<TreeItem*>(index.internalPointer());
		if (index.isValid() && item->GetType() == TreeItem::CONNECTION) {
				currentDisplayTimer.start(1000);
				disconnect(currentDisplayItem, SIGNAL(ItemDeleted(TreeItem *)), this, SLOT(AppTreeItemDeleted(TreeItem *)));
				disconnect(currentDisplayItem, SIGNAL(ItemRefreshed(TreeItem *)), this, SLOT(AppTreeItemUpdated(TreeItem *)));

				QMetaObject::Connection c = connect(item, SIGNAL(ItemRefreshed(TreeItem *)), this, SLOT(AppTreeItemUpdated(TreeItem *)));
				c = connect(item, SIGNAL(ItemDeleted(TreeItem *)), this, SLOT(AppTreeItemDeleted(TreeItem *)));
				QList<int> splitterWidgetSizes = { 1, 1 };
				ui.splitter->setSizes(splitterWidgetSizes);
				AppTreeItemUpdated(item);
		
		}
		else {
			QList<int> splitterWidgetSizes = { 1, 0 };
			ui.splitter->setSizes(splitterWidgetSizes);
			currentDisplayTimer.stop();
			currentDisplayItem = NULL;
		}
	}

}
