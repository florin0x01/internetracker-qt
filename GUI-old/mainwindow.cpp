#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"
#include <iostream>
#include <QCoreApplication>
#include <QTextStream>

using namespace std;

typedef QTableWidgetItem** QTableWidgetRow;

const char* QStringToChar(const QString &str1)
{

    QByteArray ba = str1.toLatin1();
    return ba.data();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->processList->addItem("ananas.exe");
    ui->processList->addItem("js.exe");
    ui->processList->addItem("watch.exe");
    ui->processList->addItem("steam.exe");



   //https://freegeoip.net/json/193.226.51.1
   //https://github.com/maxmind/libmaxminddb

   ui->tableWidget->setColumnCount(6);
    ui->tableWidget->setRowCount(1410);

   QStringList m_TableHeader;
   m_TableHeader<<"Source IP"<<"Destination IP"<<"Received bytes"<<"Sent bytes"<<"Protocol"<<"Country";
   ui->tableWidget->setHorizontalHeaderLabels(m_TableHeader);
   ui->tableWidget->setShowGrid(false);
   ui->tableWidget->verticalHeader()->setVisible(false);
   ui->tableWidget->setGeometry(QApplication::desktop()->screenGeometry());
   ui->tableWidget->setAutoScroll(true);


   QVector<QTableWidgetItem**> vectorIp;

   QFile f("C:\\Users\\nini\\Documents\\ips.txt");

   if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
          exit(0);

   int line_no = 0;

   while(!f.atEnd()) {
        QString ipQt = f.readLine();
        QByteArray array = ipQt.toLocal8Bit();
        char* ip = array.data();
        ip[strlen(ip)-1] = 0;

       QTableWidgetItem* row[6];
      //  cout << ip << endl;

       row[0] = new QTableWidgetItem(QString::number(line_no));
       row[1] = new QTableWidgetItem(ip);
       row[2] = new QTableWidgetItem("5400");
       row[3] = new QTableWidgetItem("3200");
       row[4] = new QTableWidgetItem("UDP");
       GeoCountry country;
       GeoCountryIp(ip, country);

       QString countryFlag = QString(":/images/") + QString(country.iso) + QString(".png");
       qDebug() << "Flag " << countryFlag << endl;

       row[5] = new QTableWidgetItem(QIcon(countryFlag), country.country);
       for(int i=0; i < 6; i++) {
           row[i]->setFlags(row[i]->flags() ^ Qt::ItemIsEditable);
           ui->tableWidget->setItem(line_no,i, row[i]);
       }
       line_no++;

   }
   f.close();
}


MainWindow::~MainWindow()
{
    delete ui;
}
