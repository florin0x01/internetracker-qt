#include "utils.h"
#include "maxminddb.h"
#include <iostream>


void GeoCountryIp(const char* ipstr, GeoCountry& finalResult)
{

    MMDB_s mmdb;
    char *fname = "C:\\GeoLite2-Country.mmdb";
    int status = MMDB_open(fname, MMDB_MODE_MMAP, &mmdb);
    int exit_code = 0;

    if (MMDB_SUCCESS != status) {
        fprintf(stderr, "\n  Can't open %s - %s\n",
            fname, MMDB_strerror(status));

        if (MMDB_IO_ERROR == status) {
            fprintf(stderr, "    IO error: %s\n", strerror(errno));
        }
       return ;
    }

    int gai_error, mmdb_error;
    MMDB_lookup_result_s result =
        MMDB_lookup_string(&mmdb, ipstr, &gai_error, &mmdb_error);

    if (0 != gai_error) {
        fprintf(stderr,
            "\n  Error from getaddrinfo for %s - %s\n\n",
            ipstr, gai_strerror(gai_error));
        exit_code = 2;
        goto end;
    }

    if (MMDB_SUCCESS != mmdb_error) {
        fprintf(stderr,
            "\n  Got an error from libmaxminddb: %s\n\n",
            MMDB_strerror(mmdb_error));
        exit_code = 3;
        goto end;
    }

      if (result.found_entry) {

        MMDB_entry_data_s entry_data;
        MMDB_entry_data_s entry_data2;

        int status = MMDB_get_value(&result.entry, &entry_data,
            "country", "iso_code", NULL);

        int status2 = MMDB_get_value(&result.entry, &entry_data2,
                   "country", "names","en", NULL);

        if (MMDB_SUCCESS != status) {
            fprintf(
                stderr,
                "Got an error looking up the entry data - %s\n",
                MMDB_strerror(status));
            exit_code = 4;
            goto end;
        }

        if (entry_data.has_data)
        {
            memset(&finalResult,0,sizeof(finalResult));
            memcpy(finalResult.iso, entry_data.utf8_string, entry_data.data_size);

            if(status2 == MMDB_SUCCESS) {
                memcpy(finalResult.country, entry_data2.utf8_string, entry_data2.data_size);
                std::cout << "Country: " << finalResult.country << std::endl;
            }
            else {
                     memcpy(finalResult.country, entry_data.utf8_string, entry_data.data_size);
            }
           // for ( ; *finalResult.iso; ++finalResult.iso) *finalResult.iso = tolower(*finalResult.iso);
            for(char*p = finalResult.iso; *p; p++) *p = tolower(*p);
            return ;
        }
    }
    else {
        fprintf(
            stderr,
            "\n  No entry for this IP address (%s) was found\n\n",
            ipstr);
        exit_code = 5;
    }

end:
    MMDB_close(&mmdb);
    memcpy(finalResult.country,"",sizeof(finalResult.country));
    memcpy(finalResult.iso, "", sizeof(finalResult.iso));
}

const char* geoIp(const char* ipstr)
{
    MMDB_s mmdb;
    const char *fname = "C:\\GeoLite2-Country.mmdb";
    int status = MMDB_open(fname, MMDB_MODE_MMAP, &mmdb);

    if (MMDB_SUCCESS != status) {

        return "";
    }

    int gai_error, mmdb_error;
    MMDB_lookup_result_s result =
        MMDB_lookup_string(&mmdb, ipstr, &gai_error, &mmdb_error);

    if (0 != gai_error) {
         MMDB_close(&mmdb);
        return "";
    }

    if (MMDB_SUCCESS != mmdb_error) {
         MMDB_close(&mmdb);
        return "";
    }


    if (result.found_entry) {
        MMDB_entry_data_s entry_data;

        std::cout << "Querying";

        status = MMDB_get_value(&result.entry, &entry_data,
                         "country", "names", "en", NULL);

        if (MMDB_SUCCESS != status) {
             MMDB_close(&mmdb);
            return "";
        }
        if (entry_data.has_data) {
            MMDB_close(&mmdb);
            return entry_data.utf8_string;
        }
    }


    MMDB_close(&mmdb);
    return "";
}
